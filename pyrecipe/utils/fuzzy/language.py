# -*- coding: utf-8 -*-
from __future__ import annotations

import re
from collections.abc import Iterable

from pyphonetics import RefinedSoundex  # type: ignore
from stop_words import safe_get_stop_words  # type: ignore

from pyrecipe.model.book import Lang

_remove_punctuation_pattern = re.compile(r"[\W_]+")
_quantity_pattern = re.compile(r"\[[^\]]*\]")
_numbers_pattern = re.compile(r"\d+")
_soundex = RefinedSoundex()


def _stop_words_for(lang: Lang) -> set[str]:
    return set(safe_get_stop_words(lang))


def _remove_punctuation(value: str) -> str:
    return re.sub(_remove_punctuation_pattern, " ", value)


def _remove_quantities(value: str) -> str:
    result = re.sub(_numbers_pattern, "", value)
    return re.sub(_quantity_pattern, "", result)


def _remove_stop_words(lang: Lang, words: Iterable[str]) -> Iterable[str]:
    stop_words = _stop_words_for(lang)
    return (w for w in words if w not in stop_words)


def tokenize(lang: Lang, value: str) -> Iterable[str]:
    return _remove_stop_words(
        lang, _remove_punctuation(_remove_quantities(value)).split()
    )


def match(lang: Lang, label: str, instruction: str) -> bool:
    reference = set(_soundex.phonetics(w) for w in tokenize(lang, instruction))
    for item in [_soundex.phonetics(w) for w in tokenize(lang, label)]:
        if item in reference:
            return True
    return False
