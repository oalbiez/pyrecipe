# -*- coding: utf-8 -*-
from __future__ import annotations

from fuzzysearch import find_near_matches  # type: ignore

from pyrecipe.model.book import Ingredient, Lang, Step

from .language import match


def _instruction_from(step: Step) -> str:
    return " ".join(p for p in step.parts if isinstance(p, str))


def _substring_match(
    _: Lang, instructions: str, ingredients: list[Ingredient]
) -> tuple[list[Ingredient], list[Ingredient]]:
    matched = []
    unmatched = []
    for ingredient in ingredients:
        if ingredient.label in instructions:
            matched.append(ingredient)
        else:
            unmatched.append(ingredient)
    return (matched, unmatched)


def _levenstein_match(
    _: Lang, instructions: str, ingredients: list[Ingredient]
) -> tuple[list[Ingredient], list[Ingredient]]:
    matched = []
    unmatched = []
    for ingredient in ingredients:
        if find_near_matches(ingredient.label, instructions, max_l_dist=1):
            matched.append(ingredient)
        else:
            unmatched.append(ingredient)
    return (matched, unmatched)


def _soundex_match(
    lang: Lang, instructions: str, ingredients: list[Ingredient]
) -> tuple[list[Ingredient], list[Ingredient]]:
    matched = []
    unmatched = []
    for ingredient in ingredients:
        if match(lang, ingredient.label, instructions):
            matched.append(ingredient)
        else:
            unmatched.append(ingredient)
    return (matched, unmatched)


def dispatch_ingredients(
    lang: Lang, ingredients: list[Ingredient], steps: list[Step]
) -> list[Step]:
    algorithms = [_substring_match, _levenstein_match, _soundex_match]
    unmatched = list(ingredients)
    result = list(steps)
    for algorithm in algorithms:
        temp = []
        for step in result:
            matched, unmatched = algorithm(lang, _instruction_from(step), unmatched)
            temp.append(step.add_ingredients(matched))
        result = temp
        if len(unmatched) == 0:
            return result
    result.insert(0, Step(unmatched, []))
    return result
