# -*- coding: utf-8 -*-
from __future__ import annotations

import re
from enum import Enum

from pyphonetics import RefinedSoundex  # type: ignore

from pyrecipe.model.tag import Categories, Diets, Region, Tag

_soundex = RefinedSoundex()


def _soundex_for(*values: str) -> set[str]:
    return set(_soundex.phonetics(v) for v in values)


_categories: dict[Enum, set[str]] = {
    Categories.BAKED_GOOD: _soundex_for("backed good", "boulange"),
    Categories.BREAKFAST: _soundex_for("breakfast", "petit déjeuner"),
    Categories.DESSERT: _soundex_for("dessert"),
    Categories.DINNER: _soundex_for("dinner"),
    Categories.DRINK: _soundex_for("drink", "boisson"),
    Categories.LUNCH: _soundex_for("lunch", "repas", "plat principal"),
    Categories.PRE_DINNER: _soundex_for("pre-dinner"),
    Categories.SIDE_DISH: _soundex_for("side-dish"),
    Categories.SNACK: _soundex_for("snack"),
    Categories.SOUP: _soundex_for("soup", "soupe"),
    Categories.STARTER: _soundex_for("starter", "entrée"),
}

_regions: dict[Enum, set[str]] = {
    Region.ASIAN: _soundex_for("asian", "asiatique"),
    Region.CHINESE: _soundex_for("chinese", "chinoise"),
    Region.FRENCH: _soundex_for("french", "francaise"),
    Region.INDIAN: _soundex_for("indian", "indienne"),
    Region.ITALIAN: _soundex_for("italian", "italienne"),
    Region.JAPANESE: _soundex_for("japanese", "japonaise"),
    Region.MAXICAN: _soundex_for("mexican", "mexicaine"),
    Region.LATIN: _soundex_for(
        "latin", "latin america", "Amérique du Sud", "Amérique latine"
    ),
}

_diets: dict[Enum, set[str]] = {
    Diets.DAIRY_FREE: _soundex_for("dairy-free", "sans lait"),
    Diets.GLUTEN_FREE: _soundex_for("gluten-free", "sans gluten"),
    Diets.KETO: _soundex_for("keto"),
    Diets.PALEO: _soundex_for("paleo-friendly", "paleo"),
    Diets.VEGAN: _soundex_for("vegan"),
    Diets.VEGETARIAN: _soundex_for("vegetarian", "végétarien", "végé"),
}


def _match_value(tags: dict[Enum, set[str]], values: list[str]) -> Tag | None:
    for tag, phonetics in tags.items():
        for word in values:
            if word in phonetics:
                return tag.value
    return None


def create_tag(value: str) -> Tag | None:
    values = [_soundex.phonetics(value)]
    values.extend(_soundex_for(*re.sub(r"\d+", "", value).split()))

    for rules in [_categories, _regions, _diets]:
        tag = _match_value(rules, values)
        if tag:
            return tag
    return None
