# -*- coding: utf-8 -*-

from collections.abc import Iterable

from pandas import Series, read_excel  # type: ignore

from pyrecipe.model.pantry import (
    Aliment,
    Component,
    Composition,
    Pantry,
    compute_vitamin_A,
)
from pyrecipe.model.reference import Reference
from pyrecipe.utils.ciqual.common import Group, to_tags


def aliments(path: str) -> Pantry:
    return Pantry.on_list(
        _to_aliment(row) for row in _rows_of(f"{path}/calnut.xlsx", "MB")
    )


def _rows_of(filename: str, hypothesis: str) -> Iterable[Series]:
    excel = read_excel(
        filename,
        sheet_name=0,  # Only read the first sheet with product by line
        dtype=str,  # Use only str as value to avoid bad approximation on Fractions
    )
    for _, row in excel.iterrows():
        if row.at["HYPOTH"] == hypothesis:
            yield row


def _to_aliment(row: Series) -> Aliment:
    groups = [
        Group(row.at["alim_grp_code"], row.at["alim_grp_nom_fr"]),
        Group(row.at["alim_ssgrp_code"], row.at["alim_ssgrp_nom_fr"]),
        Group(row.at["alim_ssssgrp_code"], row.at["alim_ssssgrp_nom_fr"]),
    ]
    return Aliment(
        identifier=Reference(row.at["FOOD_LABEL"]),
        label=row.at["FOOD_LABEL"],
        tags=to_tags(
            source="calnut",
            code=row.at["alim_code"],
            name=row.at["FOOD_LABEL"],
            groups=groups,
        ),
        composition=_to_composition(row),
    )


def _to_composition(row: Series) -> Composition:
    return Composition.for100g(
        energy=Component.kcal(row.at["nrj_kcal"]),
        fat=Component.g(row.at["lipides_g"]),
        fa_saturated=Component.g(row.at["ags_g"]),
        fa_mono=Component.g(row.at["agmi_g"]),
        fa_poly=Component.g(row.at["agpi_g"]),
        carbohydrate=Component.g(row.at["glucides_g"]),
        sugars=Component.g(row.at["sucres_g"]),
        polyols=Component.g(row.at["polyols_g"]),
        starch=Component.g(row.at["amidon_g"]),
        fibres=Component.g(row.at["fibres_g"]),
        protein=Component.g(row.at["proteines_g"]),
        salt=Component.g(row.at["sel_g"]),
        alcohol=Component.g(row.at["alcool_g"]),
        organic_acids=Component.g(row.at["acides_organiques_g"]),
        cholesterol=Component.mg(row.at["cholesterol_mg"]),
        water=Component.g(row.at["eau_g"]),
        vitamin_A=compute_vitamin_A(
            Component.ug(row.at["retinol_mcg"]),
            Component.ug(row.at["beta_carotene_mcg"]),
        ),
        vitamin_D=Component.ug(row.at["vitamine_d_mcg"]),
        vitamin_E=Component.mg(row.at["vitamine_e_mg"]),
        vitamin_K1=Component.ug(row.at["vitamine_k1_mcg"]),
        vitamin_K2=Component.ug(row.at["vitamine_k2_mcg"]),
        vitamin_C=Component.mg(row.at["vitamine_c_mg"]),
        vitamin_B1=Component.mg(row.at["vitamine_b1_mg"]),
        vitamin_B2=Component.mg(row.at["vitamine_b2_mg"]),
        vitamin_B3=Component.mg(row.at["vitamine_b3_mg"]),
        vitamin_B5=Component.mg(row.at["vitamine_b5_mg"]),
        vitamin_B6=Component.mg(row.at["vitamine_b6_mg"]),
        vitamin_B12=Component.ug(row.at["vitamine_b12_mcg"]),
        vitamin_B9=Component.ug(row.at["vitamine_b9_mcg"]),
        sodium=Component.mg(row.at["sodium_mg"]),
        magnesium=Component.mg(row.at["magnesium_mg"]),
        phosphorus=Component.mg(row.at["phosphore_mg"]),
        chloride=Component.unknown(),
        potassium=Component.mg(row.at["potassium_mg"]),
        calcium=Component.mg(row.at["calcium_mg"]),
        manganese=Component.mg(row.at["manganese_mg"]),
        iron=Component.mg(row.at["fer_mg"]),
        copper=Component.mg(row.at["cuivre_mg"]),
        zinc=Component.mg(row.at["zinc_mg"]),
        selenium=Component.ug(row.at["selenium_mcg"]),
        iodine=Component.ug(row.at["iode_mcg"]),
    )
