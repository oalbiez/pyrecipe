# -*- coding: utf-8 -*-
from collections import defaultdict
from collections.abc import Iterable
from dataclasses import dataclass, field
from fractions import Fraction

from defusedxml.ElementTree import parse  # type: ignore

from pyrecipe.model.pantry import (
    Aliment,
    Component,
    Composition,
    Pantry,
    compute_vitamin_A,
)
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.reference import Reference
from pyrecipe.utils.ciqual.common import Group, to_tags


def aliments(path: str) -> Pantry:
    return Pantry.on_list(
        _read_aliments(
            path,
            XMLGroups.read_from(f"{path}/alim_grp.xml"),
            XMLCompositions.read_from(f"{path}/compo.xml"),
        )
    )


@dataclass
class XMLGroups:
    data: dict[str, str] = field(default_factory=dict)

    @staticmethod
    def read_from(filename: str) -> "XMLGroups":
        root = root_for(filename)
        groups = XMLGroups.empty()
        for item in root.findall("ALIM_GRP"):
            groups.add(
                content_of(item, "alim_grp_code"),
                content_of(item, "alim_grp_nom_fr"),
            )
            groups.add(
                content_of(item, "alim_ssgrp_code"),
                content_of(item, "alim_ssgrp_nom_fr"),
            )
            groups.add(
                content_of(item, "alim_ssssgrp_code"),
                content_of(item, "alim_ssssgrp_nom_fr"),
            )
        return groups

    @staticmethod
    def empty() -> "XMLGroups":
        return XMLGroups()

    def add(self, code: str, name: str) -> None:
        self.data[code] = name

    def name(self, code: str) -> str | None:
        if int(code) == 0:
            return None
        return self.data.get(code, None)

    def get(self, grp_code: str, ssgrp_code: str, ssssgrp_code: str) -> list[Group]:
        return [
            Group(grp_code, self.name(grp_code)),
            Group(ssgrp_code, self.name(ssgrp_code)),
            Group(ssssgrp_code, self.name(ssssgrp_code)),
        ]


@dataclass
class XMLTeneur:
    teneur: str
    confiance: str


@dataclass
class XMLComposition:
    content: dict[int, XMLTeneur] = field(default_factory=dict)

    def to_composition(self) -> Composition:
        return Composition.for100g(
            energy=self.to_value(Component.kcal, 328),
            fat=self.to_value(Component.g, 40000),
            fa_saturated=self.to_value(Component.g, 40302),
            fa_mono=self.to_value(Component.g, 40303),
            fa_poly=self.to_value(Component.g, 40304),
            carbohydrate=self.to_value(Component.g, 31000),
            sugars=self.to_value(Component.g, 32000),
            polyols=self.to_value(Component.g, 34000),
            starch=self.to_value(Component.g, 33110),
            fibres=self.to_value(Component.g, 34100),
            protein=self.to_value(Component.g, 25003),
            salt=self.to_value(Component.g, 10004),
            alcohol=self.to_value(Component.g, 60000),
            organic_acids=self.to_value(Component.g, 65000),
            cholesterol=self.to_value(Component.mg, 75100),
            water=self.to_value(Component.g, 400),
            vitamin_A=compute_vitamin_A(
                retinol=self.to_value(Component.ug, 51200),
                beta_carotene=self.to_value(Component.ug, 51330),
            ),
            vitamin_D=self.to_value(Component.ug, 52100),
            vitamin_E=self.to_value(Component.mg, 53100),
            vitamin_K1=self.to_value(Component.ug, 54101),
            vitamin_K2=self.to_value(Component.ug, 54104),
            vitamin_C=self.to_value(Component.mg, 55100),
            vitamin_B1=self.to_value(Component.mg, 56100),
            vitamin_B2=self.to_value(Component.mg, 56200),
            vitamin_B3=self.to_value(Component.mg, 56310),
            vitamin_B5=self.to_value(Component.mg, 56400),
            vitamin_B6=self.to_value(Component.mg, 56500),
            vitamin_B12=self.to_value(Component.ug, 56600),
            vitamin_B9=self.to_value(Component.ug, 56700),
            sodium=self.to_value(Component.mg, 10110),
            magnesium=self.to_value(Component.mg, 10120),
            phosphorus=self.to_value(Component.mg, 10150),
            chloride=self.to_value(Component.mg, 10170),
            potassium=self.to_value(Component.mg, 10190),
            calcium=self.to_value(Component.mg, 10200),
            manganese=self.to_value(Component.mg, 10251),
            iron=self.to_value(Component.mg, 10260),
            copper=self.to_value(Component.mg, 10290),
            zinc=self.to_value(Component.mg, 10300),
            selenium=self.to_value(Component.ug, 10340),
            iodine=self.to_value(Component.ug, 10530),
        )

    def to_value(self, factory, code: int) -> Component:
        constituant = self.content.get(code)
        if constituant is None:
            return Component.unknown()
        try:
            teneur = Fraction(constituant.teneur.replace(",", ".").replace("<", ""))
        except ValueError:
            return Component.unknown()
        result = factory(teneur)
        # TODO: .with_confidence(Confidence.parse(constituant.confiance))
        if "<" in constituant.teneur:
            return result / Scalar.scalar(2)
        return result

    def add(self, const_code: int, value: XMLTeneur) -> None:
        self.content[const_code] = value


@dataclass
class XMLCompositions:
    compositions: dict[str, XMLComposition] = field(
        default_factory=lambda: defaultdict(XMLComposition)
    )

    @staticmethod
    def read_from(filename: str) -> "XMLCompositions":
        root = root_for(filename)
        compositions = XMLCompositions.empty()
        for item in root.findall("COMPO"):
            compositions.add(
                content_of(item, "alim_code"),
                int(content_of(item, "const_code")),
                XMLTeneur(
                    content_of(item, "teneur"), content_of(item, "code_confiance")
                ),
            )
        return compositions

    @staticmethod
    def empty() -> "XMLCompositions":
        return XMLCompositions()

    def add(self, alim_code: str, const_code: int, value: XMLTeneur) -> None:
        self.compositions[alim_code].add(const_code, value)

    def composition_of(self, alim_code: str) -> Composition:
        return self.compositions[alim_code].to_composition()


def _read_aliments(
    path: str, groups: XMLGroups, compositions: XMLCompositions
) -> Iterable[Aliment]:
    content = root_for(f"{path}/alim.xml")
    for item in content.findall("ALIM"):
        alim_code = content_of(item, "alim_code")
        label = content_of(item, "alim_nom_fr")
        grps = groups.get(
            content_of(item, "alim_grp_code"),
            content_of(item, "alim_ssgrp_code"),
            content_of(item, "alim_ssssgrp_code"),
        )
        yield Aliment(
            identifier=Reference(label),
            label=label,
            tags=to_tags(
                source="ciqual",
                code=alim_code,
                name=label,
                groups=grps,
            ),
            composition=compositions.composition_of(alim_code),
        )


def root_for(filename):
    tree = parse(filename)
    return tree.getroot()


def content_of(item, name):
    value = item.find(name).text
    if value:
        return value.strip()
    return value
