# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from pyrecipe.model.tag import Tag, Tags


@dataclass(frozen=True)
class Group:
    code: str
    name: str | None

    @staticmethod
    def to_tags(*groups: Group) -> Tags:
        return Tags.on(
            [Tag(f"group:{g.code}: {g.name}") for g in groups if g.name is not None]
        )


def to_tags(source: str, code: str, name: str, groups: list[Group]) -> Tags:
    return Tags.on(
        [Tag(f"{source}: {name}"), Tag(f"{source}: {code}")]
    ) + Group.to_tags(*groups)
