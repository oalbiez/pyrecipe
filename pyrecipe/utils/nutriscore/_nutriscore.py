# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum

from pyrecipe.model.pantry import Composition
from pyrecipe.model.quantities.energy import Energy
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar

from ._criterion import Criterion


class Nutriscore(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5


@dataclass(frozen=True)
class NutriscoreAnalyse:
    energy: Criterion[Energy]
    sugars: Criterion[Mass]
    fa_saturated: Criterion[Mass]
    sodium: Criterion[Mass]
    vegetables: Criterion[Scalar]
    fibres: Criterion[Mass]
    protein: Criterion[Mass]

    @staticmethod
    def compute_for(
        composition: Composition, vegetable_percent: Scalar
    ) -> NutriscoreAnalyse:
        per100g = composition.normalize()
        return NutriscoreAnalyse(
            energy=Criterion.energy(per100g.energy.value),
            sugars=Criterion.sugars(per100g.sugars.value),
            fa_saturated=Criterion.saturated_fat(per100g.fa_saturated.value),
            sodium=Criterion.sodium(per100g.sodium.value),
            vegetables=Criterion.vegetables(vegetable_percent),
            fibres=Criterion.fibre(per100g.fibres.value),
            protein=Criterion.protein(per100g.protein.value),
        )

    def rank(self) -> int:
        return self._negative_rank() - self._positive_rank()

    def _negative_rank(self) -> int:
        return (
            self.energy.rank
            + self.sugars.rank
            + self.fa_saturated.rank
            + self.sodium.rank
        )

    def _positive_rank(self) -> int:
        if self._should_use_protein():
            return self.vegetables.rank + self.fibres.rank + self.protein.rank
        return self.vegetables.rank + self.fibres.rank

    def _should_use_protein(self) -> bool:
        return self._negative_rank() < 11

    def score(self) -> Nutriscore:
        rank = self.rank()
        if rank < 0:
            return Nutriscore.A
        if rank in range(0, 3):
            return Nutriscore.B
        if rank in range(3, 11):
            return Nutriscore.C
        if rank in range(11, 19):
            return Nutriscore.D
        return Nutriscore.E
