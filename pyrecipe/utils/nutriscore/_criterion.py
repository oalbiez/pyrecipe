# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Generic

from pyrecipe.model.quantities.energy import Energy
from pyrecipe.model.quantities.interval import QTY
from pyrecipe.model.quantities.interval import LowerBound as LB
from pyrecipe.model.quantities.interval import UpperBound as UB
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.scale import Partition as P
from pyrecipe.model.quantities.scale import Scale

ENERGY: Scale[Energy] = Scale.scale(
    P.on(LB.closed(Energy.kj(0)), UB.closed(Energy.kj(335)), 0),
    P.on(LB.opened(Energy.kj(335)), UB.closed(Energy.kj(670)), 1),
    P.on(LB.opened(Energy.kj(670)), UB.closed(Energy.kj(1005)), 1),
    P.on(LB.opened(Energy.kj(1005)), UB.closed(Energy.kj(1340)), 1),
    P.on(LB.opened(Energy.kj(1340)), UB.closed(Energy.kj(1675)), 4),
    P.on(LB.opened(Energy.kj(1675)), UB.closed(Energy.kj(2010)), 5),
    P.on(LB.opened(Energy.kj(2010)), UB.closed(Energy.kj(2345)), 6),
    P.on(LB.opened(Energy.kj(2345)), UB.closed(Energy.kj(2680)), 7),
    P.on(LB.opened(Energy.kj(2680)), UB.closed(Energy.kj(3015)), 8),
    P.on(LB.opened(Energy.kj(3015)), UB.closed(Energy.kj(3350)), 9),
    P.on(LB.opened(Energy.kj(3350)), UB.inf(), 10),
)


SUGARS: Scale[Mass] = Scale.scale(
    P.on(LB.closed(Mass.g("0")), UB.closed(Mass.g("4.5")), 0),
    P.on(LB.opened(Mass.g("4.5")), UB.closed(Mass.g("9")), 1),
    P.on(LB.opened(Mass.g("9")), UB.closed(Mass.g("13.5")), 2),
    P.on(LB.opened(Mass.g("13.5")), UB.closed(Mass.g("18")), 3),
    P.on(LB.opened(Mass.g("18")), UB.closed(Mass.g("22.5")), 4),
    P.on(LB.opened(Mass.g("22.5")), UB.closed(Mass.g("27")), 5),
    P.on(LB.opened(Mass.g("27")), UB.closed(Mass.g("31")), 6),
    P.on(LB.opened(Mass.g("31")), UB.closed(Mass.g("36")), 7),
    P.on(LB.opened(Mass.g("36")), UB.closed(Mass.g("40")), 8),
    P.on(LB.opened(Mass.g("40")), UB.closed(Mass.g("45")), 9),
    P.on(LB.closed(Mass.g("45")), UB.inf(), 10),
)

SATURATEDFAT: Scale[Mass] = Scale.scale(
    P.on(LB.closed(Mass.g("0")), UB.closed(Mass.g("1")), 0),
    P.on(LB.opened(Mass.g("1")), UB.closed(Mass.g("2")), 1),
    P.on(LB.opened(Mass.g("2")), UB.closed(Mass.g("3")), 2),
    P.on(LB.opened(Mass.g("3")), UB.closed(Mass.g("4")), 3),
    P.on(LB.opened(Mass.g("4")), UB.closed(Mass.g("5")), 4),
    P.on(LB.opened(Mass.g("5")), UB.closed(Mass.g("6")), 5),
    P.on(LB.opened(Mass.g("6")), UB.closed(Mass.g("7")), 6),
    P.on(LB.opened(Mass.g("7")), UB.closed(Mass.g("8")), 7),
    P.on(LB.opened(Mass.g("8")), UB.closed(Mass.g("9")), 8),
    P.on(LB.opened(Mass.g("9")), UB.closed(Mass.g("10")), 9),
    P.on(LB.closed(Mass.g("10")), UB.inf(), 10),
)

SODIUM: Scale[Mass] = Scale.scale(
    P.on(LB.closed(Mass.mg("0")), UB.closed(Mass.mg("90")), 0),
    P.on(LB.closed(Mass.mg("90")), UB.closed(Mass.mg("180")), 1),
    P.on(LB.closed(Mass.mg("180")), UB.closed(Mass.mg("270")), 2),
    P.on(LB.closed(Mass.mg("270")), UB.closed(Mass.mg("360")), 3),
    P.on(LB.closed(Mass.mg("360")), UB.closed(Mass.mg("450")), 4),
    P.on(LB.closed(Mass.mg("450")), UB.closed(Mass.mg("540")), 5),
    P.on(LB.closed(Mass.mg("540")), UB.closed(Mass.mg("630")), 6),
    P.on(LB.closed(Mass.mg("630")), UB.closed(Mass.mg("720")), 7),
    P.on(LB.closed(Mass.mg("720")), UB.closed(Mass.mg("810")), 8),
    P.on(LB.closed(Mass.mg("810")), UB.closed(Mass.mg("900")), 9),
    P.on(LB.closed(Mass.mg("900")), UB.inf(), 10),
)

FIBRE: Scale[Mass] = Scale.scale(
    P.on(LB.closed(Mass.g("0")), UB.closed(Mass.g("0.9")), 0),
    P.on(LB.opened(Mass.g("0.9")), UB.closed(Mass.g("1.9")), 1),
    P.on(LB.opened(Mass.g("1.9")), UB.closed(Mass.g("2.8")), 2),
    P.on(LB.opened(Mass.g("2.8")), UB.closed(Mass.g("3.7")), 3),
    P.on(LB.opened(Mass.g("3.7")), UB.closed(Mass.g("4.7")), 4),
    P.on(LB.closed(Mass.g("4.7")), UB.inf(), 5),
)

PROTEIN: Scale[Mass] = Scale.scale(
    P.on(LB.closed(Mass.g("0")), UB.closed(Mass.g("1.6")), 0),
    P.on(LB.opened(Mass.g("1.6")), UB.closed(Mass.g("3.2")), 1),
    P.on(LB.opened(Mass.g("3.2")), UB.closed(Mass.g("4.8")), 2),
    P.on(LB.opened(Mass.g("4.8")), UB.closed(Mass.g("6.4")), 3),
    P.on(LB.opened(Mass.g("6.4")), UB.closed(Mass.g("8")), 3),
    P.on(LB.closed(Mass.g("8")), UB.inf(), 5),
)

VEGETABLES: Scale[Scalar] = Scale.scale(
    P.on(LB.closed(Scalar.scalar("0")), UB.closed(Scalar.scalar("0.4")), 0),
    P.on(LB.opened(Scalar.scalar("0.4")), UB.closed(Scalar.scalar("0.6")), 1),
    P.on(LB.opened(Scalar.scalar("0.6")), UB.closed(Scalar.scalar("0.8")), 2),
    P.on(LB.opened(Scalar.scalar("0.8")), UB.closed(Scalar.scalar("1")), 5),
)


@dataclass(frozen=True)
class Criterion(Generic[QTY]):
    rank: int
    raw: QTY
    scale: Scale

    @staticmethod
    def create(raw: QTY, scale: Scale) -> Criterion[QTY]:
        return Criterion(scale.rank(raw), raw, scale)

    @staticmethod
    def energy(raw: Energy) -> Criterion[Energy]:
        return Criterion.create(raw, ENERGY)

    @staticmethod
    def sugars(raw: Mass) -> Criterion[Mass]:
        return Criterion.create(raw, SUGARS)

    @staticmethod
    def saturated_fat(raw: Mass) -> Criterion[Mass]:
        return Criterion.create(raw, SATURATEDFAT)

    @staticmethod
    def sodium(raw: Mass) -> Criterion[Mass]:
        return Criterion.create(raw, SODIUM)

    @staticmethod
    def fibre(raw: Mass) -> Criterion[Mass]:
        return Criterion.create(raw, FIBRE)

    @staticmethod
    def protein(raw: Mass) -> Criterion[Mass]:
        return Criterion.create(raw, PROTEIN)

    @staticmethod
    def vegetables(raw: Scalar) -> Criterion[Scalar]:
        return Criterion.create(raw, VEGETABLES)
