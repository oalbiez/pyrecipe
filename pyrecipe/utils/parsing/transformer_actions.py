# -*- coding: utf-8 -*-
from collections.abc import Callable
from typing import Any, TypeVar

U = TypeVar("U")  # pylint: disable=invalid-name
V = TypeVar("V")  # pylint: disable=invalid-name

Transformer = Callable[[U], V]


def compose(*functors: Transformer) -> Callable[[Any, U], V]:
    "Compose actions in sequence"

    def apply(_, value):
        result = value
        for functor in functors:
            result = functor(result)
        return result

    return apply


def first() -> Transformer:
    "Return the first value"
    return lambda value: value[0]


def const(value) -> Transformer:
    "Return a constant value"
    return lambda *a, **kw: value


def single(functor) -> Transformer:
    return lambda value: functor(value[0])


def quantity() -> Transformer:
    def apply(value):
        (magnitude, factory) = value
        return factory(magnitude)

    return apply


def token_value() -> Transformer:
    return lambda value: value[0].value


def to_tuple(name) -> Transformer:
    def apply(value):
        return (name, value)

    return apply


def args(functor) -> Transformer:
    return lambda value: functor(*value)


def kwargs(functor) -> Transformer:
    return lambda value: functor(**value)


def strip() -> Transformer:
    return lambda value: value.strip()


def join(separator: str) -> Transformer:
    def apply(value):
        return separator.join(value)

    return apply
