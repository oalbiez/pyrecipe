# -*- coding: utf-8 -*-
from lark.indenter import Indenter


class TreeIndenter(Indenter):
    NL_type = "_NL"
    OPEN_PAREN_types: list[str] = []
    CLOSE_PAREN_types: list[str] = []
    INDENT_type = "_INDENT"
    DEDENT_type = "_DEDENT"
    tab_len = 8
    always_accept = (NL_type,)  # Needed to typecheck...
