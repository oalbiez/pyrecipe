# -*- coding: utf-8 -*-
from pathlib import Path

import requests
from bs4 import BeautifulSoup  # type: ignore


def header_of(url):
    return requests.head(url, timeout=300)


def soup_of_url(url) -> BeautifulSoup:
    return soup_of_content(requests.get(url, timeout=300).text)


def soup_of_path(path: Path) -> BeautifulSoup:
    return soup_of_content(path.read_text())


def soup_of_content(content: str) -> BeautifulSoup:
    return BeautifulSoup(content, "html.parser")


def text_of(elmt) -> str | None:
    if elmt:
        text = elmt.string
        if text:
            return str(text).strip()
        return str(" ".join(elmt.stripped_strings))
    return None


def href_of(elmt) -> str | None:
    return get_of(elmt, "href")


def get_of(elmt, key: str) -> str | None:
    if hasattr(elmt, "get"):
        return elmt.get(key)
    return None


def cleanup(soup: BeautifulSoup) -> BeautifulSoup:
    for tag in soup.find_all("style"):
        tag.decompose()
    for tag in soup.find_all("script"):
        tag.decompose()
    for tag in soup.find_all("link", type="text/css"):
        tag.decompose()
    return soup
