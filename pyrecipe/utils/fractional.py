# -*- coding: utf-8 -*-
from __future__ import annotations

from decimal import Decimal
from fractions import Fraction


def _powers_of_only_2_or_5(number: int) -> bool:
    for divisor in [2, 5]:
        while not number % divisor:
            number = number // divisor
    return number == 1


def fractional_view(value: Fraction, ndigits: int | None = None) -> str:
    if value.denominator in [2, 3, 4, 5, 8]:
        return f"{value.numerator}/{value.denominator}"
    return decimal_view(value, ndigits)


def decimal_view(value: Fraction, ndigits: int | None = None) -> str:
    result = Decimal(value.numerator) / Decimal(value.denominator)
    if _powers_of_only_2_or_5(value.denominator):
        return str(result)
    if ndigits:
        result = round(result, ndigits)
    return str(result)
