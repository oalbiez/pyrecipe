# -*- coding: utf-8 -*-
from __future__ import annotations

import json

from pyrecipe.model.book import Name, Recipe


def json_to_recipe(content: str) -> Recipe:
    structure = json.loads(content)
    print(structure["name"])
    return Recipe(name=Name(structure["name"]))
