# -*- coding: utf-8 -*-
from __future__ import annotations

import itertools
from collections.abc import Iterable
from typing import cast
from urllib.parse import urlparse

from pyrecipe.model.book import (
    Durations,
    Ingredient,
    Lang,
    Name,
    Qty,
    Recipe,
    Servings,
    Source,
    Step,
)
from pyrecipe.model.quantities.duration import Duration
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.volume import Volume
from pyrecipe.model.reference import Reference
from pyrecipe.model.tag import Diets, Tag, Tags
from pyrecipe.utils.fuzzy import dispatch_ingredients
from pyrecipe.utils.fuzzy.tag import create_tag
from pyrecipe.utils.soup import BeautifulSoup, get_of, href_of, text_of

from ._types import URL
from .scraper import ScraperDriver

_lang = Lang("fr")


def _is_recipe(soup: BeautifulSoup) -> bool:
    return soup.find("div", class_="wprm-recipe-meta-container") is not None


def _reference(url: URL) -> Reference:
    path = urlparse(url.value).path.strip("/")
    return Reference(f"menuvegetarien.{path}")


def _name(soup: BeautifulSoup) -> Name:
    value = text_of(soup.find("h1", class_="elementor-heading-title"))
    if value is None:
        raise RuntimeError("Scraper broken: cannot find the name of the recipe")
    return Name(value)


def _servings(soup: BeautifulSoup) -> Servings:
    value = text_of(soup.find("span", class_="wprm-recipe-servings"))
    if value is None:
        raise RuntimeError("Scraper broken: cannot find the servings of the recipe")
    return Servings(int(value))


def _durations(soup: BeautifulSoup) -> Durations:
    def get(tag: str) -> Duration | None:
        label = text_of(soup.find("span", class_=f"wprm-recipe-{tag}-time-label"))
        value = text_of(soup.find("span", class_=f"wprm-recipe-{tag}_time"))
        unit = text_of(soup.find("span", class_=f"wprm-recipe-{tag}_time-unit"))
        if label is None or value is None or unit is None:
            return None
        if unit in ["minute", "minutes"]:
            return Duration.minutes(int(value))
        if unit in ["heure", "heures"]:
            return Duration.hours(int(value))
        if unit in ["day", "days"]:
            return Duration.days(int(value))
        raise RuntimeError(f"Scraper broken: unknown unit {unit}")

    return Durations(
        waiting=get("custom"),
        preparing=get("prep"),
        cooking=get("cook"),
    )


_ignored = set(
    [
        "Cuisine plaisir",
        "Cuisine pour tous",
        "Cuisine rapide",
        "Cuisine simple",
        "Detox",
        "Régime",
        "Sucré salé",
        "Veggie kids",
    ]
)


def _tags(soup: BeautifulSoup) -> Tags:
    def get(name: str) -> Iterable[str]:
        for tag in soup.find_all("span", class_=name):
            for link in tag.find_all("a"):
                text = text_of(link)
                if text is not None:
                    yield text

    values = itertools.chain(
        get("wprm-recipe-course"),
        get("wprm-recipe-cuisine"),
        get("wprm-recipe-keyword"),
    )

    tags = [Diets.VEGETARIAN.value]
    for value in values:
        if value not in _ignored:
            tag = create_tag(value)
            tags.append(tag if tag else Tag.tag(value.lower()))
    return Tags.on(sorted(tags))


qty_factory = {
    None: Scalar.scalar,
    "kg": Mass.kg,
    "g": Mass.g,
    "l": Volume.l,
    "dl": Volume.dl,
    "cl": Volume.cl,
    "ml": Volume.ml,
    "càc": Volume.tsp,
    "càs": Volume.tbs,
    "cas": Volume.tbs,
}


def _steps(soup: BeautifulSoup) -> list[Step]:
    def ingredients():
        for item in soup.find_all("li", class_="wprm-recipe-ingredient"):
            magnitude = text_of(
                item.find("span", class_="wprm-recipe-ingredient-amount")
            )
            unit = text_of(item.find("span", class_="wprm-recipe-ingredient-unit"))
            label = text_of(item.find("span", class_="wprm-recipe-ingredient-name"))
            if magnitude is None:
                raise RuntimeError("Scraper broken: unknown ingredient quantity")
            if label is None:
                raise RuntimeError("Scraper broken: unknown ingredient label")
            magnitude = magnitude.replace(",", ".")
            # TODO: sometime magnitude contains unit and is not parsable
            if unit in qty_factory:
                yield Ingredient(
                    cast(Qty, qty_factory[unit](magnitude)),
                    label,
                    Reference.undefined(),
                )
            elif unit is not None:
                yield Ingredient(
                    Scalar.scalar(magnitude), unit + " " + label, Reference.undefined()
                )
            else:
                raise RuntimeError(f"Scraper broken: unknown unit {unit}")

    def instructions():
        found = False
        for item in soup.find_all("div", class_="wprm-recipe-instruction-text"):
            content = text_of(item)
            if content is not None:
                found = True
                yield Step([], parts=[content])
            else:
                raise RuntimeError("Scraper broken: cannot extract instruction")
        if not found:
            raise RuntimeError("Scraper broken: found no instruction")

    return dispatch_ingredients(_lang, list(ingredients()), list(instructions()))


class MenuVegetarienDriver(ScraperDriver):
    """Scrap recipes from https://menu-vegetarien.com/."""

    def startup(self) -> list[URL]:
        return [
            URL("https://menu-vegetarien.com/recettes-vegetariennes"),
            URL("https://menu-vegetarien.com/recettes-vegan"),
        ]

    def recipes(self, url: URL, soup: BeautifulSoup) -> Iterable[Recipe]:
        if _is_recipe(soup):
            return [
                Recipe(
                    identifier=_reference(url),
                    name=_name(soup),
                    source=Source(url.value),
                    lang=_lang,
                    servings=_servings(soup),
                    durations=_durations(soup),
                    tags=_tags(soup),
                    steps=_steps(soup),
                )
            ]
        return []

    def links(self, _: URL, soup: BeautifulSoup) -> Iterable[URL]:
        if _is_recipe(soup):
            return []

        urls = []
        for item in soup.find_all("h3", class_="elementor-post__title"):
            addr = href_of(item.a)
            if addr is not None:
                urls.append(URL(addr))

        link_next = href_of(soup.find("link", rel="next"))
        if link_next is not None:
            urls.append(URL(link_next))
        return urls

    @staticmethod
    def modified_date(soup: BeautifulSoup) -> str:
        return (
            get_of(soup.find("meta", property="article:modified_time"), "content") or ""
        )
