# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Iterable

from bs4 import BeautifulSoup  # type: ignore

from pyrecipe.model.book import Recipe
from pyrecipe.pipeline.recipes.providers.provider import RecipeProvider

from ._types import URL


class Getter(ABC):
    @abstractmethod
    def get(self, url: URL) -> str | None:
        """Get content of url"""


class Reporter(ABC):
    @abstractmethod
    def message(self, content: str) -> None:
        """Report message"""

    @abstractmethod
    def processing(self, url: URL) -> None:
        """Report the processing an url"""

    @abstractmethod
    def skip(self, url: URL, message: str) -> None:
        """Report an error processing an url"""

    @abstractmethod
    def error(self, url: URL, message: str) -> None:
        """Report an error processing an url"""

    @abstractmethod
    def status(self, visited: int, errors: int, todo: int, recipes: int) -> None:
        """Report actual status"""


class ScraperDriver(ABC):
    @abstractmethod
    def startup(self) -> list[URL]:
        """Gets startup URLs"""

    @abstractmethod
    def recipes(self, url: URL, soup: BeautifulSoup) -> Iterable[Recipe]:
        """Scrap recipes fron URL"""

    @abstractmethod
    def links(self, url: URL, soup: BeautifulSoup) -> Iterable[URL]:
        """Scrap links"""


class Scraper(RecipeProvider):
    def __init__(self, getter: Getter, reporter: Reporter, driver: ScraperDriver):
        self.getter = getter
        self.reporter = reporter
        self.driver = driver
        self.start_urls: list[URL] = driver.startup()
        self.max_url: int | None = None

    def start(self, start_url: URL) -> Scraper:
        self.start_urls = [start_url]
        return self

    def limit_urls(self, value: int) -> Scraper:
        self.max_url = value
        return self

    def provide(self) -> Iterable[Recipe]:
        todo = list(self.start_urls)
        visited: set[URL] = set()
        found = 0
        errors: list[URL] = []
        while len(todo) > 0:
            self.reporter.status(len(visited), len(errors), len(todo), found)
            current = todo.pop(0)
            if current in visited:
                self.reporter.skip(current, "already visited")
                continue
            self.reporter.processing(current)
            content = self.getter.get(current)
            if content is None:
                self.reporter.error(current, "not found")
                errors.append(current)
                continue
            visited.add(current)
            try:
                # pylint: disable=broad-except
                soup = BeautifulSoup(content, "html.parser")
                todo.extend(self.driver.links(current, soup))
                for recipe in self.driver.recipes(current, soup):
                    found += 1
                    yield recipe
            except Exception as exception:
                self.reporter.error(current, str(exception))
                errors.append(current)
            if self.max_url is not None and len(visited) >= self.max_url:
                break
        self.reporter.message("Errors:")
        for url in errors:
            self.reporter.message(f"  - {url.value}")
