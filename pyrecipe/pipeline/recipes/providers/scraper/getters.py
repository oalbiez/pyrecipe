# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from sqlite3 import Connection, connect

import requests

from ._types import URL
from .scraper import Getter


class Cache(ABC):
    @abstractmethod
    def put(self, url: URL, content: str) -> str:
        """Put content in cache"""

    @abstractmethod
    def get(self, url: URL) -> str | None:
        """Get content from cache"""


class CacheWrite(Getter):
    _delegate: Getter
    _cache: Cache

    def __init__(self, delegate: Getter, cache: Cache):
        self._delegate = delegate
        self._cache = cache

    def get(self, url: URL) -> str | None:
        """Get content of url"""
        result = self._delegate.get(url)
        if result is not None:
            self._cache.put(url, result)
        return result


class CacheRead(Getter):
    _cache: Cache

    def __init__(self, cache: Cache):
        self._cache = cache

    def get(self, url: URL) -> str | None:
        return self._cache.get(url)


class Http(Getter):
    def get(self, url: URL) -> str | None:
        """Get content of url"""
        response = requests.get(url.value, timeout=300)
        if response.ok:
            return response.text
        return None


class SqliteCache(Cache):
    _connection: Connection

    def __init__(self, db_uri: str):
        self._connection = connect(db_uri, uri=True)
        self._ensure_schema()

    def __del__(self):
        self._connection.close()

    def put(self, url: URL, content: str) -> str:
        with self._connection:
            self._connection.execute(
                "INSERT OR REPLACE INTO cache (key, value) values (?,?)",
                (url.value, content),
            )
        return content

    def get(self, url: URL) -> str | None:
        for row in self._connection.execute(
            "SELECT value FROM cache WHERE (key = ?)", (url.value,)
        ):
            return row[0]
        return None

    def _ensure_schema(self) -> None:
        cursor = self._connection.execute(
            """
            SELECT name FROM sqlite_master
                WHERE type='table' AND name='cache';
            """
        )
        has_table = cursor.fetchall()
        if has_table:
            return
        self._connection.executescript(
            """
            CREATE TABLE IF NOT EXISTS cache
                (key TEXT PRIMARY KEY UNIQUE NOT NULL, value TEXT);
            """
        )
