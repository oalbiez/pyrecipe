# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from typing import Final, cast
from urllib.parse import urlparse

from pyrecipe.model.book import (
    Durations,
    Ingredient,
    Lang,
    Name,
    Qty,
    Recipe,
    Servings,
    Source,
    Step,
)
from pyrecipe.model.quantities.duration import Duration
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.volume import Volume
from pyrecipe.model.reference import Reference
from pyrecipe.model.tag import Tag, Tags
from pyrecipe.utils.fuzzy.tag import create_tag
from pyrecipe.utils.soup import BeautifulSoup, text_of

from ._types import URL
from .scraper import ScraperDriver

_BASE: Final[str] = "https://www.trail.recipes"
_lang = Lang("en")


def _is_recipe(url: URL) -> bool:
    return "/recipes/" in url.value


def _reference(url: URL) -> Reference:
    path = urlparse(url.value).path.strip("/")
    if path.startswith("recipes/"):
        path = path[8:]
    return Reference(f"trails.{path}")


def _name(soup: BeautifulSoup) -> Name:
    value = text_of(soup.find("div", {"data-node": "604498dfe74e7"}))
    if value is None:
        raise RuntimeError("Scraper broken: cannot find the name of the recipe")
    return Name(value)


def _get_servings(soup: BeautifulSoup) -> Servings:
    for tag in soup.find_all("div", class_="fl-html"):
        text = text_of(tag)
        if text is not None:
            if text.startswith("servings"):
                content = text_of(tag.span)
                if content is not None:
                    return Servings(int(content))
    raise RuntimeError("Scraper broken: cannot find the servings of the recipe")


def _durations(soup: BeautifulSoup) -> Durations:
    for tag in soup.find_all("div", class_="fl-html"):
        text = text_of(tag)
        if text is not None:
            if "cooking time:" in text:
                spans = list(tag.find_all("span"))
                if len(spans) >= 3:
                    content = text_of(spans[2])
                    if content is not None:
                        return Durations(cooking=Duration.minutes(content))
    raise RuntimeError("Scraper broken: cannot find the cooking time of the recipe")


def _tags(soup: BeautifulSoup) -> Tags:
    def values() -> Iterable[str]:
        for tag in soup.find_all("div", class_="fl-html"):
            text = text_of(tag)
            if text is not None:
                if text.startswith("Category"):
                    return [v.strip() for v in text[9:].split(",")]
        raise RuntimeError("Scraper broken: cannot find tags")

    def to_tag(value: str) -> Tag:
        tag = create_tag(value)
        return tag if tag else Tag.tag(value.lower())

    return Tags.on(sorted(to_tag(v) for v in values()))


qty_factory = {
    "teaspoon": Volume.tsp,
    "teaspoons": Volume.tsp,
    "cup": Volume.cup,
    "cups": Volume.cup,
    "handful": Volume.handful,
    "handfuls": Volume.handful,
    "tablespoons": Volume.tbs,
    "tablespoon": Volume.tbs,
    "pinch": Volume.pinch,
    "kg": Mass.kg,
    "kgs": Mass.kg,
    "g": Mass.g,
    "gs": Mass.g,
}


def _steps(soup: BeautifulSoup) -> list[Step]:
    def parse_qty(value: str) -> Qty:
        if value and all(c in "0123456789/. " for c in value):
            return Scalar.scalar(value)
        parts = value.split(" ")
        if len(parts) == 1:
            if value.strip() == "":
                return Volume.pinch(1)
            if value in qty_factory:
                return cast(Qty, qty_factory[value](1))
            return Scalar.scalar(value)
        if len(parts) == 2:
            unit = parts[1].strip()
            magnitude = parts[0].strip()
            if unit in qty_factory:
                return cast(Qty, qty_factory[unit](magnitude))
        raise RuntimeError(f"Scraper broken: invalid quantity {value}")

    def aggregate(tag) -> str:
        result: list[str] = []
        while tag and tag.name != "strong":
            result.append(text_of(tag) or "")
            tag = tag.next_sibling
        return " ".join(result)

    def remove_suffix(suffix: str, value: str) -> str:
        return value[0 : -len(suffix)].strip()

    def ingredients():
        found = False
        for item in soup.find_all("div", {"data-node": "569d22925beeb"}):
            for strong in item.find_all("strong"):
                qty = text_of(strong)
                label = aggregate(strong.next_sibling)
                if qty is not None:
                    for suffix in [
                        "bunch",
                        "bundle",
                        "can",
                        "clove",
                        "cloves",
                        "pack",
                        "packet",
                        "pouch",
                    ]:
                        if qty.endswith(suffix):
                            qty = remove_suffix(suffix, qty)
                            label = suffix + " " + label

                    found = True
                    yield Ingredient(parse_qty(qty), label, Reference.undefined())
                else:
                    raise RuntimeError("Scraper broken: cannot extract ingredient")

        if not found:
            raise RuntimeError("Scraper broken: found no ingredient")

    at_home = text_of(soup.find("div", {"data-node": "569d22925c036"})) or ""
    result = [Step(list(ingredients()), [at_home])]
    on_trail = text_of(soup.find("div", {"data-node": "569d22925c181"}))
    if on_trail and on_trail != "–":
        result.append(Step([], [on_trail]))
    return result


class TrailRecipesDriver(ScraperDriver):
    """Scrap recipes from https://www.trail.recipes."""

    def startup(self) -> list[URL]:
        return [URL(_BASE + "/recipe-collection/dehydrated-backpacking-meals/")]

    def recipes(self, url: URL, soup: BeautifulSoup) -> Iterable[Recipe]:
        if _is_recipe(url):
            return [
                Recipe(
                    identifier=_reference(url),
                    name=_name(soup),
                    source=Source(url.value),
                    lang=_lang,
                    servings=_get_servings(soup),
                    durations=_durations(soup),
                    tags=_tags(soup),
                    steps=_steps(soup),
                )
            ]
        return []

    def links(self, url: URL, soup: BeautifulSoup) -> Iterable[URL]:
        if _is_recipe(url):
            return []
        return list(
            filter(
                _is_recipe,
                [
                    URL(tag.a.get("href"))
                    for tag in soup.find_all("h4", class_="pt-cv-title")
                ],
            )
        )
