# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from typing import Final

from pyrecipe.model.book import Lang, Recipe
from pyrecipe.utils.soup import BeautifulSoup

from ._types import URL
from .scraper import ScraperDriver

_BASE: Final[str] = "https://www.backpackingchef.com/"
_lang = Lang("en")


def _is_recipe(_: URL) -> bool:
    return False


class BackpackingchefDriver(ScraperDriver):
    """Scrap recipes from https://www.backpackingchef.com/."""

    def startup(self) -> list[URL]:
        return [URL(_BASE)]

    def recipes(self, _1: URL, _2: BeautifulSoup) -> Iterable[Recipe]:
        return []

    def links(self, url: URL, soup: BeautifulSoup) -> Iterable[URL]:
        if _is_recipe(url):
            return []
        return list(
            filter(
                lambda url: url.value.startswith(_BASE),
                [URL(tag.get("href")) for tag in soup.find_all("a")],
            )
        )
