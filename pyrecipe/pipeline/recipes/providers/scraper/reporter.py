# -*- coding: utf-8 -*-
from __future__ import annotations

from sys import stdout
from typing import TextIO

from ._types import URL
from .scraper import Reporter


class ConsoleReporter(Reporter):
    _output: TextIO

    @staticmethod
    def to_stdout() -> ConsoleReporter:
        return ConsoleReporter(stdout)

    def __init__(self, ouput: TextIO):
        self._output = ouput

    def message(self, content: str) -> None:
        self._output.write(content + "\n")

    def processing(self, url: URL) -> None:
        self._output.write(f"Processing: {url.value}\n")

    def skip(self, url: URL, message: str) -> None:
        self._output.write(f"Skipping {url.value}: {message}\n")

    def error(self, url: URL, message: str) -> None:
        self._output.write(f"Error with {url.value}: {message}\n")

    def status(self, visited: int, errors: int, todo: int, recipes: int) -> None:
        if recipes > 0:
            self._output.write(
                "status: "
                f"visited={visited}, errors={errors}, "
                f"todo={todo}, recipes={recipes}\n"
            )
        else:
            self._output.write(
                f"status: visited={visited}, errors={errors}, todo={todo}\n"
            )
