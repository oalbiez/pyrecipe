# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe

from .provider import RecipeProvider


class SequentialRecipeProvider(RecipeProvider):
    def __init__(self, providers: list[RecipeProvider]):
        self.__providers = providers

    @staticmethod
    def chain(*transformers: RecipeProvider) -> SequentialRecipeProvider:
        return SequentialRecipeProvider(list(transformers))

    def provide(self) -> Iterable[Recipe]:
        for provider in self.__providers:
            yield from provider.provide()
