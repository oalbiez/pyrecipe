# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from textwrap import dedent

from pyrecipe.model.book import Recipe
from pyrecipe.model.dsl import DSL
from pyrecipe.pipeline.recipes.providers.provider import RecipeProvider


class MemoryRecipeProvider(RecipeProvider):
    def __init__(self, content: str):
        self.content = dedent(content)

    def provide(self) -> Iterable[Recipe]:
        return DSL.parse_book(self.content).recipes
