# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe
from pyrecipe.pipeline.recipes.providers.provider import RecipeProvider


class NoneRecipeProvider(RecipeProvider):
    def provide(self) -> Iterable[Recipe]:
        return []
