# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from pathlib import Path

from pyrecipe.model.book import Recipe
from pyrecipe.model.dsl import DSL
from pyrecipe.pipeline.recipes.providers.provider import RecipeProvider


class BookfileRecipeProvider(RecipeProvider):
    def __init__(self, bookfile: Path):
        self.bookfile = bookfile

    def provide(self) -> Iterable[Recipe]:
        return DSL.parse_bookfile(self.bookfile).recipes
