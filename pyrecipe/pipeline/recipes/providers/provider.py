# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Iterable

from pyrecipe.model.book import Recipe


class RecipeProvider(ABC):
    @abstractmethod
    def provide(self) -> Iterable[Recipe]:
        """Provide recipes"""
