# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector
from pyrecipe.pipeline.recipes.collectors.void import VoidRecipeCollector
from pyrecipe.pipeline.recipes.providers.none import NoneRecipeProvider
from pyrecipe.pipeline.recipes.providers.provider import RecipeProvider
from pyrecipe.pipeline.recipes.transformers.composite import CompositeRecipeTransformer
from pyrecipe.pipeline.recipes.transformers.transformer import RecipeTransformer


class Pipeline:
    def __init__(
        self,
        provider: RecipeProvider | None = None,
        transformers: list[RecipeTransformer] | None = None,
        collector: RecipeCollector | None = None,
    ):
        self.__provider = provider or NoneRecipeProvider()
        self.__transformers = transformers or []
        self.__collector = collector or VoidRecipeCollector()

    def from_(self, provider: RecipeProvider) -> Pipeline:
        return Pipeline(provider, self.__transformers, self.__collector)

    def to_(self, collector: RecipeCollector) -> Pipeline:
        return Pipeline(self.__provider, self.__transformers, collector)

    def apply(self, transformer: RecipeTransformer) -> Pipeline:
        return Pipeline(self.__provider, [transformer], self.__collector)

    def append(self, transformer: RecipeTransformer) -> Pipeline:
        return Pipeline(
            self.__provider, self.__transformers + [transformer], self.__collector
        )

    def run(self) -> None:
        self.__collector.collect(
            CompositeRecipeTransformer(self.__transformers).process(
                self.__provider.provide()
            )
        )
