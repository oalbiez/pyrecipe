# -*- coding: utf-8 -*-
from __future__ import annotations

import google_translate_py  # type: ignore

from pyrecipe.model.book import Lang

from ._translate import Action, Translate


class GoogleDriver(Translate.Driver):
    """Use google translate."""

    google: google_translate_py.Translator

    def __init__(self):
        self.google = google_translate_py.Translator()

    def translate(self, source: Lang, target: Lang) -> Action:
        def action(value: str) -> str:
            return self.google.translate(value, source, target).strip()

        return action
