# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Callable

from pyrecipe.model.book import Lang, Recipe

from ..by_recipe_transformer import ByRecipeTransformer

Action = Callable[[str], str]


class Translate(ByRecipeTransformer):
    class Driver(ABC):
        @abstractmethod
        def translate(self, source: Lang, target: Lang) -> Action:
            """Translate the text"""

    def __init__(self, driver: Driver, lang: Lang):
        self.__driver = driver
        self.__lang = lang

    @staticmethod
    def on(driver: Driver) -> Translate:
        # pylint: disable=invalid-name
        return Translate(driver, Lang("fr"))

    def lang(self, lang: Lang) -> Translate:
        return Translate(self.__driver, lang)

    def transform(self, recipe: Recipe) -> Recipe:
        return recipe.transform_text(
            self.__driver.translate(recipe.lang, self.__lang)
        ).update(lang=self.__lang)
