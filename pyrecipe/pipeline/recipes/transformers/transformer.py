# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Iterable

from pyrecipe.model.book import Recipe


class RecipeTransformer(ABC):
    @abstractmethod
    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        """Transform recipes"""
