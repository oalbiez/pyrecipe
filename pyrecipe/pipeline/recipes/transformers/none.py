# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe

from .transformer import RecipeTransformer


class NoneRecipeTransformer(RecipeTransformer):
    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        return recipes
