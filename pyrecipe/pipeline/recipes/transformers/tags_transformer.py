# -*- coding: utf-8 -*-
from __future__ import annotations

from itertools import filterfalse
from re import sub

from pyrecipe.model.book import Recipe
from pyrecipe.model.tag import Tag, Tags
from pyrecipe.utils.predicates import Predicate

from .by_recipe_transformer import ByRecipeTransformer
from .recipes_actions import CompositeAction, RecipeAction


class TagTransformer(ByRecipeTransformer):
    def __init__(self, actions: CompositeAction):
        self.__actions = actions

    @staticmethod
    def tags() -> TagTransformer:
        return TagTransformer(CompositeAction.empty())

    def add(self, tags: Tags) -> TagTransformer:
        def action(recipe: Recipe) -> Recipe:
            new_tags = list(recipe.tags)
            for tag in tags:
                if tag not in new_tags:
                    new_tags.append(tag)
            return recipe.update(tags=Tags.on(new_tags))

        return self.__append_action(action)

    def remove(self, predicate: Predicate[Tag]) -> TagTransformer:
        def action(recipe: Recipe) -> Recipe:
            return recipe.update(
                tags=Tags.on(list(filterfalse(predicate, recipe.tags)))
            )

        return self.__append_action(action)

    def sed(self, pattern: str, replace: str) -> TagTransformer:
        def action(recipe: Recipe) -> Recipe:
            return recipe.update(
                tags=Tags.on(
                    [Tag(sub(pattern, replace, tag.content)) for tag in recipe.tags]
                )
            )

        return self.__append_action(action)

    def replace(self, values: set[str], tag: Tag) -> TagTransformer:
        def update(current: Tag) -> Tag:
            if current.content in values:
                return tag
            return current

        def action(recipe: Recipe) -> Recipe:
            return recipe.update(tags=Tags.on([update(tag) for tag in recipe.tags]))

        return self.__append_action(action)

    def transform(self, recipe: Recipe) -> Recipe:
        return self.__actions.process(recipe)

    def __append_action(self, action: RecipeAction) -> TagTransformer:
        return TagTransformer(self.__actions.append_action(action))
