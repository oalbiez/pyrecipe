# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable

from pyrecipe.model.book import Recipe

RecipeAction = Callable[[Recipe], Recipe]


class CompositeAction:
    def __init__(self, actions: list[RecipeAction]):
        self.__actions = actions

    @staticmethod
    def empty() -> CompositeAction:
        return CompositeAction([])

    def append_action(self, action: RecipeAction) -> CompositeAction:
        return CompositeAction(self.__actions + [action])

    def process(self, recipe: Recipe) -> Recipe:
        for action in self.__actions:
            recipe = action(recipe)
        return recipe
