# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe

from .transformer import RecipeTransformer


class CompositeRecipeTransformer(RecipeTransformer):
    def __init__(self, transformers: list[RecipeTransformer]):
        self.__transformers = transformers

    @staticmethod
    def chain(*transformers: RecipeTransformer) -> CompositeRecipeTransformer:
        return CompositeRecipeTransformer(list(transformers))

    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        result = recipes
        for transformer in self.__transformers:
            result = transformer.process(result)
        return result
