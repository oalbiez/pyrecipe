# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable, Iterable
from typing import Any

from pyrecipe.model.book import Recipe

from .transformer import RecipeTransformer


def title(recipe: Recipe) -> str:
    return recipe.name


class SortedRecipeTransformer(RecipeTransformer):
    def __init__(self, keys: list[Callable[[Recipe], Any]], reverse: bool = False):
        self.__keys = keys
        self.__reverse = reverse

    def by_title(self) -> SortedRecipeTransformer:
        return SortedRecipeTransformer(self.__keys + [title], self.__reverse)

    def reverse(self) -> SortedRecipeTransformer:
        return SortedRecipeTransformer(self.__keys, self.__reverse)

    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        return sorted(recipes, key=self.__key, reverse=self.__reverse)

    def __key(self, recipe: Recipe) -> Any:
        return tuple(k(recipe) for k in self.__keys)
