# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe
from pyrecipe.utils.predicates import Predicate

from .transformer import RecipeTransformer


class FilteredRecipeTransformer(RecipeTransformer):
    def __init__(self, predicate: Predicate[Recipe]):
        self.__predicate = predicate

    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        return filter(self.__predicate, recipes)
