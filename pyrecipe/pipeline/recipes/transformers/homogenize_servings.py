# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.model.book import Recipe, Servings

from .by_recipe_transformer import ByRecipeTransformer


class HomogenizeServings(ByRecipeTransformer):
    def __init__(self, servings: Servings):
        self.__servings = servings

    def transform(self, recipe: Recipe) -> Recipe:
        return recipe.homogenises_servings(self.__servings)
