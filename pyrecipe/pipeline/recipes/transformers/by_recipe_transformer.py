# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import abstractmethod
from collections.abc import Iterable

from pyrecipe.model.book import Recipe

from .transformer import RecipeTransformer


class ByRecipeTransformer(RecipeTransformer):
    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        return map(self.transform, recipes)

    @abstractmethod
    def transform(self, recipe: Recipe) -> Recipe:
        """Transform one recipe"""
