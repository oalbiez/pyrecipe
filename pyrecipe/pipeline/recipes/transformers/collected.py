# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector

from .transformer import RecipeTransformer


class CollectedRecipeTransformer(RecipeTransformer):
    def __init__(self, collector: RecipeCollector):
        self.__collector = collector

    def process(self, recipes: Iterable[Recipe]) -> Iterable[Recipe]:
        stream = list(recipes)
        self.__collector.collect(stream)
        return stream
