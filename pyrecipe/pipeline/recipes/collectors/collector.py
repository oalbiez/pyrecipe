# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Iterable

from pyrecipe.model.book import Recipe


class RecipeCollector(ABC):
    @abstractmethod
    def collect(self, recipes: Iterable[Recipe]) -> None:
        """Collect recipes"""
