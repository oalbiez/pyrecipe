# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector
from pyrecipe.pipeline.recipes.providers.provider import RecipeProvider


def _merge(first: Iterable[Recipe], second: Iterable[Recipe]) -> Iterable[Recipe]:
    first_recipes = list(first)
    second_recipes = list(second)
    by_id = dict((r.id(), r) for r in second_recipes)
    for recipe in first_recipes:
        if recipe.id() in by_id:
            yield by_id[recipe.id()]
            del by_id[recipe.id()]
        else:
            yield recipe
    for recipe in second_recipes:
        if recipe.id() in by_id:
            yield recipe


class RecipeMerger(RecipeCollector):
    def __init__(self, provider: RecipeProvider, collector: RecipeCollector):
        self.provider = provider
        self.collector = collector
        self.original_order = True

    def update_order(self) -> RecipeMerger:
        self.original_order = False
        return self

    def keep_original_order(self) -> RecipeMerger:
        self.original_order = True
        return self

    def collect(self, recipes: Iterable[Recipe]) -> None:
        if self.original_order:
            args = (self.provider.provide(), recipes)
        else:
            args = (recipes, self.provider.provide())
        self.collector.collect(_merge(*args))
