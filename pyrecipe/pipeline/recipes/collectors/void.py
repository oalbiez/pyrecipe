# -*- coding: utf-8 -*-
from __future__ import annotations

from collections import deque
from collections.abc import Iterable

from pyrecipe.model.book import Recipe

from .collector import RecipeCollector


class VoidRecipeCollector(RecipeCollector):
    def collect(self, recipes: Iterable[Recipe]) -> None:
        deque(recipes, maxlen=0)  # consume recipes
