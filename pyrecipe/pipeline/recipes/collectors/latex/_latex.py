# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable, Iterable
from typing import TextIO

from pyrecipe.model.book import Recipe
from pyrecipe.model.template import BookTemplate, Template
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector


def _translate_unit(definitions: dict[str, str]):
    def process(text: str, render: Callable[[str], str]) -> str:
        result = render(text)
        if result in definitions:
            return definitions[result]
        return result

    return process


class LatexGenerator(RecipeCollector):
    def __init__(self, stream: TextIO, template: str | None = None):
        self.stream: TextIO = stream
        self.template = BookTemplate(
            Template.on_resource(
                "pyrecipe.pipeline.recipes.collectors.latex",
                f"{template or 'cuisine'}.mustache",
            ).with_symbol(
                "unit",
                _translate_unit(
                    {"°C": "C", "°F": "F", "tsp": "TL", "tbs": "EL", "pinch": "pn"}
                ),
            )
        )

    def collect(self, recipes: Iterable[Recipe]) -> None:
        self.stream.write(self.template.process_recipes(recipes))
        self.stream.write("\n\n")
