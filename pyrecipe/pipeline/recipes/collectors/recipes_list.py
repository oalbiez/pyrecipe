# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Name, Recipe
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector


class ListRecipes(RecipeCollector):
    def __init__(self):
        self.__recipes = []

    @property
    def recipes(self) -> Iterable[Name]:
        return self.__recipes

    def collect(self, recipes: Iterable[Recipe]) -> None:
        self.__recipes.extend(Name(r.name.capitalize()) for r in recipes)
