# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from pathlib import Path

from pyrecipe.model.book import Recipe
from pyrecipe.model.dsl import DSL
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector

_OVERWRITE: str = "wt"
_APPEND: str = "at"


class BookfileRecipeCollector(RecipeCollector):
    def __init__(self, bookfile: Path):
        self.__bookfile = bookfile
        self.__behavior = _OVERWRITE

    def overwrite(self) -> BookfileRecipeCollector:
        """Replace the content of the bookfile with collected receipes."""
        self.__behavior = _OVERWRITE
        return self

    def append(self) -> BookfileRecipeCollector:
        """Append the content of the bookfile with collected receipes."""
        self.__behavior = _APPEND
        return self

    def collect(self, recipes: Iterable[Recipe]) -> None:
        with open(self.__bookfile, self.__behavior, encoding="utf-8") as output:
            for recipe in recipes:
                output.write(DSL.render_recipe(recipe))
