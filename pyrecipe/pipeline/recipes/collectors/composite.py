# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe

from .collector import RecipeCollector


class CompositeRecipeCollector(RecipeCollector):
    def __init__(self, collectors: list[RecipeCollector]):
        self.__collectors = collectors

    @staticmethod
    def chain(*collectors: RecipeCollector) -> CompositeRecipeCollector:
        return CompositeRecipeCollector(list(collectors))

    def collect(self, recipes: Iterable[Recipe]) -> None:
        recipes = list(recipes)
        for collector in self.__collectors:
            collector.collect(recipes)
