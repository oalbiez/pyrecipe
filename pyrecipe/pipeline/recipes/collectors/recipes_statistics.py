# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from dataclasses import dataclass

from pyrecipe.model.book import Recipe
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector


@dataclass(frozen=True)
class Statistics:
    recipes: int
    characters: int


class CharacterCount:
    def __init__(self):
        self.count = 0

    def __call__(self, value: str) -> str:
        self.count += len(value)
        return value


class RecipesStatistics(RecipeCollector):
    def __init__(self):
        self.__statistics = Statistics(0, 0)

    @property
    def statistics(self) -> Statistics:
        return self.__statistics

    def collect(self, recipes: Iterable[Recipe]) -> None:
        char_counter = CharacterCount()
        count = 0
        for recipe in recipes:
            recipe.transform_text(char_counter)
            count += 1
        self.__statistics = Statistics(
            self.__statistics.recipes + count,
            self.__statistics.characters + char_counter.count,
        )
