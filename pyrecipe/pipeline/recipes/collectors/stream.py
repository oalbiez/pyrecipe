# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from typing import TextIO

from pyrecipe.model.book import Recipe
from pyrecipe.model.dsl import DSL
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector


class StreamRecipeCollector(RecipeCollector):
    def __init__(self, stream: TextIO):
        self.stream: TextIO = stream

    def collect(self, recipes: Iterable[Recipe]) -> None:
        for recipe in recipes:
            self.stream.write(DSL.render_recipe(recipe))
