# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Recipe
from pyrecipe.model.dsl import DSL
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector


class MemoryRecipeCollector(RecipeCollector):
    def __init__(self):
        self.__recipes: list[Recipe] = []

    @property
    def recipes(self) -> list[Recipe]:
        return self.__recipes

    def collect(self, recipes: Iterable[Recipe]) -> None:
        self.__recipes.extend(recipes)

    def render(self) -> str:
        return DSL.render_recipes(self.__recipes)
