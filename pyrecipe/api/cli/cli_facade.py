# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from io import StringIO
from textwrap import dedent

from pyrecipe.model.book import Lang, Recipe
from pyrecipe.pipeline.recipes.collectors.collector import RecipeCollector
from pyrecipe.pipeline.recipes.collectors.latex import LatexGenerator
from pyrecipe.pipeline.recipes.collectors.recipes_list import ListRecipes
from pyrecipe.pipeline.recipes.collectors.recipes_statistics import RecipesStatistics
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.transformers.filtered import FilteredRecipeTransformer
from pyrecipe.pipeline.recipes.transformers.translator import Translate
from pyrecipe.utils.predicates import Predicate


class CliFacade:
    def __init__(
        self,
        pipeline: Pipeline,
        translator: Translate.Driver,
    ):
        self.__pipeline = pipeline
        self.__translator = translator

    def filtered(self, predicate: Predicate[Recipe]) -> CliFacade:
        return CliFacade(
            self.__pipeline.append(FilteredRecipeTransformer(predicate)),
            self.__translator,
        )

    def collect_to(self, collector: RecipeCollector) -> CliFacade:
        return CliFacade(self.__pipeline.to_(collector), self.__translator)

    def process(self) -> None:
        self.__pipeline.run()

    def list_recipes(self) -> Iterable[str]:
        collector = ListRecipes()
        self.__pipeline.to_(collector).run()
        return collector.recipes

    def generate_latex(self) -> str:
        result = StringIO()
        self.__pipeline.to_(LatexGenerator(result)).run()
        return result.getvalue()

    def statistics(self) -> str:
        collector = RecipesStatistics()
        self.__pipeline.to_(collector).run()
        statistics = collector.statistics
        return dedent(
            f"""
            Statistics:
              recipes   : {statistics.recipes}
              characters: {statistics.characters}
        """
        )

    def translate(self, lang: str) -> None:
        self.__pipeline.append(Translate.on(self.__translator).lang(Lang(lang))).run()
