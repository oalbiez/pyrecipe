# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable, Iterable, Iterator
from dataclasses import dataclass
from enum import Enum

from pyrsistent import PVector, pvector


@dataclass(frozen=True, order=True)
class Tag:
    content: str

    @staticmethod
    def tag(value: str) -> Tag:
        return Tag(f"tag:{value}")

    @staticmethod
    def region(value: str) -> Tag:
        return Tag(f"region:{value}")

    @staticmethod
    def category(value: str) -> Tag:
        return Tag(f"category:{value}")

    @staticmethod
    def diet(value: str) -> Tag:
        return Tag(f"diet:{value}")

    def transform_text(self, functor: Callable[[str], str]) -> Tag:
        for prefix in ["region:", "category:", "diet:"]:
            if self.content.startswith(prefix):
                return self
        if self.content.startswith("tag:"):
            return Tag(f"tag:{functor(self.content[4:])}")
        return Tag(functor(self.content))


@dataclass(frozen=True, order=True)
class Tags:
    content: PVector[Tag] = pvector()

    @staticmethod
    def empty() -> Tags:
        return Tags()

    @staticmethod
    def on(tags: Iterable[Tag]) -> Tags:
        return Tags(pvector(tags))

    def __len__(self) -> int:
        return len(self.content)

    def __iter__(self) -> Iterator[Tag]:
        return iter(self.content)

    def __add__(self, other: Tags) -> Tags:
        return Tags(self.content + other.content)


class Region(Enum):
    ASIAN = Tag.region("asian")
    CHINESE = Tag.region("asian.chinese")
    FRENCH = Tag.region("europe.french")
    INDIAN = Tag.region("indian")
    ITALIAN = Tag.region("europe.italian")
    JAPANESE = Tag.region("asian.japanese")
    MAXICAN = Tag.region("latin.mexican")
    LATIN = Tag.region("latin")  # South america


class Diets(Enum):
    DAIRY_FREE = Tag.diet("dairy-free")
    GLUTEN_FREE = Tag.diet("gluten-free")
    KETO = Tag.diet("keto")
    PALEO = Tag.diet("paleo-friendly")
    VEGAN = Tag.diet("vegan")
    VEGETARIAN = Tag.diet("vegetarian")


class Categories(Enum):
    BAKED_GOOD = Tag.category("baked-good")
    BREAKFAST = Tag.category("breakfast")
    DESSERT = Tag.category("dessert")
    DINNER = Tag.category("dinner")
    DRINK = Tag.category("drink")
    LUNCH = Tag.category("lunch")
    PRE_DINNER = Tag.category("pre-dinner")
    SIDE_DISH = Tag.category("side-dish")
    SNACK = Tag.category("snack")
    SOUP = Tag.category("soup")
    STARTER = Tag.category("starter")
