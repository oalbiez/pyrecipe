from collections.abc import Iterable
from pathlib import Path
from typing import cast

from pyrecipe.model.book import Book, Recipe
from pyrecipe.model.pantry import Pantry
from pyrecipe.model.template import BookTemplate, PantryTemplate, Template

from ._parser import PARSER

BOOK_RENDERER = BookTemplate(
    Template.on_resource("pyrecipe.model.dsl", "book_dsl.mustache")
)
PANTRY_RENDERER = PantryTemplate(
    Template.on_resource("pyrecipe.model.dsl", "pantry_dsl.mustache")
)


class DSL:
    ## Book

    @staticmethod
    def parse_recipe(content: str) -> Recipe:
        print(repr(content))
        # parse rerturn a polymorphic type, we needs to cast to Recipe
        return cast(Recipe, PARSER.parse(content, start="recipe"))

    @staticmethod
    def parse_book(content: str) -> Book:
        # parse rerturn a polymorphic type, we needs to cast to Book
        return cast(Book, PARSER.parse(content, start="book"))

    @staticmethod
    def parse_bookfile(filename: Path) -> Book:
        with filename.open() as file:
            return DSL.parse_book(file.read())

    @staticmethod
    def render_book(book: Book) -> str:
        return BOOK_RENDERER.process_book(book)

    @staticmethod
    def render_recipes(recipes: Iterable[Recipe]) -> str:
        return BOOK_RENDERER.process_recipes(recipes)

    @staticmethod
    def render_recipe(recipe: Recipe) -> str:
        return BOOK_RENDERER.process_recipe(recipe)

    ## Pantry

    @staticmethod
    def parse_pantry(content: str) -> Pantry:
        # parse rerturn a polymorphic type, we needs to cast to Pantry
        return cast(Pantry, PARSER.parse(content, start="pantry"))

    @staticmethod
    def parse_pantryfile(filename: Path) -> Pantry:
        with filename.open() as file:
            return DSL.parse_pantry(file.read())

    @staticmethod
    def render_pantry(pantry: Pantry, **kwargs) -> str:
        return PANTRY_RENDERER.process_pantry(pantry, **kwargs)
