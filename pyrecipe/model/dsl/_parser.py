# -*- coding: utf-8 -*-

from fractions import Fraction
from typing import Any

from lark import Lark, Transformer

from pyrecipe.model.book import (
    Book,
    Description,
    Durations,
    Ingredient,
    Lang,
    Recipe,
    Servings,
    Source,
    Step,
)
from pyrecipe.model.pantry import Aliment, Component, Composition, Pantry
from pyrecipe.model.quantities.duration import Duration
from pyrecipe.model.quantities.energy import Energy
from pyrecipe.model.quantities.interval import Interval
from pyrecipe.model.quantities.length import Length
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.temperature import Temperature
from pyrecipe.model.quantities.volume import Volume
from pyrecipe.model.reference import Reference
from pyrecipe.model.tag import Tag
from pyrecipe.utils.parsing.transformer_actions import (
    compose,
    const,
    first,
    join,
    kwargs,
    quantity,
    single,
    strip,
    to_tuple,
    token_value,
)
from pyrecipe.utils.parsing.tree_indenter import TreeIndenter


class ParserTransformer(Transformer):
    # pylint: disable=invalid-name

    ## Book
    book = compose(list, Book)

    ## Recipe
    recipe = compose(dict, kwargs(Recipe))
    recipe_name = compose(first(), to_tuple("name"))
    recipe_identifier = compose(first(), to_tuple("identifier"))

    # -- Metadata
    metadata = compose(first())
    servings = compose(first(), Servings, to_tuple("servings"))
    source = compose(token_value(), Source, to_tuple("source"))
    lang = compose(token_value(), Lang, to_tuple("lang"))

    durations = compose(dict, kwargs(Durations), to_tuple("durations"))
    waiting = compose(first(), to_tuple("waiting"))
    preparing = compose(first(), to_tuple("preparing"))
    cooking = compose(first(), to_tuple("cooking"))
    baking = compose(first(), to_tuple("baking"))

    descriptions = compose(list, join("\n"), Description, to_tuple("description"))
    description_line = compose(single(str))

    # -- Steps
    steps = compose(list, to_tuple("steps"))
    step = compose(dict, kwargs(Step))

    ingredients = compose(list, to_tuple("ingredients"))
    ingredient = compose(dict, kwargs(Ingredient))
    ingredient_qty = compose(first(), to_tuple("qty"))
    ingredient_label = compose(first(), to_tuple("label"))
    ingredient_reference = compose(first(), to_tuple("aliment"))

    instructions = compose(list, to_tuple("parts"))
    instruction_part = compose(first())
    instruction_text = compose(single(str))
    instruction_fixed_qty = compose(first())

    ## Pantry
    pantry = compose(list, Pantry.on_list)

    ## Aliment
    aliment = compose(dict, kwargs(Aliment))
    aliment_label = compose(first(), to_tuple("label"))
    aliment_identifier = compose(first(), to_tuple("identifier"))
    composition = compose(dict, kwargs(Composition.for100g), to_tuple("composition"))
    component = compose(first())

    def energy_declaration(self, value):
        if value is None:
            return ("energy", Component.unknown())
        return ("energy", Component.on(value[0]))

    COMPONENT_NAME = compose(str)

    def mass_declaration(self, value):
        name, qty = value
        if qty is None:
            return (name, Component.unknown())
        return (name, Component.on(qty))

    ## Quantities
    def interval(self, value):
        lower, upper, fct = value
        return Interval.between(fct(lower), fct(upper))

    duration = compose(quantity())
    seconds = const(Duration.seconds)
    minutes = const(Duration.minutes)
    hours = const(Duration.hours)
    days = const(Duration.days)

    energy = compose(quantity())
    kj = const(Energy.kj)
    j = const(Energy.joules)
    cal = const(Energy.cal)
    kcal = const(Energy.kcal)

    length = compose(quantity())
    m = const(Length.m)
    dm = const(Length.dm)
    cm = const(Length.cm)
    mm = const(Length.mm)

    mass = compose(quantity())
    kg = const(Mass.kg)
    g = const(Mass.g)
    mg = const(Mass.mg)
    ug = const(Mass.ug)

    piece = compose(single(Scalar.scalar))

    some = const(None)

    temperature = compose(quantity())
    celsius = const(Temperature.celsius)
    fahrenheit = const(Temperature.fahrenheit)

    unknown = const(None)

    volume = compose(quantity())
    l = const(Volume.l)
    dl = const(Volume.dl)
    cl = const(Volume.cl)
    ml = const(Volume.ml)
    cup = const(Volume.cup)
    tbs = const(Volume.tbs)
    tsp = const(Volume.tsp)
    pinch = const(Volume.pinch)

    ## Tags
    tags = compose(list, to_tuple("tags"))
    tag = compose(single(str), Tag)

    ## Common
    integer = compose(first())
    number = compose(list, join("/"), Fraction)
    NUMBER = compose(str)
    INT = compose(int)
    identifier = compose(token_value(), strip(), Reference)
    label = compose(single(str), strip())


PARSER = Lark.open(
    "parser.lark",
    rel_to=__file__,
    parser="lalr",
    postlex=TreeIndenter(),
    transformer=ParserTransformer(),
    start=[
        "aliment",
        "book",
        "ingredient",
        "pantry",
        "quantities",
        "recipe",
    ],
    regex=True,
)


def parse_quantities(content: str) -> Any:
    return PARSER.parse(content, start="quantities")
