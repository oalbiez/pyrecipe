// == Book

book: _NL* recipe*


// == Recipe

recipe: "recipe" ":" recipe_name recipe_identifier _NL [_INDENT metadata* steps _DEDENT]

recipe_name: [label]
recipe_identifier: [identifier]

metadata: servings
        | source
        | lang
        | "tags" ":" _NL [_INDENT tags _DEDENT]
        | "durations" ":" _NL [_INDENT durations _DEDENT]
        | "description" ":" _NL [_INDENT descriptions _DEDENT]
servings: "servings" ":" integer _NL
source: "source" ":" /[^\n\r#]+/ _NL
lang: "lang" ":" /[a-z][a-z]/ _NL
durations: (waiting | preparing | cooking | baking)*
waiting: "waiting" ":" duration _NL
preparing: "preparing" ":" duration _NL
cooking: "cooking" ":" duration _NL
baking: "baking" ":" duration _NL

descriptions: description_line*
description_line: /[^\n\r#]+/ _NL

steps : step*
step: _dashline ingredients instructions
_dashline: "--" "-"* _NL

ingredients: (ingredient _NL)*
ingredient: "<" ingredient_qty ">" ingredient_label ingredient_reference
ingredient_qty: length
              | mass
              | volume
              | piece
              | some
ingredient_label: label
ingredient_reference: [identifier]

instructions: (instruction_part+ _NL)*
instruction_part: "[" instruction_fixed_qty "]"
                | instruction_text
instruction_text: /[^\n\r#\[<]+/
instruction_fixed_qty: duration
                     | length
                     | mass
                     | temperature
                     | volume
                     | interval
                     | piece


// == Pantry

pantry: _NL* aliment*


// == Aliment

aliment: "aliment" ":" aliment_label aliment_identifier _NL (_INDENT tag_section? composition_section? _DEDENT)?
aliment_label: [label]
aliment_identifier: [identifier]
?tag_section: "tags" ":" _NL [_INDENT tags _DEDENT]
?composition_section: "composition" ":" _NL [_INDENT composition _DEDENT]


// == Composition

composition: component*
component: energy_declaration | mass_declaration

energy_declaration: "energy" ":" (energy | "<" unknown ">") _NL
mass_declaration: COMPONENT_NAME ":" (mass | "<" unknown ">") _NL

COMPONENT_NAME: "fat"
              | "fa_saturated"
              | "fa_mono"
              | "fa_poly"
              | "carbohydrate"
              | "sugars"
              | "polyols"
              | "starch"
              | "fibres"
              | "protein"
              | "salt"
              | "alcohol"
              | "organic_acids"
              | "cholesterol"
              | "water"
              | "vitamin_A"
              | "vitamin_D"
              | "vitamin_E"
              | "vitamin_K1"
              | "vitamin_K2"
              | "vitamin_C"
              | "vitamin_B1"
              | "vitamin_B2"
              | "vitamin_B3"
              | "vitamin_B5"
              | "vitamin_B6"
              | "vitamin_B12"
              | "vitamin_B9"
              | "sodium"
              | "magnesium"
              | "phosphorus"
              | "chloride"
              | "potassium"
              | "calcium"
              | "manganese"
              | "iron"
              | "copper"
              | "zinc"
              | "selenium"
              | "iodine"


// == Quantities

?quantities: interval
           | duration
           | energy
           | length
           | mass
           | temperature
           | volume
           | piece
           | some
           | unknown

interval: number "--" number (kg | g | seconds | minutes | hours | celsius | fahrenheit)

duration: number (seconds | minutes | hours | days)
seconds: "s"
minutes: "min"
hours: "h"
days: "d"

energy: number (j | kj | cal | kcal)
j: "j" | "J"
kj: "kj" | "kJ"
cal: "cal"
kcal: "kcal"

length: number (m | dm | cm | mm)
m: "m"
dm: "dm"
cm: "cm"
mm: "mm"

mass: number (kg | g | mg | ug)
kg: "kg"
g: "g"
mg: "mg"
ug: "ug" | "µg"

piece: number

some: "some"

temperature: number (celsius | fahrenheit)
celsius: "°C"
fahrenheit: "°F"

unknown: "unknown"

volume: number (l | dl | cl | ml | cup | tbs | tsp | pinch)
l: "l"
dl: "dl"
cl: "cl"
ml: "ml"
cup: "cup"
tbs: "tbs"
tsp: "tsp"
pinch: "pinch"


// == Common

tags: tag*
tag: /[^\n\r#]+/ _NL
label: /[^\n\r@#]+/
identifier: "@" REFERENCE
integer: INT
number: NUMBER
      | NUMBER "/" NUMBER

REFERENCE: /[\p{L}\p{N}_\-\/\|\.]+/

%import common.INT -> INT
%import common.NUMBER -> NUMBER
%declare _INDENT _DEDENT

%import common.WS_INLINE
%ignore WS_INLINE
%import common.SH_COMMENT -> COMMENT
%ignore COMMENT

_NL: ( /\r?\n[\t ]*/ | COMMENT )+
