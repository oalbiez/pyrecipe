# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from pyrecipe.model.quantities.duration import Duration
from pyrecipe.model.quantities.interval import Interval
from pyrecipe.model.quantities.length import Length
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.temperature import Temperature
from pyrecipe.model.quantities.volume import Volume

from ._ingredient import Ingredient

Item = str | Duration | Interval | Length | Mass | Scalar | Temperature | Volume


@dataclass(frozen=True)
class Step:
    ingredients: list[Ingredient]
    items: list[Item]
