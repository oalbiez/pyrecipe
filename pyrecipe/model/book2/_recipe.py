# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field

from pyrecipe.model.reference import Reference

from ._metadata import Metadata
from ._section import Section
from ._types import Name


@dataclass(frozen=True)
class Recipe:
    identifier: Reference | None = None
    name: Name = Name("")
    metadata: Metadata = Metadata.empty()
    sections: list[Section] = field(default_factory=list)
