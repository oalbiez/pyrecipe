# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field

from pyrsistent import PMap, pmap

from pyrecipe.model.tag import Tags

from ._durations import Durations
from ._types import Description, Lang, Servings, Source


@dataclass(frozen=True)
class Metadata:
    source: Source | None = None
    lang: Lang = Lang("fr")
    servings: Servings | None = None
    durations: Durations = Durations.empty()
    tags: Tags = Tags.empty()
    description: Description = field(default_factory=lambda: Description(""))
    others: PMap[str, str] = pmap({})

    @staticmethod
    def empty() -> Metadata:
        return Metadata()
