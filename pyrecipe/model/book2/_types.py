# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import NewType


@dataclass(frozen=True)
class Source:
    value: str


Name = NewType("Name", str)
Servings = NewType("Servings", int)
Description = NewType("Description", str)
Lang = NewType("Lang", str)
