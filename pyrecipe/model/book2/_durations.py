# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from pyrecipe.model.quantities.duration import Duration


@dataclass(frozen=True)
class Durations:
    waiting: Duration | None = None
    preparing: Duration | None = None
    cooking: Duration | None = None
    baking: Duration | None = None

    @staticmethod
    def empty():
        return Durations()
