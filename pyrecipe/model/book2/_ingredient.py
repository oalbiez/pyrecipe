# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from pyrecipe.model.quantities.length import Length
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.volume import Volume
from pyrecipe.model.reference import Reference

Qty = Length | Mass | Scalar | Volume | None


@dataclass(frozen=True)
class Ingredient:
    qty: Qty
    label: str
    aliment: Reference
