# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from ._step import Step
from ._types import Name


@dataclass(frozen=True)
class Section:
    name: Name
    steps: list[Step]
