# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .converters import Converters
from .dimension import Dimension, find_unit
from .number import Number
from .quantity import Quantity
from .scalar import Scalar
from .unit_factory import UnitFactory, centi, deci, milli


@dataclass(frozen=True, order=True)
class Volume(Dimension):
    class Unit(Enum):
        # pylint: disable=invalid-name
        l = UnitFactory.unit("litre", "l").store_as(milli)
        ml = UnitFactory.composite(milli, l)
        cl = UnitFactory.composite(centi, l)
        dl = UnitFactory.composite(deci, l)
        cup = UnitFactory.unit("cup", "cup").derived(ml, Converters.linear(250))
        handful = UnitFactory.unit("handful", "handful").derived(
            ml, Converters.linear(125)
        )
        tbs = UnitFactory.unit("table spoon", "tbs").derived(ml, Converters.linear(15))
        tsp = UnitFactory.unit("tea spoon", "tsp").derived(ml, Converters.linear(5))
        pinch = UnitFactory.unit("pinch", "pinch").derived(ml, Converters.linear("1/2"))

    qty: Quantity

    @staticmethod
    def on(value: Number, unit: Unit) -> Volume:
        # pylint: disable=invalid-name
        return Volume(Quantity.on(value, unit.value))

    @staticmethod
    def ml(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.ml)

    @staticmethod
    def cl(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.cl)

    @staticmethod
    def dl(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.dl)

    @staticmethod
    def l(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.l)

    @staticmethod
    def cup(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.cup)

    @staticmethod
    def handful(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.handful)

    @staticmethod
    def tbs(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.tbs)

    @staticmethod
    def tsp(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.tsp)

    @staticmethod
    def pinch(value: Number) -> Volume:
        # pylint: disable=invalid-name
        return Volume.on(value, Volume.Unit.pinch)

    @staticmethod
    def ratio(left: Volume, right: Volume) -> Scalar:
        return Quantity.ratio(left.qty, right.qty)

    @property
    def magnitude(self) -> Fraction:
        return self.qty.magnitude

    @property
    def unit(self) -> Volume.Unit:
        return find_unit(Volume.Unit, self.qty.unit.symbol)

    @property
    def symbol(self) -> str:
        return self.qty.symbol

    def to(self, unit: Unit) -> Volume:
        # pylint: disable=invalid-name
        return Volume(self.qty.to(unit.value))

    def __str__(self) -> str:
        return str(self.qty)

    def __repr__(self) -> str:
        return (
            f"Volume.on({repr(fractional_view(self.magnitude, 3))}, Volume.{self.unit})"
        )

    def __add__(self, right: Volume) -> Volume:
        return Volume(self.qty + right.qty)

    def __sub__(self, right: Volume) -> Volume:
        return Volume(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Volume:
        return Volume(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Volume:
        return Volume(self.qty / right)

    def __round__(self, ndigits: int = 0) -> Volume:
        return Volume(self.qty.__round__(ndigits))
