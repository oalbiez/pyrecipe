# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Generic

from .interval import QTY, Interval, LowerBound, UpperBound


@dataclass(frozen=True)
class Partition(Generic[QTY]):
    interval: Interval[QTY]
    rank: int

    @staticmethod
    def on(lower: LowerBound[QTY], upper: UpperBound[QTY], rank: int) -> Partition[QTY]:
        # pylint: disable=invalid-name
        return Partition(Interval(lower, upper), rank)

    def __contains__(self, value: QTY) -> bool:
        return self.interval.__contains__(value)

    def __repr__(self) -> str:
        return f"Partition({repr(self.interval)}, {self.rank})"


@dataclass(frozen=True)
class Scale(Generic[QTY]):
    partitions: list[Partition[QTY]]

    @staticmethod
    def scale(*partitions: Partition[QTY]) -> Scale[QTY]:
        return Scale(list(partitions))

    def rank(self, quantity: QTY) -> int:
        for partition in self.partitions:
            if quantity in partition:
                return partition.rank
        raise ValueError(f"{quantity} cannot be ranked")

    def interval_for(self, rank: int) -> Interval[QTY]:
        for partition in self.partitions:
            if partition.rank == rank:
                return partition.interval
        raise ValueError(f"{rank} not found in this scale")
