# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .number import Number, number_to_fraction
from .scalar import Scalar
from .unit_factory import UnitDefinition


@dataclass(frozen=True)
class Quantity:
    _value: Fraction
    _unit: UnitDefinition

    @staticmethod
    def on(value: Number, unit: UnitDefinition) -> Quantity:
        # pylint: disable=invalid-name
        return Quantity(unit.converter.to_reference(number_to_fraction(value)), unit)

    @staticmethod
    def ratio(left: Quantity, right: Quantity) -> Scalar:
        return Scalar.scalar(left.absolute_magnitude / right.absolute_magnitude)

    @property
    def magnitude(self) -> Fraction:
        return self._unit.converter.from_reference(self._value)

    @property
    def absolute_magnitude(self) -> Fraction:
        return self._value

    @property
    def unit(self) -> UnitDefinition:
        return self._unit

    @property
    def symbol(self) -> str:
        return self._unit.symbol

    def to(self, unit: UnitDefinition) -> Quantity:
        # pylint: disable=invalid-name
        return Quantity(self.absolute_magnitude, unit)

    def __str__(self) -> str:
        return f"{fractional_view(self.magnitude)} {self._unit.symbol}"

    def __add__(self, right: Quantity) -> Quantity:
        return Quantity(self.absolute_magnitude + right.absolute_magnitude, self._unit)

    def __sub__(self, right: Quantity) -> Quantity:
        return Quantity(self.absolute_magnitude - right.absolute_magnitude, self._unit)

    def __mul__(self, right: Number | Scalar) -> Quantity:
        if isinstance(right, Scalar):
            return Quantity(self.absolute_magnitude * right.value, self._unit)
        return Quantity(self.absolute_magnitude * number_to_fraction(right), self._unit)

    def __truediv__(self, right: Number | Scalar) -> Quantity:
        if isinstance(right, Scalar):
            return Quantity(self.absolute_magnitude / right.value, self._unit)
        return Quantity(self.absolute_magnitude / number_to_fraction(right), self._unit)

    def __eq__(self, right: object) -> bool:
        if isinstance(right, Quantity):
            return self.absolute_magnitude == right.absolute_magnitude
        return NotImplemented

    def __ne__(self, right: object) -> bool:
        if isinstance(right, Quantity):
            return self.absolute_magnitude != right.absolute_magnitude
        return NotImplemented

    def __lt__(self, right: Quantity) -> bool:
        return self.absolute_magnitude < right.absolute_magnitude

    def __le__(self, right: Quantity) -> bool:
        return self.absolute_magnitude <= right.absolute_magnitude

    def __gt__(self, right: Quantity) -> bool:
        return self.absolute_magnitude > right.absolute_magnitude

    def __ge__(self, right: Quantity) -> bool:
        return self.absolute_magnitude >= right.absolute_magnitude

    def __round__(self, ndigits: int = 0) -> Quantity:
        return Quantity.on(self.magnitude.__round__(ndigits), self._unit)
