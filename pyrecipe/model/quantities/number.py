# -*- coding: utf-8 -*-
from __future__ import annotations

import re
from fractions import Fraction

from typing_extensions import TypeAlias

Number: TypeAlias = str | int | float | Fraction


_pattern = re.compile("(\\d+)\\s+(\\d+/\\d+)")


def number_to_fraction(value: Number) -> Fraction:
    try:
        return Fraction(value)
    except ValueError as error:
        if isinstance(value, str):
            match = re.match(_pattern, value)
            if match:
                return Fraction(match.group(1)) + Fraction(match.group(2))
        raise error
