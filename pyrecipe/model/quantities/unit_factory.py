# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass

from .converters import Converter, Converters


class UnitDefinition(ABC):
    @property
    @abstractmethod
    def name(self) -> str:
        """Name of the unit."""

    @property
    @abstractmethod
    def symbol(self) -> str:
        """Symbol of the unit."""

    @property
    @abstractmethod
    def converter(self) -> Converter:
        """Converter used to convert values."""


@dataclass
class UnitPrefix(UnitDefinition):
    _name: str
    _symbol: str
    _aliases: list[str]
    _converter: Converter

    @property
    def name(self) -> str:
        return self._name

    @property
    def symbol(self) -> str:
        return self._symbol

    def alias(self, value: str) -> UnitPrefix:
        return UnitPrefix(
            self._name, self._symbol, self._aliases + [value], self._converter
        )

    @property
    def converter(self) -> Converter:
        return self._converter


@dataclass
class SimpleUnit(UnitDefinition):
    _name: str
    _symbol: str
    _store_as: Converter

    @property
    def name(self) -> str:
        return self._name

    @property
    def symbol(self) -> str:
        return self._symbol

    def derived(self, unit: UnitDefinition, converter: Converter) -> DerivedUnit:
        return DerivedUnit(self._name, self._symbol, unit, converter)

    def store_as(self, prefix: UnitPrefix) -> SimpleUnit:
        return SimpleUnit(
            self._name, self._symbol, Converters.inverse(prefix.converter)
        )

    @property
    def converter(self) -> Converter:
        return self._store_as


@dataclass
class DerivedUnit(UnitDefinition):
    _name: str
    _symbol: str
    _unit: UnitDefinition
    _converter: Converter

    @property
    def name(self) -> str:
        return self._name

    @property
    def symbol(self) -> str:
        return self._symbol

    @property
    def converter(self) -> Converter:
        return Converters.compose(self._converter, self._unit.converter)


@dataclass
class CompositeUnit(UnitDefinition):
    _prefix: UnitPrefix
    _unit: UnitDefinition

    @property
    def name(self) -> str:
        return self._prefix.name + self._unit.name

    @property
    def symbol(self) -> str:
        return self._prefix.symbol + self._unit.symbol

    @property
    def converter(self) -> Converter:
        return Converters.compose(self._prefix.converter, self._unit.converter)


class UnitFactory:
    @staticmethod
    def prefix(name, symbol, converter) -> UnitPrefix:
        return UnitPrefix(name, symbol, [], converter)

    @staticmethod
    def unit(name, symbol) -> SimpleUnit:
        return SimpleUnit(name, symbol, Converters.identity())

    @staticmethod
    def composite(prefix, unit) -> CompositeUnit:
        return CompositeUnit(prefix, unit)


micro = UnitFactory.prefix("micro", "µ", Converters.linear("0.000001")).alias("u")
milli = UnitFactory.prefix("milli", "m", Converters.linear("0.001"))
centi = UnitFactory.prefix("centi", "c", Converters.linear("0.01"))
deci = UnitFactory.prefix("deci", "d", Converters.linear("0.1"))
kilo = UnitFactory.prefix("kilo", "k", Converters.linear("1000"))


# class AutoScaler:

# Exemple de critères:
# ml
# cl
# dl: pas utilisé
# l
# tbs : only 1, 2, 3, 4
# tsp : only 1/4, 1/2, 1, 2, 4
# 1ml = 1/4 tsp
# 2,5ml = 1/2 tsp
# 5ml = 1 tsp
# arrondis: 0,25ml

# def autoscale(self, value: Fraction):
#     pass

# @staticmethod
# def best_scale(value: Fraction) -> MassUnit:
#     magnitudes = [
#         (item, item.value.from_reference(value)) for item in Mass.Unit
#     ]
#     best_denominator = min(m[1].denominator for m in magnitudes)
#     magnitudes = [m for m in magnitudes if m[1].denominator == best_denominator]
#     if len(magnitudes) == 1:
#         return magnitudes[0][0]
#     best_numerator = min(abs(m[1].numerator) for m in magnitudes)
#     for u, v in magnitudes:
#         if abs(v.numerator) == best_numerator:
#             return u
#     return Mass.Unit.g
