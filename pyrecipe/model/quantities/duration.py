# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .converters import Converters
from .dimension import Dimension, find_unit
from .number import Number
from .quantity import Quantity
from .scalar import Scalar
from .unit_factory import UnitFactory, milli


@dataclass(frozen=True, order=True)
class Duration(Dimension):
    class Unit(Enum):
        # pylint: disable=invalid-name
        s = UnitFactory.unit("seconds", "s").store_as(milli)
        min = UnitFactory.unit("minutes", "min").derived(s, Converters.linear(60))
        H = UnitFactory.unit("hours", "h").derived(min, Converters.linear(60))
        days = UnitFactory.unit("days", "d").derived(H, Converters.linear(24))

    qty: Quantity

    @staticmethod
    def on(value: Number, unit: Unit) -> Duration:
        # pylint: disable=invalid-name
        return Duration(Quantity.on(value, unit.value))

    @staticmethod
    def hhmm(hours: int, minutes: int) -> Duration:
        return Duration.hours(hours) + Duration.minutes(minutes)

    @staticmethod
    def seconds(value: Number) -> Duration:
        return Duration.on(value, Duration.Unit.s)

    @staticmethod
    def minutes(value: Number) -> Duration:
        return Duration.on(value, Duration.Unit.min)

    @staticmethod
    def hours(value: Number) -> Duration:
        return Duration.on(value, Duration.Unit.H)

    @staticmethod
    def days(value: Number) -> Duration:
        return Duration.on(value, Duration.Unit.days)

    @staticmethod
    def ratio(left: Duration, right: Duration) -> Scalar:
        return Quantity.ratio(left.qty, right.qty)

    @property
    def magnitude(self) -> Fraction:
        return self.qty.magnitude

    @property
    def unit(self) -> Duration.Unit:
        return find_unit(Duration.Unit, self.qty.unit.symbol)

    @property
    def symbol(self) -> str:
        return self.qty.symbol

    def to(self, unit: Unit) -> Duration:
        # pylint: disable=invalid-name
        return Duration(self.qty.to(unit.value))

    def __str__(self) -> str:
        return str(self.qty)

    def __repr__(self) -> str:
        return (
            f"Duration.on({repr(fractional_view(self.magnitude, 3))},"
            f" Duration.{self.unit})"
        )

    def __add__(self, right: Duration) -> Duration:
        return Duration(self.qty + right.qty)

    def __sub__(self, right: Duration) -> Duration:
        return Duration(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Duration:
        return Duration(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Duration:
        return Duration(self.qty / right)

    def __round__(self, ndigits: int = 0) -> Duration:
        return Duration(self.qty.__round__(ndigits))
