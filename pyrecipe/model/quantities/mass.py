# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .dimension import Dimension, find_unit
from .number import Number
from .quantity import Quantity
from .scalar import Scalar
from .unit_factory import UnitFactory, kilo, micro, milli


@dataclass(frozen=True, order=True)
class Mass(Dimension):
    class Unit(Enum):
        # pylint: disable=invalid-name
        g = UnitFactory.unit("gram", "g").store_as(micro)
        ug = UnitFactory.composite(micro, g)
        mg = UnitFactory.composite(milli, g)
        kg = UnitFactory.composite(kilo, g)

    qty: Quantity

    @staticmethod
    def on(value: Number, unit: Unit) -> Mass:
        # pylint: disable=invalid-name
        return Mass(Quantity.on(value, unit.value))

    @staticmethod
    def ug(value: Number) -> Mass:
        # pylint: disable=invalid-name
        return Mass.on(value, Mass.Unit.ug)

    @staticmethod
    def mg(value: Number) -> Mass:
        # pylint: disable=invalid-name
        return Mass.on(value, Mass.Unit.mg)

    @staticmethod
    def g(value: Number) -> Mass:
        # pylint: disable=invalid-name
        return Mass.on(value, Mass.Unit.g)

    @staticmethod
    def kg(value: Number) -> Mass:
        # pylint: disable=invalid-name
        return Mass.on(value, Mass.Unit.kg)

    @staticmethod
    def ratio(left: Mass, right: Mass) -> Scalar:
        return Quantity.ratio(left.qty, right.qty)

    @property
    def magnitude(self) -> Fraction:
        return self.qty.magnitude

    @property
    def unit(self) -> Mass.Unit:
        return find_unit(Mass.Unit, self.qty.unit.symbol)

    @property
    def symbol(self) -> str:
        return self.qty.symbol

    def to(self, unit: Unit) -> Mass:
        # pylint: disable=invalid-name
        return Mass(self.qty.to(unit.value))

    def __str__(self) -> str:
        return str(self.qty)

    def __repr__(self) -> str:
        return f"Mass.on({repr(fractional_view(self.magnitude, 3))}, Mass.{self.unit})"

    def __add__(self, right: Mass) -> Mass:
        return Mass(self.qty + right.qty)

    def __sub__(self, right: Mass) -> Mass:
        return Mass(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Mass:
        return Mass(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Mass:
        return Mass(self.qty / right)

    def __round__(self, ndigits: int = 0) -> Mass:
        return Mass(self.qty.__round__(ndigits))
