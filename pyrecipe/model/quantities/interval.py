# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Generic, TypeVar

from .duration import Duration
from .energy import Energy
from .mass import Mass
from .scalar import Scalar
from .temperature import Temperature
from .volume import Volume

QTY = TypeVar("QTY", Duration, Energy, Mass, Scalar, Temperature, Volume)


@dataclass(frozen=True)
class LowerBound(Generic[QTY]):
    _bound: QTY | None
    _closed: bool

    @staticmethod
    def closed(value: QTY) -> LowerBound[QTY]:
        return LowerBound(value, True)

    @staticmethod
    def opened(value: QTY) -> LowerBound[QTY]:
        return LowerBound(value, False)

    @staticmethod
    def inf() -> LowerBound[Any]:
        return LowerBound(None, False)

    @property
    def is_closed(self) -> bool:
        return self._closed

    @property
    def value(self) -> QTY:
        if self._bound is not None:
            return self._bound
        raise ValueError("cannot get value of infinite")

    def is_lower(self, value: QTY) -> bool:
        if self._bound is None:
            return True
        if self._closed:
            return self._bound <= value
        return self._bound < value

    def __repr__(self) -> str:
        if self._bound is None:
            return "LowerBound.inf()"
        if self._closed:
            return f"LowerBound.closed({repr(self._bound)})"
        return f"LowerBound.opened({repr(self._bound)})"


@dataclass
class UpperBound(Generic[QTY]):
    _bound: QTY | None
    _closed: bool

    @staticmethod
    def closed(value: QTY) -> UpperBound[QTY]:
        return UpperBound(value, True)

    @staticmethod
    def opened(value: QTY) -> UpperBound[QTY]:
        return UpperBound(value, False)

    @staticmethod
    def inf() -> UpperBound[Any]:
        return UpperBound(None, False)

    @property
    def is_closed(self) -> bool:
        return self._closed

    @property
    def value(self) -> QTY:
        if self._bound is not None:
            return self._bound
        raise ValueError("cannot get value of infinite")

    def is_upper(self, value: QTY) -> bool:
        if self._bound is None:
            return True
        if self._closed:
            return self._bound >= value
        return self._bound > value

    def __repr__(self) -> str:
        if self._bound is None:
            return "UpperBound.inf()"
        if self._closed:
            return f"UpperBound.closed({repr(self._bound)})"
        return f"UpperBound.opened({repr(self._bound)})"


@dataclass
class Interval(Generic[QTY]):
    lower: LowerBound[QTY]
    upper: UpperBound[QTY]

    @staticmethod
    def between(lower: QTY, upper: QTY) -> Interval[QTY]:
        return Interval(LowerBound.closed(lower), UpperBound.closed(upper))

    def __repr__(self) -> str:
        return f"Interval({repr(self.lower)}, {repr(self.upper)})"

    def __contains__(self, value: QTY) -> bool:
        return self.lower.is_lower(value) and self.upper.is_upper(value)
