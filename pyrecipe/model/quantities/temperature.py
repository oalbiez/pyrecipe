# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .converters import Converters
from .dimension import Dimension, find_unit
from .number import Number
from .quantity import Quantity
from .scalar import Scalar
from .unit_factory import UnitFactory


@dataclass(frozen=True, order=True)
class Temperature(Dimension):
    class Unit(Enum):
        # pylint: disable=invalid-name
        C = UnitFactory.unit("celsius", "°C")
        F = UnitFactory.unit("fahrenheit", "°F").derived(
            C, Converters.affine("5/9", Fraction("5/9") * -32)
        )

    qty: Quantity

    @staticmethod
    def on(value: Number, unit: Unit) -> Temperature:
        # pylint: disable=invalid-name
        return Temperature(Quantity.on(value, unit.value))

    @staticmethod
    def celsius(value: Number) -> Temperature:
        return Temperature.on(Fraction(value), Temperature.Unit.C)

    @staticmethod
    def fahrenheit(value: Number) -> Temperature:
        return Temperature.on(Fraction(value), Temperature.Unit.F)

    @staticmethod
    def ratio(left: Temperature, right: Temperature) -> Scalar:
        return Quantity.ratio(left.qty, right.qty)

    @property
    def magnitude(self) -> Fraction:
        return self.qty.magnitude

    @property
    def unit(self) -> Temperature.Unit:
        return find_unit(Temperature.Unit, self.qty.unit.symbol)

    @property
    def symbol(self) -> str:
        return self.qty.symbol

    def to(self, unit: Unit) -> Temperature:
        # pylint: disable=invalid-name
        return Temperature(self.qty.to(unit.value))

    def __str__(self) -> str:
        return str(self.qty)

    def __repr__(self) -> str:
        return (
            "Temperature.on("
            f"{repr(fractional_view(self.magnitude, 3))}, Temperature.{self.unit})"
        )

    def __add__(self, right: Temperature) -> Temperature:
        return Temperature(self.qty + right.qty)

    def __sub__(self, right: Temperature) -> Temperature:
        return Temperature(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Temperature:
        return Temperature(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Temperature:
        return Temperature(self.qty / right)

    def __round__(self, ndigits: int = 0) -> Temperature:
        return Temperature(self.qty.__round__(ndigits))
