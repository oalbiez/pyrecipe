# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from fractions import Fraction

from pyrecipe.model.quantities.dimension import Dimension
from pyrecipe.utils.fractional import fractional_view

from .number import Number, number_to_fraction


@dataclass(order=True)
class Scalar(Dimension):
    value: Fraction

    @staticmethod
    def scalar(value: Number) -> Scalar:
        return Scalar(number_to_fraction(value))

    @staticmethod
    def ratio(left: Scalar, right: Scalar) -> Scalar:
        return Scalar.scalar(left.value / right.value)

    @property
    def magnitude(self) -> Fraction:
        return self.value

    @property
    def symbol(self) -> str:
        return "#"

    def __str__(self) -> str:
        return f"{fractional_view(self.value)}"

    def __repr__(self) -> str:
        return f"Scalar.scalar({repr(fractional_view(self.value, 3))})"

    def __add__(self, right: Scalar) -> Scalar:
        return Scalar(self.value + right.value)

    def __sub__(self, right: Scalar) -> Scalar:
        return Scalar(self.value - right.value)

    def __mul__(self, right: "Scalar | Number") -> Scalar:
        if isinstance(right, Scalar):
            return Scalar(self.value * right.value)
        return Scalar(self.value * number_to_fraction(right))

    def __truediv__(self, right: "Scalar | Number") -> Scalar:
        if isinstance(right, Scalar):
            return Scalar(self.value / right.value)
        return Scalar(self.value / number_to_fraction(right))

    def __round__(self, ndigits: int = 0) -> Scalar:
        return Scalar(self.value.__round__(ndigits))
