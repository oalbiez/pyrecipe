# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .converters import Converters
from .dimension import Dimension, find_unit
from .number import Number
from .quantity import Quantity
from .scalar import Scalar
from .unit_factory import UnitFactory, kilo


@dataclass(frozen=True, order=True)
class Energy(Dimension):
    class Unit(Enum):
        # pylint: disable=invalid-name
        J = UnitFactory.unit("joules", "J")
        kJ = UnitFactory.composite(kilo, J)
        cal = UnitFactory.unit("calories", "cal").derived(J, Converters.linear("4.184"))
        kcal = UnitFactory.composite(kilo, cal)

    qty: Quantity

    @staticmethod
    def on(value: Number, unit: Unit) -> Energy:
        # pylint: disable=invalid-name
        return Energy(Quantity.on(value, unit.value))

    @staticmethod
    def joules(value: Number) -> Energy:
        return Energy.on(value, Energy.Unit.J)

    @staticmethod
    def kj(value: Number) -> Energy:
        # pylint: disable=invalid-name
        return Energy.on(value, Energy.Unit.kJ)

    @staticmethod
    def cal(value: Number) -> Energy:
        return Energy.on(value, Energy.Unit.cal)

    @staticmethod
    def kcal(value: Number) -> Energy:
        return Energy.on(value, Energy.Unit.kcal)

    @staticmethod
    def ratio(left: Energy, right: Energy) -> Scalar:
        return Quantity.ratio(left.qty, right.qty)

    @property
    def magnitude(self) -> Fraction:
        return self.qty.magnitude

    @property
    def unit(self) -> Energy.Unit:
        return find_unit(Energy.Unit, self.qty.unit.symbol)

    @property
    def symbol(self) -> str:
        return self.qty.symbol

    def to(self, unit: Unit) -> Energy:
        # pylint: disable=invalid-name
        return Energy(self.qty.to(unit.value))

    def __str__(self) -> str:
        return str(self.qty)

    def __repr__(self) -> str:
        return (
            f"Energy.on({repr(fractional_view(self.magnitude, 3))}, Energy.{self.unit})"
        )

    def __add__(self, right: Energy) -> Energy:
        return Energy(self.qty + right.qty)

    def __sub__(self, right: Energy) -> Energy:
        return Energy(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Energy:
        return Energy(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Energy:
        return Energy(self.qty / right)

    def __round__(self, ndigits: int = 0) -> Energy:
        return Energy(self.qty.__round__(ndigits))
