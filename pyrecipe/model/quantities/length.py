# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from fractions import Fraction

from pyrecipe.utils.fractional import fractional_view

from .dimension import Dimension, find_unit
from .number import Number
from .quantity import Quantity
from .scalar import Scalar
from .unit_factory import UnitFactory, centi, deci, milli


@dataclass(frozen=True, order=True)
class Length(Dimension):
    class Unit(Enum):
        # pylint: disable=invalid-name
        m = UnitFactory.unit("meter", "m").store_as(milli)
        dm = UnitFactory.composite(deci, m)
        cm = UnitFactory.composite(centi, m)
        mm = UnitFactory.composite(milli, m)

    qty: Quantity

    @staticmethod
    def on(value: Number, unit: Unit) -> Length:
        # pylint: disable=invalid-name
        return Length(Quantity.on(value, unit.value))

    @staticmethod
    def mm(value: Number) -> Length:
        # pylint: disable=invalid-name
        return Length.on(value, Length.Unit.mm)

    @staticmethod
    def cm(value: Number) -> Length:
        # pylint: disable=invalid-name
        return Length.on(value, Length.Unit.cm)

    @staticmethod
    def dm(value: Number) -> Length:
        # pylint: disable=invalid-name
        return Length.on(value, Length.Unit.dm)

    @staticmethod
    def m(value: Number) -> Length:
        # pylint: disable=invalid-name
        return Length.on(value, Length.Unit.m)

    @staticmethod
    def ratio(left: Length, right: Length) -> Scalar:
        return Quantity.ratio(left.qty, right.qty)

    @property
    def magnitude(self) -> Fraction:
        return self.qty.magnitude

    @property
    def unit(self) -> Length.Unit:
        return find_unit(Length.Unit, self.qty.unit.symbol)

    @property
    def symbol(self) -> str:
        return self.qty.symbol

    def to(self, unit: Unit) -> Length:
        # pylint: disable=invalid-name
        return Length(self.qty.to(unit.value))

    def __str__(self) -> str:
        return str(self.qty)

    def __repr__(self) -> str:
        return (
            f"Length.on({repr(fractional_view(self.magnitude, 3))}, Length.{self.unit})"
        )

    def __add__(self, right: Length) -> Length:
        return Length(self.qty + right.qty)

    def __sub__(self, right: Length) -> Length:
        return Length(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Length:
        return Length(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Length:
        return Length(self.qty / right)

    def __round__(self, ndigits: int = 0) -> Length:
        return Length(self.qty.__round__(ndigits))
