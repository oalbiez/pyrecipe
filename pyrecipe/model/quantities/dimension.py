# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Iterable
from enum import Enum
from fractions import Fraction
from typing import TypeVar


class Dimension(ABC):
    @property
    @abstractmethod
    def magnitude(self) -> Fraction:
        """Return the magnitude of the quantity"""

    @property
    @abstractmethod
    def symbol(self) -> str:
        """Return the unit's symbol of the quantity"""


UnitEnum = TypeVar("UnitEnum", bound=Enum)


def find_unit(definitions: Iterable[UnitEnum], symbol: str) -> UnitEnum:
    for definition in definitions:
        if definition.value.symbol == symbol:
            return definition
    raise ValueError(f"{symbol} is not a valid unit for {repr(definitions)}")
