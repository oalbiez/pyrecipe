# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from fractions import Fraction

from .number import Number, number_to_fraction


class Converter(ABC):
    @abstractmethod
    def to_reference(self, value: Fraction) -> Fraction:
        """Transforms external value to internal value"""

    @abstractmethod
    def from_reference(self, value: Fraction) -> Fraction:
        """Transforms internal value to external value"""


@dataclass
class _Identity(Converter):
    """Represents identity transformation"""

    def to_reference(self, value: Fraction) -> Fraction:
        return value

    def from_reference(self, value: Fraction) -> Fraction:
        return value


@dataclass
class _Linear(Converter):
    """Represents linear transformation"""

    _factor: Fraction

    def to_reference(self, value: Fraction) -> Fraction:
        return value * self._factor

    def from_reference(self, value: Fraction) -> Fraction:
        return value / self._factor


@dataclass
class _Affine(Converter):
    """Represents affine transformation"""

    _factor: Fraction
    _offset: Fraction

    def to_reference(self, value: Fraction) -> Fraction:
        return (value * self._factor) + self._offset

    def from_reference(self, value: Fraction) -> Fraction:
        return (value - self._offset) / self._factor


@dataclass
class _Composition(Converter):
    """Represents composition transformation"""

    _first: Converter
    _second: Converter

    def to_reference(self, value: Fraction) -> Fraction:
        return self._second.to_reference(self._first.to_reference(value))

    def from_reference(self, value: Fraction) -> Fraction:
        return self._first.from_reference(self._second.from_reference(value))


@dataclass
class _Inverse(Converter):
    """Represents reversed transformation"""

    _inner: Converter

    def to_reference(self, value: Fraction) -> Fraction:
        return self._inner.from_reference(value)

    def from_reference(self, value: Fraction) -> Fraction:
        return self._inner.to_reference(value)


class Converters:
    @staticmethod
    def identity() -> Converter:
        return _Identity()

    @staticmethod
    def linear(value: Number) -> Converter:
        return _Linear(number_to_fraction(value))

    @staticmethod
    def affine(scale: Number, offset: Number) -> Converter:
        return _Affine(number_to_fraction(scale), number_to_fraction(offset))

    @staticmethod
    def compose(first: Converter, second: Converter) -> Converter:
        return _Composition(first, second)

    @staticmethod
    def inverse(converter: Converter) -> Converter:
        return _Inverse(converter)
