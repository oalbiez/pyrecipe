# -*- coding: utf-8 -*-
from ._book_template import BookTemplate
from ._pantry_template import PantryTemplate
from ._template import Template
