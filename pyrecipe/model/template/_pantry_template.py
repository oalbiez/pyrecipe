# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.model.pantry import Pantry

from ._pantry_view import PantryView
from ._template import Template


class PantryTemplate:
    def __init__(self, template: Template) -> None:
        self.template = template

    def process_pantry(self, pantry: Pantry, **kwargs) -> str:
        return self.template.process(pantry=PantryView.create(pantry, **kwargs))
