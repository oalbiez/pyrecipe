# -*- coding: utf-8 -*-
from __future__ import annotations

import pkgutil
from dataclasses import dataclass
from typing import Any

from chevron import render  # type: ignore


def _normalize(text, renderer):
    return " ".join(renderer(text).split()).replace(" .", ".").replace(" ,", ",")


@dataclass
class Template:
    template: str
    symbols: dict[str, Any]
    partials: dict[str, str]

    @staticmethod
    def on_string(template: str) -> Template:
        return Template(template, {"normalize": _normalize}, {})

    @staticmethod
    def on_resource(module: str, resource: str) -> Template:
        content = pkgutil.get_data(module, resource)
        if content:
            return Template(content.decode("utf-8"), {"normalize": _normalize}, {})
        raise ValueError(f"resource not found {resource} in package {module}")

    def with_partial(self, name: str, content: str) -> Template:
        return Template(self.template, self.symbols, self.partials | {name: content})

    def with_symbol(self, name: str, value: Any) -> Template:
        return Template(self.template, self.symbols | {name: value}, self.partials)

    def process(self, **kwargs) -> str:
        return render(
            template=self.template,
            partials_dict=self.partials,
            data=self.symbols | kwargs,
        )
