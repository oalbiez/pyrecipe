# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from pyrecipe.model.pantry import Aliment, Component, Composition, Pantry
from pyrecipe.model.quantities.dimension import Dimension
from pyrecipe.utils.fractional import decimal_view


@dataclass(frozen=True)
class QuantityView:
    full: str
    magnitude: str
    unit: str

    @staticmethod
    def create(value: Dimension) -> QuantityView:
        return QuantityView(
            full=f"{decimal_view(value.magnitude)} {value.symbol}",
            magnitude=decimal_view(value.magnitude),
            unit=value.symbol,
        )


@dataclass(frozen=True)
class ComponentView:
    name: str
    value: QuantityView | None
    has_value: bool

    @staticmethod
    def from_component(
        name: str, component: Component, show_unknown_component: bool
    ) -> ComponentView | None:
        if not component and not show_unknown_component:
            return None
        return component.transform(
            lambda v: ComponentView(
                name=name, value=QuantityView.create(v), has_value=True
            ),
            ComponentView(name=name, value=None, has_value=False),
        )


@dataclass(frozen=True)
class CompositionView:
    components: list[ComponentView]

    @staticmethod
    def create(
        composition: Composition, show_unknown_component: bool
    ) -> CompositionView:
        return CompositionView(
            list(
                filter(
                    None,
                    [
                        ComponentView.from_component(
                            "energy", composition.energy, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "fat", composition.fat, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "fa_saturated",
                            composition.fa_saturated,
                            show_unknown_component,
                        ),
                        ComponentView.from_component(
                            "fa_mono", composition.fa_mono, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "fa_poly", composition.fa_poly, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "carbohydrate",
                            composition.carbohydrate,
                            show_unknown_component,
                        ),
                        ComponentView.from_component(
                            "sugars", composition.sugars, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "polyols", composition.polyols, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "starch", composition.starch, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "fibres", composition.fibres, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "protein", composition.protein, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "salt", composition.salt, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "alcohol", composition.alcohol, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "organic_acids",
                            composition.organic_acids,
                            show_unknown_component,
                        ),
                        ComponentView.from_component(
                            "cholesterol",
                            composition.cholesterol,
                            show_unknown_component,
                        ),
                        ComponentView.from_component(
                            "water", composition.water, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_A", composition.vitamin_A, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_D", composition.vitamin_D, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_E", composition.vitamin_E, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_K1", composition.vitamin_K1, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_K2", composition.vitamin_K2, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_C", composition.vitamin_C, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_B1", composition.vitamin_B1, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_B2", composition.vitamin_B2, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_B3", composition.vitamin_B3, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_B5", composition.vitamin_B5, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_B6", composition.vitamin_B6, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "vitamin_B12",
                            composition.vitamin_B12,
                            show_unknown_component,
                        ),
                        ComponentView.from_component(
                            "vitamin_B9", composition.vitamin_B9, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "sodium", composition.sodium, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "magnesium", composition.magnesium, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "phosphorus", composition.phosphorus, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "chloride", composition.chloride, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "potassium", composition.potassium, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "calcium", composition.calcium, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "manganese", composition.manganese, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "iron", composition.iron, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "copper", composition.copper, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "zinc", composition.zinc, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "selenium", composition.selenium, show_unknown_component
                        ),
                        ComponentView.from_component(
                            "iodine", composition.iodine, show_unknown_component
                        ),
                    ],
                )
            )
        )


@dataclass(frozen=True)
class AlimentView:
    identifier: str
    label: str
    tags: list[str]
    has_tags: bool
    composition: CompositionView

    @staticmethod
    def create(aliment: Aliment, show_unknown_component: bool) -> AlimentView:
        return AlimentView(
            identifier=aliment.identifier.value,
            label=aliment.label,
            tags=[tag.content for tag in aliment.tags],
            has_tags=bool(aliment.tags),
            composition=CompositionView.create(
                aliment.composition, show_unknown_component
            ),
        )


@dataclass(frozen=True)
class PantryView:
    recipes: list[AlimentView]

    @staticmethod
    def create(pantry: Pantry, *, show_unknown_component: bool = False) -> PantryView:
        return PantryView(
            [
                AlimentView.create(r, show_unknown_component)
                for r in pantry.aliments.values()
            ]
        )
