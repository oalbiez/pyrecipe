# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable

from pyrecipe.model.book import Book, Recipe

from ._book_view import BookView
from ._template import Template


class BookTemplate:
    def __init__(self, template: Template) -> None:
        self.template = template

    def process_book(self, book: Book) -> str:
        return self.template.process(book=BookView.create(book))

    def process_recipes(self, recipes: Iterable[Recipe]) -> str:
        return self.template.process(book=BookView.from_recipes(recipes))

    def process_recipe(self, recipe: Recipe) -> str:
        return self.template.process(book=BookView.from_recipes([recipe]))
