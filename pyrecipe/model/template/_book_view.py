# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from dataclasses import dataclass

from pyrecipe.model.book import Book, Durations, Ingredient, Part, Qty, Recipe, Step
from pyrecipe.model.quantities.dimension import Dimension
from pyrecipe.model.quantities.interval import Interval
from pyrecipe.model.reference import Reference
from pyrecipe.utils.fractional import fractional_view


@dataclass
class QuantityView:
    full: str
    magnitude: str
    has_magnitude: bool
    unit: str | None
    has_unit: bool

    @staticmethod
    def create_from_qty(value: Qty) -> QuantityView:
        if value is None:
            return QuantityView(
                full="some",
                magnitude="",
                has_magnitude=False,
                unit=None,
                has_unit=False,
            )
        return QuantityView.create_from_dimension(value)

    @staticmethod
    def create_from_dimension(value: Dimension) -> QuantityView:
        if value.symbol == "#":
            return QuantityView(
                full=fractional_view(value.magnitude),
                magnitude=fractional_view(value.magnitude),
                has_magnitude=True,
                unit=None,
                has_unit=False,
            )
        return QuantityView(
            full=f"{fractional_view(value.magnitude)} {value.symbol}",
            magnitude=fractional_view(value.magnitude),
            has_magnitude=True,
            unit=value.symbol,
            has_unit=True,
        )


@dataclass
class DurationView:
    name: str
    value: QuantityView

    @staticmethod
    def create(durations: Durations) -> list[DurationView]:
        return [
            DurationView(k, QuantityView.create_from_dimension(v))
            for k, v in durations.as_dict().items()
            if v is not None
        ]


@dataclass
class IngredientView:
    qty: QuantityView
    label: str
    reference: str
    has_reference: bool

    @staticmethod
    def create(ingredient: Ingredient) -> IngredientView:
        return IngredientView(
            qty=QuantityView.create_from_qty(ingredient.qty),
            label=ingredient.label,
            reference=_view_reference(ingredient.aliment),
            has_reference=bool(ingredient.aliment),
        )


@dataclass
class IntervalView:
    lower: QuantityView
    upper: QuantityView

    @staticmethod
    def create(part: Interval) -> IntervalView:
        return IntervalView(
            QuantityView.create_from_dimension(part.lower.value),
            QuantityView.create_from_dimension(part.upper.value),
        )


@dataclass
class PartView:
    literal: str | None
    has_literal: bool
    quantity: QuantityView | None
    has_quantity: bool
    interval: IntervalView | None
    has_interval: bool

    @staticmethod
    def create(part: Part) -> PartView:
        if isinstance(part, str):
            return PartView(
                literal=part,
                has_literal=True,
                quantity=None,
                has_quantity=False,
                interval=None,
                has_interval=False,
            )
        if isinstance(part, Interval):
            return PartView(
                literal=None,
                has_literal=False,
                quantity=None,
                has_quantity=False,
                interval=IntervalView.create(part),
                has_interval=True,
            )

        return PartView(
            literal=None,
            has_literal=False,
            quantity=QuantityView.create_from_dimension(part),
            has_quantity=True,
            interval=None,
            has_interval=False,
        )


@dataclass
class StepView:
    ingredients: list[IngredientView]
    has_ingredients: bool
    description: list[PartView]

    @staticmethod
    def create(step: Step) -> StepView:
        return StepView(
            ingredients=[IngredientView.create(i) for i in step.ingredients],
            has_ingredients=bool(step.ingredients),
            description=[PartView.create(p) for p in step.parts],
        )


@dataclass
class RecipeView:
    # pylint: disable=too-many-instance-attributes
    identifier: str
    has_identifier: bool
    name: str
    lang: str
    source: str | None
    has_source: bool
    servings: int | None
    has_servings: bool
    durations: list[DurationView]
    has_durations: bool
    tags: list[str]
    has_tags: bool
    description: str
    has_description: bool
    steps: list[StepView]
    has_steps: bool

    @staticmethod
    def create(recipe: Recipe) -> RecipeView:
        if recipe.source is None:
            source = ""
        else:
            source = recipe.source.value
        return RecipeView(
            identifier=_view_reference(recipe.identifier),
            has_identifier=recipe.identifier is not None,
            name=recipe.name,
            lang=str(recipe.lang),
            source=source,
            has_source=recipe.source is not None,
            servings=recipe.servings,
            has_servings=recipe.servings is not None,
            durations=DurationView.create(recipe.durations),
            has_durations=not recipe.durations.is_empty(),
            tags=[tag.content for tag in recipe.tags],
            has_tags=bool(recipe.tags),
            description=recipe.description,
            has_description=bool(recipe.description),
            steps=[StepView.create(step) for step in recipe.steps],
            has_steps=bool(recipe.steps),
        )


@dataclass
class BookView:
    recipes: list[RecipeView]

    @staticmethod
    def create(book: Book) -> BookView:
        return BookView([RecipeView.create(r) for r in book.recipes])

    @staticmethod
    def from_recipes(recipes: Iterable[Recipe]) -> BookView:
        return BookView([RecipeView.create(r) for r in recipes])


def _view_reference(ref: Reference | None) -> str:
    if ref is None:
        return ""
    return ref.value
