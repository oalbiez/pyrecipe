# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, fields

from pyrecipe.model.quantities.duration import Duration


@dataclass(frozen=True)
class Durations:
    waiting: Duration | None = None
    preparing: Duration | None = None
    cooking: Duration | None = None
    baking: Duration | None = None

    @staticmethod
    def empty():
        return Durations()

    def is_empty(self):
        return (
            self.waiting is None
            and self.preparing is None
            and self.cooking is None
            and self.baking is None
        )

    def as_dict(self) -> dict[str, Duration]:
        return dict((field.name, getattr(self, field.name)) for field in fields(self))
