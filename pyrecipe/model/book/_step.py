# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass

from pyrecipe.model.quantities.duration import Duration
from pyrecipe.model.quantities.interval import Interval
from pyrecipe.model.quantities.length import Length
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.temperature import Temperature
from pyrecipe.model.quantities.volume import Volume

from ._ingredient import Ingredient

Part = str | Duration | Interval | Length | Mass | Scalar | Temperature | Volume


def _transform_part(functor: Callable[[str], str], part: Part) -> Part:
    if isinstance(part, str):
        return functor(part)
    return part


@dataclass(frozen=True)
class Step:
    ingredients: list[Ingredient]
    parts: list[Part]

    def add_ingredients(self, ingredients: list[Ingredient]) -> Step:
        return Step(self.ingredients + ingredients, self.parts)

    def transform_text(self, functor: Callable[[str], str]) -> Step:
        return Step(
            ingredients=[i.transform_text(functor) for i in self.ingredients],
            parts=[_transform_part(functor, part) for part in self.parts],
        )

    def scale(self, ratio: Scalar) -> Step:
        return Step(
            ingredients=[i.scale(ratio) for i in self.ingredients], parts=self.parts
        )
