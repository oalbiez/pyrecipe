# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable, Iterable
from dataclasses import dataclass

from ._recipe import Recipe
from ._types import Servings


@dataclass(frozen=True)
class Book:
    recipes: list[Recipe]

    @staticmethod
    def from_iterable(values: Iterable[Recipe]) -> Book:
        return Book(list(values))

    def homogenises_servings(self, servings: Servings) -> Book:
        return Book([r.homogenises_servings(servings) for r in self.recipes])

    def transform_text(self, functor: Callable[[str], str]) -> Book:
        return Book([r.transform_text(functor) for r in self.recipes])
