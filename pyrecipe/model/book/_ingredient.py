# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass

from pyrecipe.model.quantities.length import Length
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.quantities.volume import Volume
from pyrecipe.model.reference import Reference

Qty = Length | Mass | Scalar | Volume | None


@dataclass(frozen=True)
class Ingredient:
    qty: Qty
    label: str
    aliment: Reference

    def transform_text(self, functor: Callable[[str], str]) -> Ingredient:
        return Ingredient(self.qty, functor(self.label), self.aliment)

    def scale(self, ratio: Scalar) -> Ingredient:
        if self.qty is None:
            return self
        return Ingredient(self.qty * ratio, self.label, self.aliment)
