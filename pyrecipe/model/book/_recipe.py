# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass, field
from fractions import Fraction

from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.reference import Reference
from pyrecipe.model.tag import Tags

from ._durations import Durations
from ._ingredient import Ingredient
from ._step import Step
from ._types import Description, Lang, Name, Servings, Source


@dataclass(frozen=True)
class Recipe:
    # pylint: disable=too-many-instance-attributes
    identifier: Reference | None = None
    name: Name = Name("")
    source: Source | None = None
    lang: Lang = Lang("fr")
    servings: Servings | None = None
    durations: Durations = Durations.empty()
    tags: Tags = field(default_factory=Tags.empty)
    description: Description = field(default_factory=lambda: Description(""))
    steps: list[Step] = field(default_factory=list)

    def id(self) -> Reference:
        # pylint: disable=invalid-name
        if self.identifier:
            return self.identifier
        return Reference(self.name)

    def ingredients(self) -> list[Ingredient]:
        return [i for s in self.steps for i in s.ingredients]

    def update(
        self,
        name: Name | None = None,
        source: Source | None = None,
        lang: Lang | None = None,
        servings: Servings | None = None,
        durations: Durations | None = None,
        tags: Tags | None = None,
        steps: list[Step] | None = None,
    ) -> Recipe:
        # pylint: disable=too-many-arguments, too-many-positional-arguments
        return Recipe(
            identifier=self.identifier,
            name=name or self.name,
            source=source or self.source,
            lang=lang or self.lang,
            servings=servings or servings,
            durations=durations or self.durations,
            tags=tags or self.tags,
            steps=steps or self.steps,
        )

    def homogenises_servings(self, servings: Servings) -> Recipe:
        if self.servings is None:
            return self
        ratio = Scalar.scalar(Fraction(servings, self.servings))
        return self.update(
            servings=servings, steps=[s.scale(ratio) for s in self.steps]
        )

    def transform_text(self, functor: Callable[[str], str]) -> Recipe:
        return self.update(
            name=Name(functor(self.name)),
            tags=Tags.on([t.transform_text(functor) for t in self.tags]),
            steps=[s.transform_text(functor) for s in self.steps],
        )
