# -*- coding: utf-8 -*-
from __future__ import annotations

from unidecode import unidecode

from pyrecipe.utils.predicates import Predicate, all_true

from ._recipe import Recipe


def _strict_match(pattern: str, value: str) -> bool:
    return pattern in unidecode(value.lower())


class Predicates:
    def __init__(self, predicates: list[Predicate[Recipe]] | None = None):
        self.predicates = predicates or []

    def add(self, predicate: Predicate[Recipe]) -> Predicates:
        return Predicates(self.predicates + [predicate])

    def get(self) -> Predicate[Recipe]:
        return all_true(*self.predicates)

    def strict_named(self, pattern) -> Predicates:
        pattern = unidecode(pattern.lower())

        def predicate(recipe: Recipe) -> bool:
            return _strict_match(pattern, recipe.name)

        return self.add(predicate)

    def strict_ingredient(self, pattern: str) -> Predicates:
        pattern = unidecode(pattern.lower())

        def predicate(recipe: Recipe) -> bool:
            for ingredient in recipe.ingredients():
                if _strict_match(pattern, ingredient.label):
                    return True
            return False

        return self.add(predicate)
