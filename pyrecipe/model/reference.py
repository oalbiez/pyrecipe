# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass


@dataclass(frozen=True)
class Reference:
    value: str

    @staticmethod
    def undefined():
        return Reference("")

    def __bool__(self) -> bool:
        return self.value != ""

    def __str__(self) -> str:
        return f"@{self.value}"
