# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field

from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.reference import Reference
from pyrecipe.model.tag import Tags
from pyrecipe.utils.predicates import Predicate

from ._component import Component
from ._composition import Composition


@dataclass(frozen=True)
class Aliment:
    identifier: Reference
    label: str
    tags: Tags = field(default_factory=Tags.empty)
    composition: Composition = field(
        default_factory=lambda: Composition.for100g(energy=Component.unknown())
    )

    @property
    def reference(self) -> Reference:
        return self.identifier

    def composition_for(self, qty: Mass) -> Composition:
        return self.composition.forQ(qty)

    @staticmethod
    def matching(value: str) -> Predicate[Aliment]:
        value = value.lower()

        def predicate(aliment: Aliment) -> bool:
            if value in aliment.label.lower():
                return True
            for tag in aliment.tags:
                if value in tag.content.lower():
                    return True
            return False

        return predicate
