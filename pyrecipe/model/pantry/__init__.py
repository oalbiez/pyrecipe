# -*- coding: utf-8 -*-
from ._aliment import Aliment
from ._component import Component
from ._composition import Composition, compute_vitamin_A
from ._pantry import Pantry
