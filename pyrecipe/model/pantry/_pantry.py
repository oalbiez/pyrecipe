# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from dataclasses import dataclass
from itertools import chain

from pyrecipe.model.book import Ingredient
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.reference import Reference

from ._aliment import Aliment, Predicate
from ._composition import Composition


@dataclass
class Pantry:
    aliments: dict[Reference, Aliment]

    @staticmethod
    def empty() -> Pantry:
        return Pantry({})

    @staticmethod
    def on_list(aliments: Iterable[Aliment]) -> Pantry:
        by_reference: dict[Reference, Aliment] = {}
        for item in aliments:
            if not item.reference:
                raise ValueError(
                    f"cannot add aliment with undefined reference: {repr(item)}"
                )
            by_reference[item.reference] = item
        return Pantry(by_reference)

    def filter(self, predicate: Predicate) -> Pantry:
        return Pantry({k: v for k, v in self.aliments.items() if predicate(v)})

    def add(self, aliment: Aliment) -> Pantry:
        if not aliment.reference:
            raise ValueError("cannot add aliment with undefined reference")
        self.aliments[aliment.reference] = aliment
        return self

    def __add__(self, other: Pantry) -> Pantry:
        return Pantry.on_list(chain(self.aliments.values(), other.aliments.values()))

    def __len__(self) -> int:
        return len(self.aliments)

    def __getitem__(self, reference: Reference) -> Aliment:
        if not reference:
            raise ValueError("cannot get undefined reference")
        if reference not in self.aliments:
            raise ValueError(f"not found: {reference.value}")
        return self.aliments[reference]

    def __contains__(self, reference: Reference) -> bool:
        return reference in self.aliments

    def composition_for(self, ingredients: list[Ingredient]) -> Composition:
        result = Composition.empty()
        for ingredient in ingredients:
            qty = ingredient.qty
            if isinstance(qty, Mass):
                result = result + self[ingredient.aliment].composition_for(qty)
            else:
                raise NotImplementedError("composition_for only support mass quantity")
        return result
