# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass
from typing import Generic, TypeVar

from pyrecipe.model.quantities.energy import Energy
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.number import Number
from pyrecipe.model.quantities.scalar import Scalar

Unit = TypeVar("Unit", Mass, Energy)

# pylint: disable=invalid-name
Type = TypeVar("Type")


@dataclass(frozen=True)
class Component(Generic[Unit]):
    qty: Unit | None

    @staticmethod
    def unknown() -> Component:
        return Component(None)

    @staticmethod
    def on(qty: Unit) -> Component[Unit]:
        # pylint: disable=invalid-name
        return Component(qty)

    @staticmethod
    def g(value: Number) -> Component[Mass]:
        # pylint: disable=invalid-name
        return Component.on(Mass.g(value))

    @staticmethod
    def mg(value: Number) -> Component[Mass]:
        # pylint: disable=invalid-name
        return Component.on(Mass.mg(value))

    @staticmethod
    def ug(value: Number) -> Component[Mass]:
        # pylint: disable=invalid-name
        return Component.on(Mass.ug(value))

    @staticmethod
    def kj(value: Number) -> Component[Energy]:
        # pylint: disable=invalid-name
        return Component.on(Energy.kj(value))

    @staticmethod
    def kcal(value: Number) -> Component[Energy]:
        # pylint: disable=invalid-name
        return Component.on(Energy.kcal(value))

    @property
    def value(self) -> Unit:
        if self.qty is not None:
            return self.qty
        raise ValueError("unknwon component has no value")

    def transform(self, functor: Callable[[Unit], Type], default: Type) -> Type:
        if self.qty is not None:
            return functor(self.qty)
        return default

    def __bool__(self) -> bool:
        return self.qty is not None

    def __str__(self) -> str:
        if self.qty is None:
            return "<unknown>"
        return str(self.qty)

    def __add__(self, right: Component[Unit]) -> Component[Unit]:
        if self.qty is None:
            return right
        if right.qty is None:
            return self
        return Component(self.qty + right.qty)

    def __sub__(self, right: Component[Unit]) -> Component[Unit]:
        if self.qty is None:
            return right
        if right.qty is None:
            return self
        return Component(self.qty - right.qty)

    def __mul__(self, right: Number | Scalar) -> Component[Unit]:
        if self.qty is None:
            return self
        return Component(self.qty * right)

    def __truediv__(self, right: Number | Scalar) -> Component[Unit]:
        if self.qty is None:
            return self
        return Component(self.qty / right)

    def __eq__(self, right: object) -> bool:
        if not isinstance(right, Component):
            return NotImplemented
        return self.qty == right.qty

    def __ne__(self, right: object) -> bool:
        if not isinstance(right, Component):
            return NotImplemented
        return self.qty != right.qty

    def __lt__(self, right: Component) -> bool:
        if self.qty is None or right.qty is None:
            return False
        return self.qty < right.qty

    def __le__(self, right: Component) -> bool:
        if self.qty is None or right.qty is None:
            return False
        return self.qty <= right.qty

    def __gt__(self, right: Component) -> bool:
        if self.qty is None or right.qty is None:
            return False
        return self.qty > right.qty

    def __ge__(self, right: Component) -> bool:
        if self.qty is None or right.qty is None:
            return False
        return self.qty >= right.qty

    def __round__(self, ndigits: int = 0) -> Component:
        if self.qty is None:
            return self
        return Component(self.qty.__round__(ndigits))
