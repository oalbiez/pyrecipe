# -*- coding: utf-8 -*-
from __future__ import annotations

import operator
from collections.abc import Callable
from dataclasses import dataclass, fields
from typing import TypeVar

from pyrecipe.model.quantities.energy import Energy
from pyrecipe.model.quantities.mass import Mass

from ._component import Component


def compute_vitamin_A(retinol: Component, beta_carotene: Component) -> Component:
    # pylint: disable=invalid-name
    if not retinol:
        return Component.unknown()
    return round(retinol + (beta_carotene / 12), 3)  # type: ignore[call-overload]


Param = TypeVar("Param", Mass, Component)
Transformer = Callable[[Param], Param]
Combiner = Callable[[Param, Param], Param]


@dataclass(frozen=True)
class Composition:
    # pylint: disable=invalid-name, too-many-instance-attributes

    reference: Mass = Mass.g(100)
    energy: Component[Energy] = Component.unknown()
    fat: Component[Mass] = Component.unknown()
    fa_saturated: Component[Mass] = Component.unknown()
    fa_mono: Component[Mass] = Component.unknown()
    fa_poly: Component[Mass] = Component.unknown()
    carbohydrate: Component[Mass] = Component.unknown()
    sugars: Component[Mass] = Component.unknown()
    polyols: Component[Mass] = Component.unknown()
    starch: Component[Mass] = Component.unknown()
    fibres: Component[Mass] = Component.unknown()
    protein: Component[Mass] = Component.unknown()
    salt: Component[Mass] = Component.unknown()
    alcohol: Component[Mass] = Component.unknown()
    organic_acids: Component[Mass] = Component.unknown()
    cholesterol: Component[Mass] = Component.unknown()
    water: Component[Mass] = Component.unknown()
    vitamin_A: Component[Mass] = Component.unknown()
    vitamin_D: Component[Mass] = Component.unknown()
    vitamin_E: Component[Mass] = Component.unknown()
    vitamin_K1: Component[Mass] = Component.unknown()
    vitamin_K2: Component[Mass] = Component.unknown()
    vitamin_C: Component[Mass] = Component.unknown()
    vitamin_B1: Component[Mass] = Component.unknown()
    vitamin_B2: Component[Mass] = Component.unknown()
    vitamin_B3: Component[Mass] = Component.unknown()
    vitamin_B5: Component[Mass] = Component.unknown()
    vitamin_B6: Component[Mass] = Component.unknown()
    vitamin_B12: Component[Mass] = Component.unknown()
    vitamin_B9: Component[Mass] = Component.unknown()
    sodium: Component[Mass] = Component.unknown()
    magnesium: Component[Mass] = Component.unknown()
    phosphorus: Component[Mass] = Component.unknown()
    chloride: Component[Mass] = Component.unknown()
    potassium: Component[Mass] = Component.unknown()
    calcium: Component[Mass] = Component.unknown()
    manganese: Component[Mass] = Component.unknown()
    iron: Component[Mass] = Component.unknown()
    copper: Component[Mass] = Component.unknown()
    zinc: Component[Mass] = Component.unknown()
    selenium: Component[Mass] = Component.unknown()
    iodine: Component[Mass] = Component.unknown()

    @staticmethod
    def empty() -> Composition:
        return Composition.on(reference=Mass.g(0), energy=Component.unknown())

    @staticmethod
    def on(
        *,
        reference: Mass,
        energy: Component[Energy],
        **other: Component[Mass],
    ) -> Composition:
        return Composition(reference=reference, energy=energy, **other)

    @staticmethod
    def for100g(
        *,
        energy: Component[Energy],
        **other: Component[Mass],
    ) -> Composition:
        return Composition(reference=Mass.g(100), energy=energy, **other)

    def forQ(self, mass: Mass) -> Composition:
        if self.reference == mass:
            return self
        ratio = Mass.ratio(mass, self.reference)
        return self.transform(lambda v: v * ratio)

    def normalize(self) -> Composition:
        return self.forQ(Mass.g(100))

    def transform(self, functor: Transformer) -> Composition:
        return Composition(
            **{field.name: functor(getattr(self, field.name)) for field in fields(self)}
        )

    def combine(self, other: Composition, functor: Combiner) -> Composition:
        return Composition(
            **{
                field.name: functor(
                    getattr(self, field.name), getattr(other, field.name)
                )
                for field in fields(self)
            }
        )

    def compute_energy(self) -> Energy:
        """Compute energy using Règlement UE N° 1169/2011"""

        def energy_by_g(energy: Energy):
            def apply(mass: Mass) -> Energy:
                return energy * mass.to(Mass.Unit.g).magnitude

            return apply

        def to_g(mass: Mass) -> Mass:
            return mass.to(Mass.Unit.g)

        result: list[Energy] = [
            self.fat.transform(energy_by_g(Energy.kj(37)), Energy.kcal(0)),
            self.alcohol.transform(energy_by_g(Energy.kj(29)), Energy.kcal(0)),
            self.protein.transform(energy_by_g(Energy.kj(17)), Energy.kcal(0)),
            self.organic_acids.transform(energy_by_g(Energy.kj(13)), Energy.kcal(0)),
            self.starch.transform(energy_by_g(Energy.kj(8)), Energy.kcal(0)),
        ]

        if self.carbohydrate and self.polyols:
            carbohydrate = self.carbohydrate.transform(to_g, Mass.g(0)).magnitude
            polyols = self.polyols.transform(to_g, Mass.g(0)).magnitude
            result.append(Energy.kj(17) * (carbohydrate - polyols))
            result.append(Energy.kj(10) * polyols)
        return sum(result, Energy.kj(0))

    def __add__(self, other: Composition) -> Composition:
        return self.combine(other, operator.__add__)

    def __str__(self) -> str:
        return (
            f"Composition(mass={self.reference}"
            f", energy={self.energy}"
            f", fat={self.fat}"
            f", carbohydrate={self.carbohydrate}"
            f", protein={self.protein}"
            f", salt={self.salt}"
            f", water={self.water}"
        )


# fat: Mass.Unit.g
# fa_saturated: Mass.Unit.g
# fa_mono: Mass.Unit.g
# fa_poly: Mass.Unit.g
# carbohydrate: Mass.Unit.g
# sugars: Mass.Unit.g
# polyols: Mass.Unit.g
# starch: Mass.Unit.g
# fibres: Mass.Unit.g
# protein: Mass.Unit.g
# salt: Mass.Unit.g
# alcohol: Mass.Unit.g
# organic_acids: Mass.Unit.g
# cholesterol: Mass.Unit.mg
# water: Mass.Unit.g
# vitamin_A: Mass.Unit.ug
# vitamin_D: Mass.Unit.ug
# vitamin_E: Mass.Unit.mg
# vitamin_K1: Mass.Unit.ug
# vitamin_K2: Mass.Unit.ug
# vitamin_C: Mass.Unit.mg
# vitamin_B1: Mass.Unit.mg
# vitamin_B2: Mass.Unit.mg
# vitamin_B3: Mass.Unit.mg
# vitamin_B5: Mass.Unit.mg
# vitamin_B6: Mass.Unit.mg
# vitamin_B12: Mass.Unit.ug
# vitamin_B9: Mass.Unit.ug
# sodium: Mass.Unit.mg
# magnesium: Mass.Unit.mg
# phosphorus: Mass.Unit.mg
# chloride: Mass.Unit.mg
# potassium: Mass.Unit.mg
# calcium: Mass.Unit.mg
# manganese: Mass.Unit.mg
# iron: Mass.Unit.mg
# copper: Mass.Unit.mg
# zinc: Mass.Unit.mg
# selenium: Mass.Unit.ug
# iodine: Mass.Unit.ug
