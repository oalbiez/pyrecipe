
SOURCES_FILES = $(shell find pyrecipe -type f -name '*.py')
TEST_FILES = $(shell find tests -type f -name '*.py')
SCRIPTS = recipes
PYTHON_FILES = $(SOURCES_FILES) $(TEST_FILES) $(SCRIPTS)
HOOKS = $(shell ls .githooks/*)

init:
	pip install -r requirements.txt

format:
	@black --check .
	@isort --check --diff .

typecheck:
	@mypy \
		--install-types \
		--non-interactive \
		--show-error-codes \
		--linecount-report .report \
		$(PYTHON_FILES)

lint:
	@pylint -j 4 $(PYTHON_FILES)

check: format typecheck lint

test:
	@pytest --hypothesis-profile=dev -m "not slow"

all-test:
	@pytest --hypothesis-profile=dev

debug-test:
	@pytest -vv --hypothesis-profile=dev

coverage:
	@pytest --hypothesis-profile=dev --cov=pyrecipe --cov-report=html:.htmlcov

reformat:
	@black $(PYTHON_FILES)
	@isort $(PYTHON_FILES)

githooks:
	@cp "$(HOOKS)" ".git/hooks/"


ensure-encoding:
	@grep --files-without-match --fixed-strings "# -*- coding" $(PYTHON_FILES)
# | xargs sed -i '' -e "1s/^/# -*- coding: utf-8 -*-\n/"

print: book
	pdfjam --quiet --a4paper --no-landscape --frame true --nup 2x2 --outfile print.pdf book.pdf