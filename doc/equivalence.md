
## Masse volumique g/250ml
Amandes en poudre	250 ml 1-tasse	100 g
Amandes tranchées	250 ml 1-tasse	100 g
Arachides écalées	250 ml 1-tasse	130 g
Avoine(flocons)	250 ml 1-tasse	90 g
Bananes en purée	250 ml 1-tasse	240 g
Basilic haché grossièrement	250 ml 1-tasse	22 g
Beurre	250 ml 1-tasse	225 g
Beurre	15 ml
1 c. à soupe	14 g
Beurre d’amande	250 ml 1-tasse	250 g
Beurre d’arachides	250 ml 1-tasse	276 g
Bleuets	250 ml 1-tasse	150 g
Cacao (poudre)	250 ml 1-tasse	85 g
Canneberges	250 ml 1-tasse	120 g
Canneberges séchées	250 ml 1-tasse	130 g
Carotte râpée	250 ml 1-tasse	95 g
Cassonade	250 ml 1-tasse	200 g
Céréale de grains de riz grillés (Rice Krispies)	250 ml 1-tasse	26 g
Cerises marasquins	250 ml 1-tasse	160 g
Cerises séchées	250 ml 1-tasse	146 g
Chapelure	250 ml 1-tasse	104 g
Chapelure de biscuit graham	250 ml 1-tasse	110 g
Chocolat brisures	250 ml 1-tasse	163 g
Citron, jus	15 ml
1 c. à soupe	15 g
Citrouille, purée	250 ml 1-tasse	240 g
Courgettes râpées	250 ml 1-tasse	125 g
Courgettes tranchées	250 ml 1-tasse	125 g
Couscous isréalien	250 ml 1-tasse	150 g
Crème 35%	250 ml 1-tasse	240 g
Crème sure régulière	250 ml 1-tasse	230 g
Dattes hachées	250 ml 1-tasse	150 g
Farine à gâteau tamisée	250 ml 1-tasse	100 g
Farine à pâtisserie	250 ml 1-tasse	112 g
Farine d’épeautre intégrale	250 ml 1-tasse	120 g
Farine de blé entier	250 ml 1-tasse	125 g
Farine de maïs	250 ml 1-tasse	160 g
Farine de seigle	250 ml 1-tasse	135 g
Farine sarrasin	250 ml 1-tasse	168 g
Farine tout usage	250 ml 1-tasse	125 g
Fraises coupées	250 ml 1-tasse	125 g
Framboises	250 ml 1-tasse	120 g
Fromage cheddar râpé	250 ml 1-tasse	95 g
Fromage feta	250 ml 1-tasse	130 g
Fromage gouda	250 ml 1-tasse	87 g
Fromage gruyère	250 ml 1-tasse	70 g
Fromage mozzarella	250 ml 1-tasse	110 g
Fromage parmesan	250 ml 1-tasse	60 g
Gourgane	250 ml 1-tasse	140 g
Huile d’olive	250 ml 1-tasse	200 g
Jambon en dés	250 ml 1-tasse	125 g
Lait écrémé en poudre	250 ml 1-tasse	100 g
Lait entier 3,25%	250 ml 1-tasse	240 g
Lentilles brunes	250 ml 1-tasse	195 g
Macaroni coupé	250 ml 1-tasse	140 g
Maïs en grains	250 ml 1-tasse	170 g
Mélasse	250 ml 1-tasse	340 g
Miel	15 ml
1c. à soupe
	22 g
Miel	250 ml 1-tasse	355 g
Noix de coco non sucré râpé	250 ml 1-tasse	88 g
Noix de grenoble hachées	250 ml 1-tasse	120 g
Noix de pacanes hachées	250 ml 1-tasse	120 g
Olives noires	250 ml 1-tasse	140 g
Olives vertes farcies	250 ml 1-tasse	120 g
Olives vertes tranchées	250 ml 1-tasse	140 g
Orge perlé	250 ml 1-tasse	200g
Pâte de tomate	250 ml 1-tasse	260 g
Pois jaune sec avec pelure	250 ml 1-tasse	210 g
Pommes en tranches	250 ml 1-tasse	120 g
Poulet cuit haché	250 ml 1-tasse	115 g
Raisins secs	250 ml 1-tasse	150 g
Rhubarbe en dés	250 ml 1-tasse	120 g
Riz	250 ml 1-tasse	200 g
Riz Sauvage	250 ml 1-tasse	175 g
Saindoux	250 ml 1-tasse	220 g
Semoule de maïs	250 ml 1-tasse	145 g
Shortening végétal	250 ml 1-tasse	190 g
Sirop de maïs	250 ml 1-tasse	325 g
Son de blé	250 ml 1-tasse	45 g
Sucre à glacer (non tamisé)	250 ml 1-tasse	113 g
Sucre à glacer (tamisé)	250 ml 1-tasse	95 g
Sucre blanc	250 ml 1-tasse	200 g
Sucre d’érable	250 ml 1-tasse	150 g
Tomate en conserve sans jus	250 ml 1-tasse	250 g

## Masse volumique kg/m3
Abricots (séchés) - 640 kg/m
Ail - 680 kg/m
Ail (haché) - 640 kg/m
Amandes (ensemble) - 720 kg/m
Amandes (sol) - 360 kg/m
Amidon de maïs (farine de maïs) - 640 kg/m
Anchois - 1020 kg/m
Arachides (haché) - 680 kg/m
Arachides (huile de rôti) - 640 kg/m
Arrow-root - 950 kg/m
Avoine (acier-coupe) - 680 kg/m
Avoine (laminé) - 340 kg/m
Bananas (purée) - 970 kg/m
Bananas (tranches) - 760 kg/m
Basilic (séché) - 110 kg/m
Beef (RAW) - 930 kg/m
Beurre - 970 kg/m
Beurre d'arachide - 760 kg/m
Bicarbonate de soude - 870 kg/m
Biscuit mix (Bisquick) - 550 kg/m
Blancs d'œufs - 930 kg/m
Blue farine de maïs - 510 kg/m
Bran (unsifted) - 230 kg/m
Café (instantané) - 230 kg/m
Café (sol) - 380 kg/m
Canneberges - 420 kg/m
Cannelle (sol) - 510 kg/m
Céleri semences - 510 kg/m
Céréales (Rice Krispies) - 90 kg/m
Champignon (bois oreille) - 420 kg/m
Champignons (en tranches) - 280 kg/m
Champignons (entier) - 250 kg/m
Champignons (haché) - 320 kg/m
Champignons (noir chinois) - 210 kg/m
Chapelure de biscuits graham - 380 kg/m
Chocolat (cacao en poudre) - 470 kg/m
Chocolat (fondu) - 1020 kg/m
Chocolat (râpé) - 420 kg/m
Chou (déchiquetés) - 1440 kg/m
Ciboulette (frais, haché) - 210 kg/m
Ciboulette (haché séché) - 30 kg/m
Clous de girofle (entier) - 380 kg/m
Clous de girofle (sol) - 400 kg/m
Cookies Oreo (broyées) - 510 kg/m
Cracker miettes - 250 kg/m
Crème - 510 kg/m
Crème de blé - 760 kg/m
Crème de tartre - 640 kg/m
Crisco (fondu) - 890 kg/m
Crisco (solide) - 930 kg/m
Curcuma (sol) - 590 kg/m
Dates (haché) - 640 kg/m
De poudre à pâte - 760 kg/m
Des miettes de pain (emballé) - 510 kg/m
Des miettes de pain (frais) - 250 kg/m
Eau - 1000 kg/m
Échalotes - 1020 kg/m
Écorces d'orange confit - 530 kg/m
Egg nouilles - 380 kg/m
Épice - 420 kg/m
Épinards (cuits) - 760 kg/m
Farina - 760 kg/m
Farine (pain de blé) - 420 kg/m
Farine (sarrasin) - 720 kg/m
Farine (seigle) - 380 kg/m
Farine (semoule) - 740 kg/m
Farine d'avoine (non cuite) - 340 kg/m
Figues (séchées) - 700 kg/m
Fleurets de chou-fleur - 970 kg/m
Flour (États-Unis tout-usage) - 420 kg/m
Flour (gâteau) - 380 kg/m
Flour (légumineuse) - 550 kg/m
Flour (pomme de terre) - 720 kg/m
Flour (Royaume-Uni auto-sensibilisation) - 470 kg/m
Flour (Sourds Smith) - 550 kg/m
Fraises - 640 kg/m
Fromage (Colby (râpé) - 470 kg/m
Fromage (cottage) - 970 kg/m
Fromage (crème) - 1020 kg/m
Fromage (jack (râpé) - 550 kg/m
Fromage (parmesan râpé) - 760 kg/m
Fromages (cheddar (râpé) - 510 kg/m
Gâteau de miettes (frais) - 380 kg/m
Gélatine - 930 kg/m
Germe de blé - 530 kg/m
Gingembre (frais) - 970 kg/m
Gingembre (sol) - 510 kg/m
Ginger (cristal) - 1020 kg/m
Graines de moutarde - 640 kg/m
Graines de pavot - 570 kg/m
Graines de sésame - 680 kg/m
Grains de poivre (blanc) - 640 kg/m
Grains de poivre (noir) - 570 kg/m
Groseilles - 640 kg/m
Gruau de sarrasin - 720 kg/m
Guimauves (petite) - 210 kg/m
Gumdrops - 680 kg/m
Gummi porte - 640 kg/m
Haricots (secs) - 850 kg/m
Jaunes d'œufs - 1140 kg/m
Kasha - 720 kg/m
L'huile d'olive - 810 kg/m
La farine (de blé entier) - 550 kg/m
La farine (de riz) - 640 kg/m
Lait - 1032 kg/m
Lait (en poudre) - 490 kg/m
Lait (évaporé) - 930 kg/m
Lard - 760 kg/m
Le secteur de la viande (cuits) - 970 kg/m
Lentilles - 850 kg/m
Macaroni (non cuits) - 490 kg/m
Margarine - 930 kg/m
Mayonnaise - 930 kg/m
Mélasse - 1480 kg/m
Miel - 1440 kg/m
Mustard (préparé) - 1060 kg/m
Mustard (sec) - 490 kg/m
Noisettes (ensemble) - 720 kg/m
Noix (haché) - 490 kg/m
Noix (pour sauce) - 510 kg/m
Noix (sol) - 360 kg/m
Noix de cajou (pétrole rôti) - 470 kg/m
Noix de coco (déchiquetés) - 320 kg/m
Noix de pécan (haché) - 510 kg/m
Noix de pécan (pour sauce) - 510 kg/m
Noix de pécan (sol) - 420 kg/m
Noix de raisin - 510 kg/m
Noix du Brésil (au total) - 640 kg/m
Oeufs (battus) - 970 kg/m
Oignon (haché) - 640 kg/m
Oignon (haché) - 850 kg/m
Oignon (tranches) - 550 kg/m
Olives (haché) - 760 kg/m
Orge (non cuits) - 780 kg/m
Paprika - 490 kg/m
Patates douces (brut) - 760 kg/m
Pépites de chocolat - 760 kg/m
Peppers (chili haché) - 720 kg/m
Persil (frais) - 170 kg/m
Pétrole (légume) - 890 kg/m
Pignolias (noix de pin) - 530 kg/m
Pois (non cuits) - 640 kg/m
Pois cassés - 850 kg/m
Pommes (séchées) - 380 kg/m
Pommes (tranches) - 760 kg/m
Pommes de terre (cuites coupées en dés) - 850 kg/m
Pommes de terre (en purée) - 890 kg/m
Pommes de terre (matières premières tranches) - 760 kg/m
Potiron (cuit) - 760 kg/m
Pousses de bambou - 1140 kg/m
Purée de pommes de terre - 890 kg/m
Raisins secs - 640 kg/m
Rice (non cuit) - 890 kg/m
Rice (non cuit Basmati) - 830 kg/m
Rice (sauvages) - 610 kg/m
Riz (cuit à la vapeur) - 680 kg/m
Saindoux - 930 kg/m
Scallions (oignons verts) - 210 kg/m
Sel - 1020 kg/m
Semoule de maïs - 720 kg/m
Sirop (maïs) - 1480 kg/m
Spaghetti (non cuits) - 510 kg/m
Sucre (brun) - 850 kg/m
Sucre (confiserie) - 550 kg/m
Sucre (de ricin) - 810 kg/m
Sucre (en poudre) - 550 kg/m
Sucre (granulé) - 810 kg/m
Sultanines - 640 kg/m
Sweet pommes de terre (cuites) - 1020 kg/m
Thé - 320 kg/m
Thon (en boîte) - 850 kg/m
Tiger Lily fleurs - 170 kg/m
Tomates (haché) - 680 kg/m
Vanille gaufrettes (broyées) - 680 kg/m
Yeast (sèche active) - 1230 kg/m
Zeste d'orange (râpé) - 380 kg/m
Zeste de citron (râpé) - 640 kg/m
Zeste de citron confit - 570 kg/m

