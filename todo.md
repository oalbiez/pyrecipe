# TODO

## Functionality

### Book
- ajout de note dans le déroulé ?
- ajout de variantes ?
- define a way to organize recipes into book, with following properties:
  - can be ordererd
  - human readable
  - can add a recipe and store it at the right place
  - manage multiples languages with translations
  - can list recipes with filter, order
  - can group recipes ?
  - can copy/move recipe from books
  - can add/modify/remove tags

  Possibles solutions:
    - one recipe per file into directory, named by reference and language (how to order)
    - into sqlite DB (not human readable)
    - into a big ordered file

- improve tags :
  - region: for food style (italien, chinese, asian, ...)
  - category: entrée, plat principal, ...
  - diet: vegan, vagetarian, keto, gluten-free


### Pantry
- add DefinedAliment and DerivedAliment
- define a way to organize pantry like books

### Usecases

- export_recipes
- scrap_recipes [normalize_tags, remove_tags, add_tags, parse_instructions, ...]
- order_recipes
- recipe_dispatch_ingredients
- parse_instructions
- recipe_normalize_tags
- map_ingredients_to_aliments
- analyse_recipe
- lint_recipe
- nutriscore_recipes
- list_aliments
- import_aliments
- aliment_normalize_tags
- aliment_remove_tags
- aliment_add_tags
- aliment_transform_tags


### Fuzzy

### Compositions
- Composition computation is based on Mass, but recipes quantities can be scalar, Volume or Mass
- Compute nutritional composition of recipes
- Compute nutriscore of recipes
- unités : https://www.cuisine-et-mets.com/mesures_conversions/

### Linter
- Add linter of recipes


### Scraper
- https://schema.org/Recipe

- 750g
- marmitton
- https://www.cuisine-libre.org/recettes
- https://www.recettes-vegetariennes.org/
- https://www.ptitchef.com/
- http://wiki.a-brest.net/index.php/Recettes_libres
- https://lacerisesurlemaillot.fr/curry-massamn-crevettes/
- https://www.ma-petite-recette.fr
- https://chefsimon.com/
- https://www.iterroir.fr
- https://recettes-vegan.org
- https://bloguedebeatrice.com/
- https://www.soscuisine.fr/

- https://deshydrateraliments.com/recette-celeri-en-poudre/


- https://dryingallfoods.com/vegetables/



## Refactoring
- improve the managment of include (don't open files directly, but returns structure) to be pure
- make intermediate parsers and renderer to be used in tests
- faire des étapes de nettoyage des recettes (réorganisation des ingrédients, normalisation des tags, traductions, identification des quantité dans les instructions, ...) et simplifier les scrapers
- use argparse instead of docopt


## Other

- pre-push should run test only if python files were modified
- https://github.com/johann-petrak/licenseheaders


# Libraries

- https://rich.readthedocs.io/en/latest/index.html, could be usefull for CLI
- https://returns.readthedocs.io
- https://pypi.org/project/PyLaTeX/
- https://pypi.org/project/translate/


## typechecker

### Static
- https://github.com/facebook/pyre-check
- https://github.com/Microsoft/pyright
- https://github.com/google/pytype
- https://mypy.readthedocs.io/

### Dynamic
- https://beartype.readthedocs.io/en/latest/



recipe bookfile=full.book filter=carottes bookfile=my.book:overwrite
recipe bookfile=full.book filter=carottes bookfile=my.book:append


recipe process input=scrapper:mv ouput=bookfile:my.book:append
recipe statistics input=bookfile:my.book
recipe generate input=bookfile:my.book output=latex:book.tex:cuisine
recipe generate input=bookfile:my.book output=latex:book.tex:dehydrated

recipe input=scrapper:mv filter=name:carottes tr=tags:add:mv output=bookfile:all.book:overwrite



## Related projects
- https://github.com/szilagyib/RecipeDSL
- https://github.com/Sharknoon/Cooking-Recipe-DSL
- https://github.com/dfithian/chez-grater
- https://github.com/dfithian/mo-nomz
- https://cooklang.org/ (https://github.com/cooklang/awesome-cooklang-recipes)




<script type="application/ld+json">
  {"@context":"http://schema.org","@type":"Recipe","name":"Soupe à la courgette et au curry","recipeCategory":"soupe","image":["https://assets.afcdn.com/recipe/20181017/82783_w1024h576c1cx1944cy2592cxb3888cyb5184.webp","https://assets.afcdn.com/recipe/20181017/82783_w1024h768c1cx1944cy2592cxb3888cyb5184.webp","https://assets.afcdn.com/recipe/20181017/82783_w1024h1024c1cx1944cy2592cxb3888cyb5184.webp","https://assets.afcdn.com/recipe/20181017/82783_w1024h576c1cx1944cy2592cxb3888cyb5184.jpg","https://assets.afcdn.com/recipe/20181017/82783_w1024h768c1cx1944cy2592cxb3888cyb5184.jpg","https://assets.afcdn.com/recipe/20181017/82783_w1024h1024c1cx1944cy2592cxb3888cyb5184.jpg"],"datePublished":"2006-10-23T11:43:00+02:00","prepTime":"PT30M","cookTime":"PT30M","totalTime":"PT60M","recipeYield":"6 personnes","recipeIngredient":["1 kg de courgette","1 oignon","curry","poivre","sel","huile","300 g de pomme de terre","1 gousse d'ail","bouillon de volaille"],"recipeInstructions":[{"@type":"HowToStep","text":"Découpez en cubes les courgettes et les pommes de terre épluchées. Coupez les oignons grossièrement."},{"@type":"HowToStep","text":"Préparez du bouillon de volaille (frais ou lyophilisé)."},{"@type":"HowToStep","text":"Dans une cocotte, faites chauffez 2 cuillères à soupe d'huile. Quand celle ci est bien chaude, versez les oignons et l'ail et saupoudrez de curry (vous pouvez être généreux mais si la présence du curry vous surprend, mettez en peu la 1ère fois)."},{"@type":"HowToStep","text":"Remuez constamment, le temps que les oignons s'imprègnent de curry. Versez 1 l d'eau (y compris le bouillon)."},{"@type":"HowToStep","text":"Ajoutez les pommes de terre et 5 à 10 mn plus tard, les dés de courgettes. "},{"@type":"HowToStep","text":"Laissez cuire 20 min. La soupe ne doit pas bouillir. Salez et poivrez."},{"@type":"HowToStep","text":"Mixez (de préférence le plus possible, cette soupe est meilleure lisse). C'est prêt!"}],"author":"emmy_16450253","description":"courgette, oignon, curry, poivre, sel, huile, pomme de terre, ail, bouillon de volaille","keywords":"Soupe à la courgette et au curry, courgette, oignon, curry, poivre, sel, huile, pomme de terre, ail, bouillon de volaille, soupe, facile, bon marché","recipeCuisine":"Entrée","aggregateRating":{"@type":"AggregateRating","reviewCount":325,"ratingValue":4.7,"worstRating":0,"bestRating":5},"video":{"@type":"VideoObject","name":"Soupe à la courgette et au curry","description":"Soupe à la courgette et au curry","thumbnailUrl":["https://assets.afcdn.com/recipe/20181017/82783_w1024h576c1cx1944cy2592cxb3888cyb5184.webp","https://assets.afcdn.com/recipe/20181017/82783_w1024h768c1cx1944cy2592cxb3888cyb5184.webp","https://assets.afcdn.com/recipe/20181017/82783_w1024h1024c1cx1944cy2592cxb3888cyb5184.webp","https://assets.afcdn.com/recipe/20181017/82783_w1024h576c1cx1944cy2592cxb3888cyb5184.jpg","https://assets.afcdn.com/recipe/20181017/82783_w1024h768c1cx1944cy2592cxb3888cyb5184.jpg","https://assets.afcdn.com/recipe/20181017/82783_w1024h1024c1cx1944cy2592cxb3888cyb5184.jpg"],"contentUrl":"https://assets.afcdn.com/video13/20181016/v541449_16223-soupe-courgette-curry-1080-1080-fr-pad-mp4_HD.mp4","embedUrl":"https://aufeminin.com/reloaded/embed/video/541449","uploadDate":"2018-10-16 18:10:00.000000"}}

  </script>














v:Energy = 1000J

print (v en kJ) -> 1KJ
print (v) -> 1000J

print (magnitude de v) -> 1000
print (unité de v) -> J


w: Mass = v // erreur de compile, Energy != Mass





