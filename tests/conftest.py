# -*- coding: utf-8 -*-
import os

from hypothesis import HealthCheck, Verbosity, settings

settings.register_profile(
    "ci",
    max_examples=500,
    suppress_health_check=[HealthCheck.differing_executors],
)
settings.register_profile(
    "dev",
    max_examples=10,
    suppress_health_check=[HealthCheck.differing_executors],
)
settings.register_profile(
    "debug",
    max_examples=10,
    verbosity=Verbosity.verbose,
    suppress_health_check=[HealthCheck.differing_executors],
)
settings.load_profile(os.getenv("HYPOTHESIS_PROFILE", "default"))
