# -*- coding: utf-8 -*-
from __future__ import annotations

import pytest

from pyrecipe.model.template import Template


def test_template_should_render_template() -> None:
    assert Template.on_string("Hello {{name}}!").process(name="world") == "Hello world!"


def test_template_should_render_template_from_resource() -> None:
    assert (
        Template.on_resource("tests.model.template", "template.mustache").process(
            name="world"
        )
        == "Hello world!"
    )


def test_template_should_fail_when_requesting_template_from_unknown_package() -> None:
    with pytest.raises(ValueError):
        _ = Template.on_resource("tests.unknown", "any.mustache")


def test_template_should_fail_when_requesting_unknown_template() -> None:
    with pytest.raises(FileNotFoundError):
        _ = Template.on_resource("tests.model.template", "unknown.mustache")


def test_template_should_render_template_with_symbol() -> None:
    assert (
        Template.on_string("Hello {{name}}!")
        .with_symbol(name="name", value="world")
        .process()
        == "Hello world!"
    )


def test_template_should_render_template_with_partial() -> None:
    template = Template.on_string("Hello {{> do_name}}!").with_partial(
        name="do_name", content="{{name}}"
    )
    assert template.process(name="world") == "Hello world!"


def test_template_should_render_template_with_normalize() -> None:
    template = Template.on_string(
        "{{#normalize}} Text    to    normalize   "
        ", respecting  typography    .  {{/normalize}}"
    )
    assert template.process() == "Text to normalize, respecting typography."
