# -*- coding: utf-8 -*-
from textwrap import dedent

from pyrecipe.model.book import Servings
from pyrecipe.model.dsl import DSL


def test_recipe_should_list_its_ingredients() -> None:
    recipe = DSL.parse_recipe(
        dedent(
            """
            recipe:
                --
                <20g> flour
                <2l> water
            """
        ).lstrip()
    )
    assert len(recipe.ingredients()) == 2


def test_recipe_should_scale_when_servings_is_specified() -> None:
    recipe = DSL.parse_recipe(
        dedent(
            """
            recipe:
                servings: 2
                --
                <20g> flour
            """
        ).lstrip()
    )
    result = DSL.render_recipe(recipe.homogenises_servings(Servings(4)))
    assert "servings: 4" in result
    assert "<40 g>" in result


def test_recipe_should_not_scale_when_servings_is_unspecified() -> None:
    recipe = DSL.parse_recipe(
        dedent(
            """
            recipe:
                --
                <20g> flour
            """
        ).lstrip()
    )
    result = DSL.render_recipe(recipe.homogenises_servings(Servings(4)))
    assert "<20 g>" in result
