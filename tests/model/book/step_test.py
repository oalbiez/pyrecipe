# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.model.book import Ingredient, Step
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.reference import Reference


def flour(qty: Mass | None) -> Ingredient:
    return Ingredient(qty, "flour", Reference.undefined())


def test_step_should_scale() -> None:
    assert Step(ingredients=[flour(Mass.g(20))], parts=[]).scale(
        Scalar.scalar(2)
    ) == Step(ingredients=[flour(Mass.g(40))], parts=[])


def test_step_should_be_able_to_add_ingredients() -> None:
    assert Step(ingredients=[], parts=[]).add_ingredients([flour(Mass.g(20))]) == Step(
        ingredients=[flour(Mass.g(20))], parts=[]
    )
