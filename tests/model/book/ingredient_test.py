# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.model.book import Ingredient
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.model.reference import Reference


def flour(qty: Mass | None) -> Ingredient:
    return Ingredient(qty, "flour", Reference.undefined())


def test_ingredient_should_scale() -> None:
    assert flour(Mass.g(20)).scale(Scalar.scalar(3)) == flour(Mass.g(60))


def test_ingredient_should_not_scale_when_no_quantity() -> None:
    assert flour(None).scale(Scalar.scalar(3)) == flour(None)


def test_ingredient_should_be_transformable() -> None:
    assert Ingredient(None, "flour", Reference.undefined()).transform_text(
        lambda v: v.upper()
    ) == Ingredient(None, "FLOUR", Reference.undefined())
