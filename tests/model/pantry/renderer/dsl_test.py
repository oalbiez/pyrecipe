# -*- coding: utf-8 -*-
from textwrap import dedent

from pyrecipe.model.dsl import DSL

pantry = DSL.parse_pantry(
    dedent(
        """
        aliment: the aliment @ref
            composition:
                energy: 20.3 kcal
                fat: 0.25 g
    """
    )
)


def test_pantry_should_render_without_unknown_values() -> None:
    assert "<unknown>" not in DSL.render_pantry(pantry, show_unknown_component=False)


def test_pantry_should_render_with_unknown_values() -> None:
    assert "<unknown>" in DSL.render_pantry(pantry, show_unknown_component=True)
