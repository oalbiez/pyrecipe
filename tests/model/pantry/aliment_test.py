# -*- coding: utf-8 -*-
from pyrecipe.model.pantry import Aliment, Component, Composition
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.reference import Reference
from pyrecipe.model.tag import Tag, Tags

predicate = Aliment.matching("some")


def test_aliment_matching_should_failed_when_no_match() -> None:
    assert predicate(Aliment(Reference("ref"), "label")) is False


def test_aliment_matching_should_succed_when_match_in_label() -> None:
    assert predicate(Aliment(Reference("ref"), "some label")) is True
    assert predicate(Aliment(Reference("ref"), "SOME label")) is True


def test_aliment_matching_should_succed_when_match_in_tags() -> None:
    assert (
        predicate(Aliment(Reference("ref"), "label", tags=Tags.on([Tag("some tag")])))
        is True
    )
    assert (
        predicate(Aliment(Reference("ref"), "label", tags=Tags.on([Tag("SOME tag")])))
        is True
    )


def test_aliment_should_provide_composition_for_quantity() -> None:
    aliment = Aliment(
        Reference("ref"),
        "label",
        composition=Composition.for100g(energy=Component.kcal(100)),
    )
    assert aliment.composition_for(Mass.kg(1)).energy == Component.kcal(1000)
