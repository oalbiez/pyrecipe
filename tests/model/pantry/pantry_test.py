# -*- coding: utf-8 -*-
import string
from textwrap import dedent

import pytest
from hypothesis import given
from hypothesis.strategies import lists, text

from pyrecipe.model.book import Ingredient
from pyrecipe.model.dsl import DSL
from pyrecipe.model.pantry import Aliment, Component, Composition, Pantry
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.volume import Volume
from pyrecipe.model.reference import Reference


def aliment_names():
    return lists(
        text(alphabet=string.ascii_letters, min_size=10),
        min_size=1,
        max_size=100,
        unique=True,
    )


def test_pantry_should_have_a_null_size_when_empty() -> None:
    assert len(Pantry.empty()) == 0


def test_pantry_should_reject_aliment_without_reference() -> None:
    with pytest.raises(ValueError):
        _ = Pantry.on_list([Aliment(Reference.undefined(), "aliment")])


@given(aliment_names())
def test_pantry_should_have_a_non_null_size_when_filled(names: list[str]) -> None:
    assert len(pantry_on(names)) == len(names)


@given(aliment_names())
def test_pantry_should_have_contains_operator_matching_success(
    names: list[str],
) -> None:
    pantry = pantry_on(names)
    for name in names:
        assert Reference(name) in pantry


@given(aliment_names())
def test_pantry_should_have_contains_operator_matching_failed(names: list[str]) -> None:
    pantry = Pantry.empty()
    for name in names:
        assert Reference(name) not in pantry


@given(aliment_names())
def test_pantry_should_provide_aliment_by_reference_when_found(
    names: list[str],
) -> None:
    pantry = pantry_on(names)
    for name in names:
        assert pantry[Reference(name)].label == name


@given(aliment_names())
def test_pantry_should_provide_aliment_by_reference_when_not_found(
    names: list[str],
) -> None:
    pantry = Pantry.empty()
    for name in names:
        with pytest.raises(ValueError):
            _ = pantry[Reference(name)]


def test_pantry_should_provide_aliment_by_reference_when_undefined_reference() -> None:
    with pytest.raises(ValueError):
        _ = Pantry.empty()[Reference.undefined()]


@given(aliment_names())
def test_pantry_should_add_aliment(names: list[str]) -> None:
    pantry = Pantry.empty()
    for name in names:
        pantry = pantry.add(Aliment(Reference(name), name))
    assert len(pantry) == len(names)


def test_pantry_should_reject_addition_of_aliment_without_reference() -> None:
    pantry = Pantry.empty()
    with pytest.raises(ValueError):
        _ = pantry.add(Aliment(Reference.undefined(), "aliment"))


@given(aliment_names())
def test_pantry_should_filter_aliment(names: list[str]) -> None:
    pantry = pantry_on(names).filter(lambda a: "a" in a.label)
    for name in names:
        if "a" in name:
            assert Reference(name) in pantry
        else:
            assert Reference(name) not in pantry


@given(aliment_names())
def test_pantry_should_be_composable(names: list[str]) -> None:
    pantry = pantry_on(names[0:10]) + pantry_on(names[10:])
    assert len(pantry) == len(names)


def pantry_on(values: list[str]) -> Pantry:
    return Pantry.on_list([Aliment(Reference(value), value) for value in values])


minimal = dedent(
    """
    aliment: one @one
        composition:
            energy: 20 kcal
            fat: 0.25 g
            carbohydrate: 4 g
            fibres: 1 g
            protein: 0.5 g

    aliment: two @two
        composition:
            energy: 100 kcal
            fat: 3 g
            carbohydrate: 2 g
            fibres: 10 g
            protein: 30 g
    """
)


def test_pantry_should_compute_composition_of_ingredients_when_no_ingredient() -> None:
    pantry = DSL.parse_pantry(minimal)
    assert pantry.composition_for([]) == Composition.empty()


def test_pantry_should_compute_composition_of_ingredients_with_ingredient() -> None:
    pantry = DSL.parse_pantry(minimal)
    ingredients = [
        Ingredient(Mass.g(200), "of one", Reference("one")),
        Ingredient(Mass.g(50), "of two", Reference("two")),
    ]
    assert pantry.composition_for(ingredients) == Composition.on(
        reference=Mass.g(250),
        energy=Component.kcal(90),
        fat=Component.g(2),
        carbohydrate=Component.g(9),
        fibres=Component.g(7),
        protein=Component.g(16),
    )


def test_pantry_should_reject_composition_of_unknown_ingredient() -> None:
    pantry = Pantry.empty()
    ingredients = [
        Ingredient(Mass.g(200), "of one", Reference("one")),
    ]
    with pytest.raises(ValueError):
        _ = pantry.composition_for(ingredients)


def test_pantry_should_reject_composition_of_ingredient_without_mass() -> None:
    pantry = DSL.parse_pantry(minimal)
    ingredients = [
        Ingredient(Volume.ml(200), "of one", Reference("one")),
    ]
    with pytest.raises(NotImplementedError):
        _ = pantry.composition_for(ingredients)
