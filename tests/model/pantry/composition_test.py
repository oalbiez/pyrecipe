# -*- coding: utf-8 -*-
from pyrecipe.model.pantry import Component, Composition, compute_vitamin_A
from pyrecipe.model.quantities.mass import Mass


def test_computation_of_vitamin_A() -> None:
    # pylint: disable=invalid-name
    assert compute_vitamin_A(Component.ug(0), Component.ug(0)) == Component.ug(0)
    assert compute_vitamin_A(Component.ug(5), Component.ug(0)) == Component.ug(5)
    assert compute_vitamin_A(Component.ug(37), Component.ug(8)) == Component.ug(
        "37.667"
    )

    assert (
        compute_vitamin_A(Component.unknown(), Component.ug(100)) == Component.unknown()
    )
    assert compute_vitamin_A(Component.ug(10), Component.unknown()) == Component.ug(10)


def test_composition_should_scale_with_quantity() -> None:
    composition = Composition.for100g(energy=Component.kcal(100))
    scaled = composition.forQ(Mass.g(200))
    assert scaled.reference == Mass.g(200)
    assert scaled.energy == Component.kcal(200)


def test_composition_should_scale_with_quantity_do_nothing() -> None:
    composition = Composition.for100g(energy=Component.kcal(100))
    scaled = composition.forQ(Mass.g(100))
    assert scaled.reference == Mass.g(100)
    assert scaled.energy == Component.kcal(100)


def test_composition_should_normalize_to_100g() -> None:
    composition = Composition.on(reference=Mass.g(50), energy=Component.kcal(100))
    normalized = composition.normalize()
    assert normalized.reference == Mass.g(100)
    assert normalized.energy == Component.kcal(200)


def test_composition_should_render_most_important_components() -> None:
    composition = Composition.for100g(energy=Component.kcal(10))
    assert (
        str(composition) == "Composition(mass=100 g"
        ", energy=10 kcal"
        ", fat=<unknown>"
        ", carbohydrate=<unknown>"
        ", protein=<unknown>"
        ", salt=<unknown>"
        ", water=<unknown>"
    )


def test_composition_should_be_sumable() -> None:
    first = Composition.for100g(energy=Component.kcal(10))
    second = Composition.for100g(energy=Component.kcal(20))
    assert (first + second).reference == Mass.g(200)
    assert (first + second).energy == Component.kcal(30)


def test_composition_should_compute_energy() -> None:
    """Compute energy using Règlement UE N° 1169/2011"""
    composition = Composition.for100g(
        energy=Component.unknown(),
        fat=Component.g(10),
        alcohol=Component.g(2),
        protein=Component.g(30),
        organic_acids=Component.g(5),
        starch=Component.g(40),
        carbohydrate=Component.g(10),
        polyols=Component.g(3),
    )
    assert str(composition.compute_energy()) == "1472 kJ"
