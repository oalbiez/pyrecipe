# -*- coding: utf-8 -*-
from fractions import Fraction

import pytest
from hypothesis import assume, given
from hypothesis.strategies import integers

from pyrecipe.model.pantry import Component
from pyrecipe.model.quantities.mass import Mass, Number
from pyrecipe.model.quantities.unit_factory import UnitFactory

_g = UnitFactory.unit("gram", "g")


def qty(value: Number) -> Component[Mass]:
    return Component.on(Mass.g(value))


def unknwon() -> Component:
    return Component.unknown()


def test_component_should_be_on_mass() -> None:
    assert str(Component.g(2)) == "2 g"
    assert str(Component.mg(2)) == "2 mg"
    assert str(Component.ug(2)) == "2 µg"


def test_component_should_be_on_energy() -> None:
    assert str(Component.kj(2)) == "2 kJ"
    assert str(Component.kcal(2)) == "2 kcal"


def test_component_should_be_readable() -> None:
    assert str(Component.on(Mass.g(2))) == "2 g"
    assert str(unknwon()) == "<unknown>"


def test_component_should_have_value() -> None:
    assert str(Component.g(2).value) == "2 g"
    assert str(Component.mg(2).value) == "2 mg"
    assert str(Component.ug(2).value) == "2 µg"


def test_unknown_component_should_not_have_value() -> None:
    with pytest.raises(ValueError):
        _ = Component.unknown().value


@given(integers(), integers())
def test_component_should_have_addition_operation_with_defined_values(
    left: int, right: int
) -> None:
    assert qty(left) + qty(right) == qty(left + right)


@given(integers())
def test_component_should_have_addition_operation_with_undefined_values(
    value: int,
) -> None:
    assert qty(value) + unknwon() == unknwon() + qty(value) == qty(value)


@given(integers(), integers())
def test_component_should_have_substraction_operation_with_defined_values(
    left: int, right: int
) -> None:
    assert qty(left) - qty(right) == qty(left - right)


@given(integers())
def test_component_should_have_substraction_operation_with_undefined_values(
    value: int,
) -> None:
    assert qty(value) - unknwon() == unknwon() - qty(value) == qty(value)


@given(integers(), integers())
def test_component_should_have_multiplication_operation_with_defined_values(
    left: int, right: int
) -> None:
    assert qty(left) * right == qty(left * right)


@given(integers())
def test_component_should_have_multiplication_operation_with_undefined_values(
    value: int,
) -> None:
    assert unknwon() * value == unknwon()


@given(integers(), integers())
def test_component_should_have_division_operation_with_defined_values(
    left: int, right: int
) -> None:
    assume(right != 0)
    assert qty(left) / right == qty(Fraction(left, right))


@given(integers())
def test_component_should_have_division_operation_with_undefined_values(
    value: int,
) -> None:
    assume(value != 0)
    assert unknwon() / value == unknwon()


def test_component_should_have_equal_operation_with_bad_values() -> None:
    # pylint: disable=unnecessary-dunder-call
    assert qty(1).__eq__(None) == NotImplemented
    assert unknwon().__eq__(None) == NotImplemented


@given(integers())
def test_component_should_have_equal_operation_with_defined_values(value: int) -> None:
    assert qty(value) == qty(value)


def test_component_should_have_equal_operation_with_undefined_values() -> None:
    assert unknwon() == unknwon()


def test_component_should_have_not_equal_operation_with_bad_values() -> None:
    # pylint: disable=unnecessary-dunder-call
    assert qty(1).__ne__(None) == NotImplemented
    assert unknwon().__ne__(None) == NotImplemented


@given(integers(), integers())
def test_component_should_have_not_equal_operation_with_defined_values(
    left: int, right: int
) -> None:
    assume(left != right)
    assert qty(left) != qty(right)


@given(integers())
def test_component_should_have_not_equal_operation_with_undefined_values(
    value: int,
) -> None:
    assert qty(value) != unknwon()
    assert unknwon() != qty(value)


@given(integers(), integers())
def test_component_should_have_grether_than_operation_with_defined_values(
    left: int, right: int
) -> None:
    assume(left > right)
    assert qty(left) > qty(right)


@given(integers())
def test_component_should_have_grether_than_operation_with_undefined_values(
    value: int,
) -> None:
    # pylint: disable=unneeded-not
    assert not qty(value) > unknwon()
    assert not unknwon() > qty(value)


@given(integers(), integers())
def test_component_should_have_grether_or_equal_operation_with_defined_values(
    left: int, right: int
) -> None:
    assume(left >= right)
    assert qty(left) >= qty(right)


@given(integers())
def test_component_should_have_grether_or_equal_operation_with_undefined_values(
    value: int,
) -> None:
    # pylint: disable=unneeded-not
    assert not qty(value) >= unknwon()
    assert not unknwon() >= qty(value)


@given(integers(), integers())
def test_component_should_have_less_than_operation_with_defined_values(
    left: int, right: int
) -> None:
    assume(left < right)
    assert qty(left) < qty(right)


@given(integers())
def test_component_should_have_less_than_operation_with_undefined_values(
    value: int,
) -> None:
    # pylint: disable=unneeded-not
    assert not qty(value) < unknwon()
    assert not unknwon() < qty(value)


@given(integers(), integers())
def test_component_should_have_less_or_equal_operation_with_defined_values(
    left: int, right: int
) -> None:
    assume(left <= right)
    assert qty(left) <= qty(right)


@given(integers())
def test_component_should_have_less_or_equal_operation_with_undefined_values(
    value: int,
) -> None:
    # pylint: disable=unneeded-not
    assert not qty(value) <= unknwon()
    assert not unknwon() <= qty(value)


def test_component_should_round_with_defined_value() -> None:
    value = qty("1.1235")
    assert str(round(value, 0)) == "1 g"  # type: ignore[call-overload]
    assert str(round(value, 1)) == "1.1 g"  # type: ignore[call-overload]
    assert str(round(value, 2)) == "1.12 g"  # type: ignore[call-overload]
    assert str(round(value, 3)) == "1.124 g"  # type: ignore[call-overload]


def test_component_should_round_with_undefined_value() -> None:
    assert round(unknwon(), 2) == unknwon()  # type: ignore[call-overload]


def test_component_should_transform_with_defined_value() -> None:
    assert qty("1").transform(lambda m: m * 2, Mass.g(0)) == Mass.g(2)


def test_component_should_transform_with_undefined_value() -> None:
    assert unknwon().transform(lambda m: m * 2, Mass.g(42)) == Mass.g(42)
