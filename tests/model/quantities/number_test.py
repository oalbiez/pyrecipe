from fractions import Fraction

import pytest

from pyrecipe.model.quantities.number import number_to_fraction


def test_number_to_fraction_from_string() -> None:
    assert number_to_fraction("3") == Fraction("3")
    assert number_to_fraction("1 1/2") == Fraction("1.5")
    assert number_to_fraction("5 1/4") == Fraction("5.25")


def test_number_to_fraction_from_int() -> None:
    assert number_to_fraction(3) == Fraction("3")


def test_number_to_fraction_from_fraction() -> None:
    assert number_to_fraction(Fraction(3)) == Fraction("3")


def test_number_to_fraction_from_float() -> None:
    assert number_to_fraction(Fraction(3.0)) == Fraction("3")


def test_number_to_fraction_when_error() -> None:
    with pytest.raises(ValueError):
        _ = number_to_fraction("not a number")
