# -*- coding: utf-8 -*-
import pytest

from pyrecipe.model.quantities.interval import Interval, LowerBound, UpperBound
from pyrecipe.model.quantities.mass import Mass


class TestLowerBound:
    def test_closed_lower_bound_should_be_closed(self) -> None:
        assert LowerBound.closed(Mass.g(0)).is_closed is True

    def test_closed_lower_bound_should_have_value(self) -> None:
        assert LowerBound.closed(Mass.g(0)).value == Mass.g(0)

    def test_closed_lower_bound_should_contains_bound(self) -> None:
        assert LowerBound.closed(Mass.g(0)).is_lower(Mass.g(0)) is True

    def test_closed_lower_bound_should_have_repr(self) -> None:
        assert (
            repr(LowerBound.closed(Mass.g(0)))
            == "LowerBound.closed(Mass.on('0', Mass.Unit.g))"
        )

    def test_opened_lower_bound_should_not_be_closed(self) -> None:
        assert LowerBound.opened(Mass.g(0)).is_closed is False

    def test_opened_lower_bound_should_have_value(self) -> None:
        assert LowerBound.opened(Mass.g(0)).value == Mass.g(0)

    def test_opened_lower_bound_should_not_contains_bound(self) -> None:
        assert LowerBound.opened(Mass.g(0)).is_lower(Mass.g(0)) is False

    def test_opened_lower_bound_should_have_repr(self) -> None:
        assert (
            repr(LowerBound.opened(Mass.g(0)))
            == "LowerBound.opened(Mass.on('0', Mass.Unit.g))"
        )

    def test_inifinte_lower_bound_should_not_be_closed(self) -> None:
        assert LowerBound.inf().is_closed is False

    def test_inifinte_lower_bound_should_not_have_value(self) -> None:
        with pytest.raises(ValueError):
            _ = LowerBound.inf().value

    def test_inifinte_lower_bound_should_contains_all_values(self) -> None:
        assert LowerBound.inf().is_lower(Mass.g(0)) is True
        assert LowerBound.inf().is_lower(Mass.g(-10000)) is True

    def test_inifinte_lower_bound_should_have_repr(self) -> None:
        assert repr(LowerBound.inf()) == "LowerBound.inf()"


class TestUpperBound:
    def test_closed_upper_bound_should_be_closed(self) -> None:
        assert UpperBound.closed(Mass.g(0)).is_closed is True

    def test_closed_upper_bound_should_have_value(self) -> None:
        assert UpperBound.closed(Mass.g(0)).value == Mass.g(0)

    def test_closed_upper_bound_should_contains_bound(self) -> None:
        assert UpperBound.closed(Mass.g(0)).is_upper(Mass.g(0)) is True

    def test_closed_upper_bound_should_have_repr(self) -> None:
        assert (
            repr(UpperBound.closed(Mass.g(0)))
            == "UpperBound.closed(Mass.on('0', Mass.Unit.g))"
        )

    def test_opened_upper_bound_should_not_be_closed(self) -> None:
        assert UpperBound.opened(Mass.g(0)).is_closed is False

    def test_opened_upper_bound_should_have_value(self) -> None:
        assert UpperBound.opened(Mass.g(0)).value == Mass.g(0)

    def test_opened_upper_bound_should_not_contains_bound(self) -> None:
        assert UpperBound.opened(Mass.g(0)).is_upper(Mass.g(0)) is False

    def test_opened_upper_bound_should_have_repr(self) -> None:
        assert (
            repr(UpperBound.opened(Mass.g(0)))
            == "UpperBound.opened(Mass.on('0', Mass.Unit.g))"
        )

    def test_inifinte_upper_bound_should_not_be_closed(self) -> None:
        assert UpperBound.inf().is_closed is False

    def test_inifinte_upper_bound_should_not_have_value(self) -> None:
        with pytest.raises(ValueError):
            _ = UpperBound.inf().value

    def test_inifinte_upper_bound_should_contains_all_values(self) -> None:
        assert UpperBound.inf().is_upper(Mass.g(0)) is True
        assert UpperBound.inf().is_upper(Mass.g(-10000)) is True

    def test_inifinte_upper_bound_should_have_repr(self) -> None:
        assert repr(UpperBound.inf()) == "UpperBound.inf()"


class TestInterval:
    def test_interval_should_contains_values(self) -> None:
        assert Mass.g(0) in Interval.between(Mass.g(0), Mass.g(10))
        assert Mass.g(10) in Interval.between(Mass.g(0), Mass.g(10))

    def test_interval_should_have_repr(self) -> None:
        assert (
            repr(Interval.between(Mass.g(0), Mass.g(10))) == "Interval("
            "LowerBound.closed(Mass.on('0', Mass.Unit.g)), "
            "UpperBound.closed(Mass.on('10', Mass.Unit.g)))"
        )
