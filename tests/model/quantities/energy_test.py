# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.energy import Energy

from .operators_helper import Operators


def test_energy_should_be_convertible_on_all_energy_units() -> None:
    value = Energy.joules(4184)
    assert value.to(Energy.Unit.cal).magnitude == Fraction("1000")
    assert value.to(Energy.Unit.kcal).magnitude == Fraction("1")


def test_mass_unit() -> None:
    assert Energy.joules(3).unit == Energy.Unit.J
    assert Energy.kj(3).unit == Energy.Unit.kJ
    assert Energy.cal(3).unit == Energy.Unit.cal
    assert Energy.kcal(3).unit == Energy.Unit.kcal


def test_energy_rendering() -> None:
    assert str(Energy.joules(3)) == "3 J"
    assert str(Energy.kj(3)) == "3 kJ"
    assert str(Energy.cal(3)) == "3 cal"
    assert str(Energy.kcal(3)) == "3 kcal"


def test_energy_symbol() -> None:
    assert Energy.joules(3).symbol == "J"
    assert Energy.kj(3).symbol == "kJ"
    assert Energy.cal(3).symbol == "cal"
    assert Energy.kcal(3).symbol == "kcal"


def test_energy_representation() -> None:
    assert repr(Energy.joules(3)) == "Energy.on('3', Energy.Unit.J)"
    assert repr(Energy.kj(3)) == "Energy.on('3', Energy.Unit.kJ)"
    assert repr(Energy.cal(3)) == "Energy.on('3', Energy.Unit.cal)"
    assert repr(Energy.kcal(3)) == "Energy.on('3', Energy.Unit.kcal)"


def test_energy_representation_should_eval() -> None:
    value = Energy.kj(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestEnergyOperations(Operators):
    def unit(self):
        return Energy

    def qty(self, value):
        return Energy.joules(value)
