# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.volume import Volume

from .operators_helper import Operators


def test_volume_should_be_convertible_on_all_volume_units() -> None:
    value = Volume.l("0.9")
    assert value.to(Volume.Unit.dl).magnitude == Fraction("9")
    assert value.to(Volume.Unit.cl).magnitude == Fraction("90")
    assert value.to(Volume.Unit.ml).magnitude == Fraction("900")
    assert value.to(Volume.Unit.tbs).magnitude == Fraction("60")
    assert value.to(Volume.Unit.tsp).magnitude == Fraction("180")


def test_volume_unit() -> None:
    assert Volume.ml(3).unit == Volume.Unit.ml
    assert Volume.cl(3).unit == Volume.Unit.cl
    assert Volume.dl(3).unit == Volume.Unit.dl
    assert Volume.l(3).unit == Volume.Unit.l
    assert Volume.tbs(3).unit == Volume.Unit.tbs
    assert Volume.tsp(3).unit == Volume.Unit.tsp
    assert Volume.cup(3).unit == Volume.Unit.cup
    assert Volume.handful(3).unit == Volume.Unit.handful
    assert Volume.pinch(3).unit == Volume.Unit.pinch


def test_volume_rendering() -> None:
    assert str(Volume.ml(3)) == "3 ml"
    assert str(Volume.cl(3)) == "3 cl"
    assert str(Volume.dl(3)) == "3 dl"
    assert str(Volume.l(3)) == "3 l"
    assert str(Volume.tbs(3)) == "3 tbs"
    assert str(Volume.tsp(3)) == "3 tsp"
    assert str(Volume.cup(3)) == "3 cup"
    assert str(Volume.handful(3)) == "3 handful"
    assert str(Volume.pinch(3)) == "3 pinch"


def test_volume_symbol() -> None:
    assert Volume.ml(3).symbol == "ml"
    assert Volume.cl(3).symbol == "cl"
    assert Volume.dl(3).symbol == "dl"
    assert Volume.l(3).symbol == "l"
    assert Volume.tbs(3).symbol == "tbs"
    assert Volume.tsp(3).symbol == "tsp"
    assert Volume.cup(3).symbol == "cup"
    assert Volume.handful(3).symbol == "handful"
    assert Volume.pinch(3).symbol == "pinch"


def test_volume_representation() -> None:
    assert repr(Volume.ml(3)) == "Volume.on('3', Volume.Unit.ml)"
    assert repr(Volume.cl(3)) == "Volume.on('3', Volume.Unit.cl)"
    assert repr(Volume.dl(3)) == "Volume.on('3', Volume.Unit.dl)"
    assert repr(Volume.l(3)) == "Volume.on('3', Volume.Unit.l)"
    assert repr(Volume.tbs(3)) == "Volume.on('3', Volume.Unit.tbs)"
    assert repr(Volume.tsp(3)) == "Volume.on('3', Volume.Unit.tsp)"
    assert repr(Volume.cup(3)) == "Volume.on('3', Volume.Unit.cup)"
    assert repr(Volume.handful(3)) == "Volume.on('3', Volume.Unit.handful)"
    assert repr(Volume.pinch(3)) == "Volume.on('3', Volume.Unit.pinch)"


def test_volume_representation_should_eval() -> None:
    value = Volume.l(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestVolumeOperations(Operators):
    def unit(self):
        return Volume

    def qty(self, value):
        return Volume.l(value)
