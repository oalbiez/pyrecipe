# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.converters import Converters
from pyrecipe.model.quantities.unit_factory import UnitFactory, milli

gram = UnitFactory.unit("gram", "g")


def test_unit_should_have_name() -> None:
    assert gram.name == "gram"


def test_unit_should_have_symbol() -> None:
    assert gram.symbol == "g"


def test_unit_should_have_storage_conversion() -> None:
    assert gram.converter.to_reference(Fraction(1)) == Fraction(1)
    assert gram.store_as(milli).converter.to_reference(Fraction(1)) == Fraction(1000)


centi = UnitFactory.prefix("centi", "c", Converters.linear("0.01"))


def test_unit_prefix_should_have_name() -> None:
    assert centi.name == "centi"


def test_unit_prefix_should_have_symbol() -> None:
    assert centi.symbol == "c"


def test_unit_prefix_should_have_conversion() -> None:
    assert centi.converter.to_reference(Fraction(100)) == Fraction(1)
    assert centi.converter.from_reference(Fraction(1)) == Fraction(100)


centigram = UnitFactory.composite(centi, gram)


def test_composite_unit_should_have_name() -> None:
    assert centigram.name == "centigram"


def test_composite_unit_should_have_symbol() -> None:
    assert centigram.symbol == "cg"


def test_composite_unit_should_have_conversion() -> None:
    assert centigram.converter.to_reference(Fraction(100)) == Fraction(1)
    assert centigram.converter.from_reference(Fraction(1)) == Fraction(100)


base = UnitFactory.unit("base", "#b")
derived = UnitFactory.unit("derived", "#d").derived(
    base, Converters.affine(Fraction(2), Fraction(10))
)


def test_derived_unit_should_have_name() -> None:
    assert derived.name == "derived"


def test_derived_unit_should_have_symbol() -> None:
    assert derived.symbol == "#d"


def test_derived_unit_should_have_conversion() -> None:
    assert derived.converter.to_reference(Fraction(1)) == Fraction(12)
    assert derived.converter.from_reference(Fraction(12)) == Fraction(1)
