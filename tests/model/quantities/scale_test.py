# -*- coding: utf-8 -*-
import pytest

from pyrecipe.model.quantities.interval import Interval, LowerBound, UpperBound
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scale import Partition, Scale


def test_partition_should_repr() -> None:
    assert (
        repr(
            Partition.on(LowerBound.closed(Mass.g(0)), UpperBound.closed(Mass.g(1)), 0)
        )
        == "Partition("
        "Interval(LowerBound.closed(Mass.on('0', Mass.Unit.g)), "
        "UpperBound.closed(Mass.on('1', Mass.Unit.g))), 0)"
    )


def test_scale_should_rank_a_value():
    scale = Scale.scale(
        Partition.on(LowerBound.closed(Mass.g(0)), UpperBound.closed(Mass.g(1)), 0),
        Partition.on(LowerBound.opened(Mass.g(1)), UpperBound.closed(Mass.g(2)), 1),
        Partition.on(LowerBound.opened(Mass.g(2)), UpperBound.closed(Mass.g(3)), 2),
    )
    assert scale.rank(Mass.g(0)) == 0
    assert scale.rank(Mass.g(0.5)) == 0
    assert scale.rank(Mass.g(1)) == 0
    assert scale.rank(Mass.g(1.01)) == 1
    assert scale.rank(Mass.g(2)) == 1
    assert scale.rank(Mass.g(2.01)) == 2
    assert scale.rank(Mass.g(3)) == 2


def test_scale_should_fail_when_value_is_not_rankable():
    scale = Scale.scale(
        Partition.on(LowerBound.closed(Mass.g(0)), UpperBound.closed(Mass.g(1)), 0),
        Partition.on(LowerBound.opened(Mass.g(1)), UpperBound.closed(Mass.g(2)), 1),
        Partition.on(LowerBound.opened(Mass.g(2)), UpperBound.closed(Mass.g(3)), 2),
    )
    with pytest.raises(ValueError):
        _ = scale.rank(Mass.g(5))


def test_scale_should_find_interval_for_a_given_rank():
    scale = Scale.scale(
        Partition.on(LowerBound.closed(Mass.g(0)), UpperBound.closed(Mass.g(1)), 0),
        Partition.on(LowerBound.opened(Mass.g(1)), UpperBound.closed(Mass.g(2)), 1),
        Partition.on(LowerBound.opened(Mass.g(2)), UpperBound.closed(Mass.g(3)), 2),
    )

    assert scale.interval_for(1) == Interval(
        LowerBound.opened(Mass.g(1)), UpperBound.closed(Mass.g(2))
    )


def test_scale_should_fail_when_searching_unknwon_rank():
    scale = Scale.scale(
        Partition.on(LowerBound.closed(Mass.g(0)), UpperBound.closed(Mass.g(1)), 0),
        Partition.on(LowerBound.opened(Mass.g(1)), UpperBound.closed(Mass.g(2)), 1),
        Partition.on(LowerBound.opened(Mass.g(2)), UpperBound.closed(Mass.g(3)), 2),
    )
    with pytest.raises(ValueError):
        _ = scale.interval_for(5)
