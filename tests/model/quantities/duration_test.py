# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.duration import Duration

from .operators_helper import Operators


def test_duration_should_be_convertible_on_all_duration_units() -> None:
    value = Duration.days(1)
    assert value.to(Duration.Unit.H).magnitude == Fraction("24")
    assert value.to(Duration.Unit.min).magnitude == Fraction("24") * Fraction("60")
    assert value.to(Duration.Unit.s).magnitude == Fraction("24") * Fraction("3600")


def test_duration_hhmm() -> None:
    assert Duration.hhmm(1, 30) == Duration.minutes(90)


def test_duration_unit() -> None:
    assert Duration.days(3).unit == Duration.Unit.days
    assert Duration.hours(3).unit == Duration.Unit.H
    assert Duration.minutes(3).unit == Duration.Unit.min
    assert Duration.seconds(3).unit == Duration.Unit.s


def test_duration_rendering() -> None:
    assert str(Duration.days(1)) == "1 d"
    assert str(Duration.hours(1)) == "1 h"
    assert str(Duration.minutes(2)) == "2 min"
    assert str(Duration.seconds(2)) == "2 s"


def test_duration_symbol() -> None:
    assert Duration.days(3).symbol == "d"
    assert Duration.hours(3).symbol == "h"
    assert Duration.minutes(3).symbol == "min"
    assert Duration.seconds(3).symbol == "s"


def test_duration_representation() -> None:
    assert repr(Duration.days(3)) == "Duration.on('3', Duration.Unit.days)"
    assert repr(Duration.hours(3)) == "Duration.on('3', Duration.Unit.H)"
    assert repr(Duration.minutes(3)) == "Duration.on('3', Duration.Unit.min)"
    assert repr(Duration.seconds(3)) == "Duration.on('3', Duration.Unit.s)"


def test_duration_representation_should_eval() -> None:
    value = Duration.hours(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestdurationOperations(Operators):
    def unit(self):
        return Duration

    def qty(self, value):
        return Duration.minutes(value)
