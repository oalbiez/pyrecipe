# -*- coding: utf-8 -*-
from fractions import Fraction

from hypothesis import assume, given
from hypothesis.strategies import fractions

from pyrecipe.model.quantities.converters import Converters


@given(fractions())
def test_identity_converter_do_nothing(x: Fraction) -> None:
    converter = Converters.identity()
    assert converter.to_reference(x) == x
    assert converter.from_reference(x) == x
    assert converter.from_reference(converter.to_reference(x)) == x


@given(fractions(), fractions())
def test_linear_converter_do_linear_transformation(
    scale: Fraction, x: Fraction
) -> None:
    assume(scale != 0)
    converter = Converters.linear(scale)
    assert converter.from_reference(converter.to_reference(x)) == x


@given(fractions(), fractions(), fractions())
def test_linear_converter_do_affine_transformation(
    scale: Fraction, offset: Fraction, x: Fraction
) -> None:
    assume(scale != 0)
    converter = Converters.affine(scale, offset)
    assert converter.from_reference(converter.to_reference(x)) == x


@given(fractions(), fractions(), fractions())
def test_inversed_converter(scale: Fraction, offset: Fraction, x: Fraction) -> None:
    assume(scale != 0)
    converter = Converters.affine(scale, offset)
    inversed = Converters.inverse(converter)
    assert converter.from_reference(inversed.from_reference(x)) == x
    assert inversed.to_reference(converter.to_reference(x)) == x
    assert inversed.from_reference(inversed.to_reference(x)) == x


@given(fractions(), fractions(), fractions())
def test_composition_converter(scale: Fraction, offset: Fraction, x: Fraction) -> None:
    assume(scale != 0)
    first = Converters.affine(scale, offset)
    second = Converters.linear(scale)
    converter = Converters.compose(first, second)
    assert converter.from_reference(converter.to_reference(x)) == x
