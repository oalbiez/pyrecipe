# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.length import Length

from .operators_helper import Operators


def test_length_should_be_convertible_on_all_length_units() -> None:
    value = Length.m(1)
    assert value.to(Length.Unit.mm).magnitude == Fraction("1000")
    assert value.to(Length.Unit.cm).magnitude == Fraction("100")
    assert value.to(Length.Unit.dm).magnitude == Fraction("10")


def test_length_unit() -> None:
    assert Length.mm(3).unit == Length.Unit.mm
    assert Length.cm(3).unit == Length.Unit.cm
    assert Length.dm(3).unit == Length.Unit.dm
    assert Length.m(3).unit == Length.Unit.m


def test_length_rendering() -> None:
    assert str(Length.mm(3)) == "3 mm"
    assert str(Length.cm(3)) == "3 cm"
    assert str(Length.dm(3)) == "3 dm"
    assert str(Length.m(3)) == "3 m"


def test_length_symbol() -> None:
    assert Length.mm(3).symbol == "mm"
    assert Length.cm(3).symbol == "cm"
    assert Length.dm(3).symbol == "dm"
    assert Length.m(3).symbol == "m"


def test_length_representation() -> None:
    assert repr(Length.mm("3.56")) == "Length.on('3.56', Length.Unit.mm)"
    assert repr(Length.cm(3)) == "Length.on('3', Length.Unit.cm)"
    assert repr(Length.dm(3)) == "Length.on('3', Length.Unit.dm)"
    assert repr(Length.m(3)) == "Length.on('3', Length.Unit.m)"


def test_length_representation_should_eval() -> None:
    value = Length.m(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestLengthOperations(Operators):
    def unit(self):
        return Length

    def qty(self, value):
        return Length.m(value)
