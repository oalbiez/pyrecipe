# -*- coding: utf-8 -*-
from enum import Enum

import pytest

from pyrecipe.model.quantities.dimension import find_unit
from pyrecipe.model.quantities.unit_factory import UnitFactory, kilo, micro, milli


class Units(Enum):
    # pylint: disable=invalid-name
    gram = UnitFactory.unit("gram", "g")
    microgram = UnitFactory.composite(micro, gram)
    milligram = UnitFactory.composite(milli, gram)
    kilogram = UnitFactory.composite(kilo, gram)


def test_find_units_on_success() -> None:
    assert find_unit(Units, "g") == Units.gram


def test_find_units_on_error() -> None:
    with pytest.raises(ValueError):
        find_unit(Units, "l")
