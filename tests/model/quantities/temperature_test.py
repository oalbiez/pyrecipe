# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.temperature import Temperature

from .operators_helper import Operators


def test_temperature_should_be_convertible_on_all_temperature_units() -> None:
    value = Temperature.celsius(200)
    assert value.to(Temperature.Unit.F).magnitude == Fraction("392")


def test_temperature_unit() -> None:
    assert Temperature.celsius(3).unit == Temperature.Unit.C
    assert Temperature.fahrenheit(3).unit == Temperature.Unit.F


def test_temperature_rendering() -> None:
    assert str(Temperature.celsius(3)) == "3 °C"
    assert str(Temperature.fahrenheit(3)) == "3 °F"


def test_temperature_symbol() -> None:
    assert Temperature.celsius(3).symbol == "°C"
    assert Temperature.fahrenheit(3).symbol == "°F"


def test_temperature_representation() -> None:
    assert repr(Temperature.celsius(3)) == "Temperature.on('3', Temperature.Unit.C)"
    assert repr(Temperature.fahrenheit(3)) == "Temperature.on('3', Temperature.Unit.F)"


def test_temperature_representation_should_eval() -> None:
    value = Temperature.celsius(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestTemperatureOperations(Operators):
    def unit(self):
        return Temperature

    def qty(self, value):
        return Temperature.celsius(value)
