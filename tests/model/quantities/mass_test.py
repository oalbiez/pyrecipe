# -*- coding: utf-8 -*-
from fractions import Fraction

from pyrecipe.model.quantities.mass import Mass

from .operators_helper import Operators


def test_mass_should_be_convertible_on_all_mass_units() -> None:
    value = Mass.g(1)
    assert value.to(Mass.Unit.ug).magnitude == Fraction("1000000")
    assert value.to(Mass.Unit.mg).magnitude == Fraction("1000")
    assert value.to(Mass.Unit.g).magnitude == Fraction("1")
    assert value.to(Mass.Unit.kg).magnitude == Fraction("1/1000")


def test_mass_unit() -> None:
    assert Mass.ug(3).unit == Mass.Unit.ug
    assert Mass.mg(3).unit == Mass.Unit.mg
    assert Mass.g(3).unit == Mass.Unit.g
    assert Mass.kg(3).unit == Mass.Unit.kg


def test_mass_rendering() -> None:
    assert str(Mass.ug(3)) == "3 µg"
    assert str(Mass.mg(3)) == "3 mg"
    assert str(Mass.g(3)) == "3 g"
    assert str(Mass.kg(3)) == "3 kg"


def test_mass_symbol() -> None:
    assert Mass.ug(3).symbol == "µg"
    assert Mass.mg(3).symbol == "mg"
    assert Mass.g(3).symbol == "g"
    assert Mass.kg(3).symbol == "kg"


def test_mass_representation() -> None:
    assert repr(Mass.ug("3.56")) == "Mass.on('3.56', Mass.Unit.ug)"
    assert repr(Mass.mg(3)) == "Mass.on('3', Mass.Unit.mg)"
    assert repr(Mass.g(3)) == "Mass.on('3', Mass.Unit.g)"
    assert repr(Mass.kg(3)) == "Mass.on('3', Mass.Unit.kg)"


def test_mass_representation_should_eval() -> None:
    value = Mass.g(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestMassOperations(Operators):
    def unit(self):
        return Mass

    def qty(self, value):
        return Mass.g(value)
