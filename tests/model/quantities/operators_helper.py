# -*- coding: utf-8 -*-
from abc import abstractmethod
from fractions import Fraction

import pytest
from hypothesis import assume, given
from hypothesis.strategies import integers

from pyrecipe.model.quantities.scalar import Scalar

numbers = integers(-10_000_000_000, 10_000_000_000)


def approx(value):
    return pytest.approx(value, 0.01)


class Operators:
    # pylint: disable=invalid-name

    @abstractmethod
    def unit(self): ...

    @abstractmethod
    def qty(self, value): ...

    @given(numbers, numbers)
    def test_should_have_addition_operation(self, left: int, right: int) -> None:
        assert self.qty(left) + self.qty(right) == approx(self.qty(left + right))

    @given(numbers, numbers)
    def test_should_have_substraction_operation(self, left: int, right: int) -> None:
        assert self.qty(left) - self.qty(right) == approx(self.qty(left - right))

    @given(numbers, numbers)
    def test_should_have_multiplication_operation_with_number(
        self, left: int, right: int
    ) -> None:
        assert self.qty(left) * right == approx(self.qty(left * right))

    @given(numbers, numbers)
    def test_should_have_multiplication_operation_with_scalar(
        self, left: int, right: int
    ) -> None:
        assert self.qty(left) * Scalar.scalar(right) == approx(self.qty(left * right))

    @given(numbers, numbers)
    def test_should_have_division_operation_with_number(
        self, left: int, right: int
    ) -> None:
        assume(right != 0)
        assert self.qty(left) / right == self.qty(Fraction(left, right))

    @given(numbers, numbers)
    def test_should_have_division_operation_with_scalar(
        self, left: int, right: int
    ) -> None:
        assume(right != 0)
        assert self.qty(left) / Scalar.scalar(right) == self.qty(Fraction(left, right))

    @given(numbers, numbers)
    def test_should_have_ratio(self, left, right) -> None:
        assume(right != 0)
        assert self.unit().ratio(self.qty(left), self.qty(right)) == Scalar.scalar(
            Fraction(left, right)
        )

    @given(numbers)
    def test_should_have_equal_operation(self, value: int) -> None:
        assert self.qty(value) == self.qty(value)

    @given(numbers, numbers)
    def test_should_have_not_equal_operation(self, left: int, right: int) -> None:
        assume(left != right)
        assert self.qty(left) != self.qty(right)

    @given(numbers, numbers)
    def test_should_have_grether_than_operation(self, left: int, right: int) -> None:
        assume(left > right)
        assert self.qty(left) > self.qty(right)

    @given(numbers, numbers)
    def test_should_have_grether_or_equal_operation(
        self, left: int, right: int
    ) -> None:
        assume(left >= right)
        assert self.qty(left) >= self.qty(right)

    @given(numbers, numbers)
    def test_should_have_less_than_operation(self, left: int, right: int) -> None:
        assume(left < right)
        assert self.qty(left) < self.qty(right)

    @given(numbers, numbers)
    def test_should_have_less_or_equal_operation(self, left: int, right: int) -> None:
        assume(left <= right)
        assert self.qty(left) <= self.qty(right)

    def test_should_round(self) -> None:
        value = self.qty("1.1235")
        assert round(value, 0) == self.qty("1")  # type: ignore[call-overload]
        assert round(value, 1) == self.qty("1.1")  # type: ignore[call-overload]
        assert round(value, 2) == self.qty("1.12")  # type: ignore[call-overload]
        assert round(value, 3) == self.qty("1.124")  # type: ignore[call-overload]
