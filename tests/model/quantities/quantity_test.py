# -*- coding: utf-8 -*-
from pyrecipe.model.quantities.quantity import Quantity
from pyrecipe.model.quantities.unit_factory import UnitFactory

from .operators_helper import Operators

_g = UnitFactory.unit("gram", "g")


def test_quantity_should_round() -> None:
    value = Quantity.on("1.1235", _g)
    assert str(round(value, 0)) == "1 g"  # type: ignore[call-overload]
    assert str(round(value, 1)) == "1.1 g"  # type: ignore[call-overload]
    assert str(round(value, 2)) == "1.12 g"  # type: ignore[call-overload]
    assert str(round(value, 3)) == "1.124 g"  # type: ignore[call-overload]


def test_quantity_should_reject_bad_comparison() -> None:
    value = Quantity.on("1.1235", _g)
    # pylint: disable=unnecessary-dunder-call
    assert value.__eq__(None) == NotImplemented
    assert value.__ne__(None) == NotImplemented


class TestQuantityOperations(Operators):
    def unit(self):
        return Quantity

    def qty(self, value):
        return Quantity.on(value, _g)
