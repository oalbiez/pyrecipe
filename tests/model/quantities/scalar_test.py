# -*- coding: utf-8 -*-
from pyrecipe.model.quantities.scalar import Scalar

from .operators_helper import Operators


def test_scalar_rendering() -> None:
    assert str(Scalar.scalar(3)) == "3"


def test_scalar_symbol() -> None:
    assert Scalar.scalar(3).symbol == "#"


def test_scalar_representation() -> None:
    assert repr(Scalar.scalar(3)) == "Scalar.scalar('3')"


def test_scalar_representation_should_eval() -> None:
    value = Scalar.scalar(3)
    assert eval(repr(value)) == value  # pylint: disable=eval-used


class TestScalarOperations(Operators):
    def unit(self):
        return Scalar

    def qty(self, value):
        return Scalar.scalar(value)
