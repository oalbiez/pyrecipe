# -*- coding: utf-8 -*-
from pyrecipe.model.tag import Tag


def test_default_tag_should_not_be_prefixed() -> None:
    assert Tag("value").content == "value"


def test_general_tag_should_be_prefixed() -> None:
    assert Tag.tag("value").content == "tag:value"


def test_region_tag_should_be_prefixed() -> None:
    assert Tag.region("value").content == "region:value"


def test_category_tag_should_be_prefixed() -> None:
    assert Tag.category("value").content == "category:value"


def test_diet_tag_should_be_prefixed() -> None:
    assert Tag.diet("value").content == "diet:value"


def tr(v: str) -> str:
    return v.upper()


def test_tag_should_transform_only_general_and_default_tags() -> None:
    assert Tag("value").transform_text(tr) == Tag("VALUE")
    assert Tag.tag("value").transform_text(tr) == Tag.tag("VALUE")
    assert Tag.region("value").transform_text(tr) == Tag.region("value")
    assert Tag.category("value").transform_text(tr) == Tag.category("value")
    assert Tag.diet("value").transform_text(tr) == Tag.diet("value")
