# -*- coding: utf-8 -*-
from pyrecipe.model.reference import Reference


def test_reference_should_contains_the_value() -> None:
    assert Reference("to").value == "to"


def test_empty_reference_should_be_empty_value() -> None:
    assert Reference.undefined().value == ""


def test_reference_should_render_to_string() -> None:
    assert str(Reference("to")) == "@to"
