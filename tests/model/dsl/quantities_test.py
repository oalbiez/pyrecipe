from pyrecipe.model.dsl import parse_quantities
from pyrecipe.model.quantities.mass import Mass
from pyrecipe.model.quantities.scalar import Scalar


def test_quantities_should_parse_scalar() -> None:
    assert parse_quantities("3") == Scalar.scalar(3)
    assert parse_quantities("30") == Scalar.scalar(30)
    assert parse_quantities("1/2") == Scalar.scalar("1/2")


def test_quantities_should_parse_mass() -> None:
    assert parse_quantities("80 g") == Mass.g(80)
    assert parse_quantities("3g") == Mass.g(3)
    assert parse_quantities("1/2 kg") == Mass.kg("1/2")
    assert parse_quantities("3mg") == Mass.mg(3)
    assert parse_quantities("3  ug") == Mass.ug(3)
    assert parse_quantities("3µg") == Mass.ug(3)
