# -*- coding: utf-8 -*-
from textwrap import dedent

from pyrecipe.model.dsl import DSL

source = dedent(
    """
    aliment: Tomate ronde, crue @vegetable.tomate|ronde|crue
        tags:
            calnut: Tomate ronde, crue
            calnut: 20276
            group:02: fruits, légumes, légumineuses et oléagineux
            group:0201: légumes
            group:020101: légumes crus

        composition:
            energy: 20.3 kcal
            fat: 0.25 g
            fa_saturated: 0.005 g
            fa_mono: 0.005 g
            fa_poly: 0.005 g
            carbohydrate: 3.59 g
            sugars: 2.8 g
            polyols: 0.25 g
            starch: 0.18 g
            fibres: 1 g
            protein: 0.5 g
            salt: 0.0063 g
            alcohol: 0 g
            organic_acids: 0.035 g
            cholesterol: 0 mg
            water: 94.1 g
            vitamin_A: 67.25 µg
            vitamin_D: 0 µg
            vitamin_E: 0.38 mg
            vitamin_K1: 1.13 µg
            vitamin_K2: 0 µg
            vitamin_C: 16.3 mg
            vitamin_B1: 0.048 mg
            vitamin_B2: 0.005 mg
            vitamin_B3: 0.45 mg
            vitamin_B5: 0.061 mg
            vitamin_B6: 0.07 mg
            vitamin_B12: 0 µg
            vitamin_B9: 13.5 µg
            sodium: 2.5 mg
            magnesium: 7 mg
            phosphorus: 20 mg
            chloride: <unknown>
            potassium: 210 mg
            calcium: 6.8 mg
            manganese: 0.1 mg
            iron: 0.17 mg
            copper: 0.02 mg
            zinc: 0.08 mg
            selenium: 10 µg
            iodine: 10 µg
    """
)


def test_pantry_should_parse_and_render() -> None:
    result = DSL.render_pantry(DSL.parse_pantry(source), show_unknown_component=True)
    assert result.strip() == source.strip()
