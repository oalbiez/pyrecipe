# -*- coding: utf-8 -*-
from textwrap import dedent

from pyrecipe.model.dsl import DSL


def test_book_should_parse_and_render() -> None:
    content = dedent(
        """
        recipe: Grape Couscous @dehydrated.Couscous
            source: myself
            lang: en
            servings: 1
            description:
                A smart description

            --
            <80 g> of couscous @couscous
            <20 g> dried grapes @fruit.grapes|dried
            <3 g> vegetable broth powder @vegetable-broth
            <3 g> dehydrated leeks
            <7> chickpeas
            <some> salt
            Mix all ingredients together.
            --
            <40 cl> water
            Add water at a minimum temperature of [90 °C] and wait [8--10 min].
        """
    )
    assert DSL.render_book(DSL.parse_book(content)).strip() == content.strip()
