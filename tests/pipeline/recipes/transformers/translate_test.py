# -*- coding: utf-8 -*-
from __future__ import annotations

from textwrap import dedent

from pyrecipe.model.book import Lang
from pyrecipe.model.dsl import DSL
from pyrecipe.pipeline.recipes.transformers.translator import Action, Translate


class MarkupTranslator(Translate.Driver):
    """Test translator."""

    def translate(self, source: Lang, target: Lang) -> Action:
        def action(value: str) -> str:
            return f"[{source}->{target}: {value}]"

        return action


def test_translate_should_translate_recipe() -> None:
    assert (
        translate(
            """
            recipe: Name
                lang: en

                --
                <20 g> flour
                <2 l> water
                Instruction with [1] quantity
            """,
            Lang("fr"),
        )
        == normalize(
            """
            recipe: [en->fr: Name]
                lang: fr

                --
                <20 g> [en->fr: flour]
                <2 l> [en->fr: water]
                [en->fr: Instruction with ] [1] [en->fr: quantity]


            """
        )
    )


def translate(recipe_defintion: str, lang: Lang) -> str:
    recipe = DSL.parse_recipe(dedent(recipe_defintion).lstrip())
    return DSL.render_recipe(
        Translate.on(MarkupTranslator()).lang(lang).transform(recipe)
    )


def normalize(value: str) -> str:
    return dedent(value).lstrip()
