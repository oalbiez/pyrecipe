# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.model.book import Recipe
from pyrecipe.model.tag import Tag, Tags
from pyrecipe.pipeline.recipes.transformers.tags_transformer import TagTransformer
from pyrecipe.utils.predicates import Predicate


def add_tags(recipe_tags, new_tags) -> list[Tag]:
    return (
        TagTransformer.tags()
        .add(Tags.on(new_tags))
        .transform(Recipe(tags=recipe_tags))
        .tags.content.tolist()
    )


class TestAddTagsTransformer:
    def test_add_tags_should_append_new_tags_in_order(self) -> None:
        assert add_tags(recipe_tags=[Tag("A")], new_tags=[Tag("B"), Tag("C")]) == [
            Tag("A"),
            Tag("B"),
            Tag("C"),
        ]

    def test_add_tags_should_append_tags_in_order_ignoring_existing_ones(self) -> None:
        assert add_tags(
            recipe_tags=[Tag("B")], new_tags=[Tag("A"), Tag("B"), Tag("C")]
        ) == [
            Tag("B"),
            Tag("A"),
            Tag("C"),
        ]


def contains(value) -> Predicate:
    def predicate(tag: Tag) -> bool:
        return value in tag.content

    return predicate


def remove_tags(recipe_tags, predicate) -> list[Tag]:
    return (
        TagTransformer.tags()
        .remove(predicate)
        .transform(Recipe(tags=Tags.on(recipe_tags)))
        .tags.content.tolist()
    )


class TestRemoveTagsTransformer:
    def test_add_tags_should_remove_matching_tags(self) -> None:
        assert remove_tags(
            recipe_tags=[Tag("A"), Tag("B")], predicate=contains("A")
        ) == [
            Tag("B"),
        ]

    def test_add_tags_should_do_nothing_when_no_match(self) -> None:
        assert remove_tags(
            recipe_tags=[Tag("A"), Tag("B")], predicate=contains("C")
        ) == [
            Tag("A"),
            Tag("B"),
        ]
