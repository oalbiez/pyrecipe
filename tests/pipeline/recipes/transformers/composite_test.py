# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.model.book import Recipe, Servings
from pyrecipe.model.tag import Region, Tags
from pyrecipe.pipeline.recipes.transformers.by_recipe_transformer import (
    ByRecipeTransformer,
)
from pyrecipe.pipeline.recipes.transformers.composite import CompositeRecipeTransformer


class Italian(ByRecipeTransformer):
    def transform(self, recipe: Recipe) -> Recipe:
        return recipe.update(tags=Tags.on([Region.ITALIAN.value]))


class For6(ByRecipeTransformer):
    def transform(self, recipe: Recipe) -> Recipe:
        return recipe.update(servings=Servings(6))


def test_compose_should_apply_all_transformers() -> None:
    result = list(
        CompositeRecipeTransformer.chain(Italian(), For6()).process([Recipe()])
    )[0]
    assert result.servings == 6
    assert result.tags == Tags.on([Region.ITALIAN.value])
