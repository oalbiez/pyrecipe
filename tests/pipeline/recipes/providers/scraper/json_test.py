# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.pipeline.recipes.providers.scraper.json import json_to_recipe

RECIPE = """
{
    "@context": "http://schema.org",
    "@type": "Recipe",
    "name": "Soupe à la courgette et au curry",
    "recipeCategory": "soupe",
    "datePublished": "2006-10-23T11:43:00+02:00",
    "prepTime": "PT30M",
    "cookTime": "PT30M",
    "totalTime": "PT60M",
    "recipeYield": "6 personnes",
    "recipeIngredient": [
        "1 kg de courgette",
        "1 oignon",
        "curry",
        "poivre",
        "sel",
        "huile",
        "300 g de pomme de terre",
        "1 gousse d'ail",
        "bouillon de volaille"
    ],
    "recipeInstructions": [
        {
            "@type": "HowToStep",
            "text": "Découpez en cubes les courgettes et les pommes de terre épluchées. Coupez les oignons grossièrement."
        },
        {
            "@type": "HowToStep",
            "text": "Préparez du bouillon de volaille (frais ou lyophilisé)."
        },
        {
            "@type": "HowToStep",
            "text": "Dans une cocotte, faites chauffez 2 cuillères à soupe d'huile. Quand celle ci est bien chaude, versez les oignons et l'ail et saupoudrez de curry (vous pouvez être généreux mais si la présence du curry vous surprend, mettez en peu la 1ère fois)."
        },
        {
            "@type": "HowToStep",
            "text": "Remuez constamment, le temps que les oignons s'imprègnent de curry. Versez 1 l d'eau (y compris le bouillon)."
        },
        {
            "@type": "HowToStep",
            "text": "Ajoutez les pommes de terre et 5 à 10 mn plus tard, les dés de courgettes. "
        },
        {
            "@type": "HowToStep",
            "text": "Laissez cuire 20 min. La soupe ne doit pas bouillir. Salez et poivrez."
        },
        {
            "@type": "HowToStep",
            "text": "Mixez (de préférence le plus possible, cette soupe est meilleure lisse). C'est prêt!"
        }
    ],
    "author": "emmy_16450253",
    "description": "courgette, oignon, curry, poivre, sel, huile, pomme de terre, ail, bouillon de volaille",
    "keywords": "Soupe à la courgette et au curry, courgette, oignon, curry, poivre, sel, huile, pomme de terre, ail, bouillon de volaille, soupe, facile, bon marché",
    "recipeCuisine": "Entrée",
}
"""


def test_should_scrap_name() -> None:
    recipe = json_to_recipe(
        """
        {
            "@context": "http://schema.org",
            "@type": "Recipe",
            "name": "Soupe à la courgette et au curry"
        }
    """
    )
    assert recipe.name == "Soupe à la courgette et au curry"
