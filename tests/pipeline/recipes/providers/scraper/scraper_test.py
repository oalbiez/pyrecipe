# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Iterable
from io import StringIO

from bs4 import BeautifulSoup  # type: ignore

from pyrecipe.model.book import Recipe
from pyrecipe.pipeline.recipes.providers.scraper.reporter import ConsoleReporter
from pyrecipe.pipeline.recipes.providers.scraper.scraper import (
    URL,
    Getter,
    Scraper,
    ScraperDriver,
)


class FakeDriver(ScraperDriver):
    def __init__(self):
        self._startup: list[URL] = []
        self._links: dict[URL, list[URL]] = {}
        self._recipes: dict[URL, list[Recipe]] = {}

    def start(self, url: URL) -> FakeDriver:
        self._startup.append(url)
        return self

    def recipe(self, url: URL, recipe: Recipe) -> FakeDriver:
        self._recipes.setdefault(url, []).append(recipe)
        return self

    def link(self, url: URL, link: URL) -> FakeDriver:
        self._links.setdefault(url, []).append(link)
        return self

    def startup(self) -> list[URL]:
        return self._startup

    def recipes(self, url: URL, _: BeautifulSoup) -> Iterable[Recipe]:
        return self._recipes.get(url, [])

    def links(self, url: URL, _: BeautifulSoup) -> Iterable[URL]:
        return self._links.get(url, [])


class AlwaysFoundGetter(Getter):
    def get(self, _: URL) -> str | None:
        return "<html></html>"


class NeverFoundGetter(Getter):
    def get(self, _: URL) -> str | None:
        return None


class TestScrap:
    def test_scrap_recipes_when_url_found(self) -> None:
        scraper = Scraper(
            AlwaysFoundGetter(),
            ConsoleReporter(StringIO()),
            FakeDriver()
            .start(URL("start"))
            .recipe(URL("start"), Recipe())
            .recipe(URL("start"), Recipe()),
        )
        assert len(list(scraper.provide())) == 2

    def test_scrap_visit_url_once(self) -> None:
        scraper = Scraper(
            AlwaysFoundGetter(),
            ConsoleReporter(StringIO()),
            FakeDriver()
            .start(URL("start"))
            .recipe(URL("start"), Recipe())
            .link(URL("start"), URL("start")),
        )
        assert len(list(scraper.provide())) == 1

    def test_scrap_no_recipe_when_url_not_found(self) -> None:
        scraper = Scraper(
            NeverFoundGetter(),
            ConsoleReporter(StringIO()),
            FakeDriver()
            .start(URL("start"))
            .recipe(URL("start"), Recipe())
            .recipe(URL("start"), Recipe()),
        )
        assert len(list(scraper.provide())) == 0

    def test_scrap_multiples_urls(self) -> None:
        scraper = Scraper(
            AlwaysFoundGetter(),
            ConsoleReporter(StringIO()),
            FakeDriver()
            .start(URL("start"))
            .link(URL("start"), URL("a"))
            .link(URL("start"), URL("b"))
            .link(URL("a"), URL("c"))
            .recipe(URL("a"), Recipe())
            .recipe(URL("b"), Recipe())
            .recipe(URL("c"), Recipe()),
        )
        assert len(list(scraper.provide())) == 3
