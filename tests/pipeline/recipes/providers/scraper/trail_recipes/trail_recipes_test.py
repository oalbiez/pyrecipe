# -*- coding: utf-8 -*-
from __future__ import annotations

import pkgutil

from bs4 import BeautifulSoup  # type: ignore

from pyrecipe.pipeline.recipes.providers.scraper.trail_recipes import (
    URL,
    TrailRecipesDriver,
)


class TestTrailRecipesDriver:
    def test_scrap_should_extract_links_from_index_page(self) -> None:
        recipes = TrailRecipesDriver().recipes(
            URL("dehydrated-backpacking-meals"),
            soup_from("dehydrated-backpacking-meals.raw"),
        )
        assert len(list(recipes)) == 0

    def test_scrap_should_extract_recipe_from_recipe_page(self) -> None:
        recipes = TrailRecipesDriver().recipes(
            URL("/recipes/beef-alfredo"), soup_from("beef-alfredo.raw")
        )
        assert len(list(recipes)) == 1

    def test_walk_should_extract_links_from_index_page(self) -> None:
        links = TrailRecipesDriver().links(
            URL("dehydrated-backpacking-meals"),
            soup_from("dehydrated-backpacking-meals.raw"),
        )
        assert len(list(links)) == 61

    def test_walk_should_extract_links_from_recipe_page(self) -> None:
        links = TrailRecipesDriver().links(
            URL("/recipes/beef-alfredo"), soup_from("beef-alfredo.raw")
        )
        assert len(list(links)) == 0


def soup_from(name: str) -> BeautifulSoup:
    content = pkgutil.get_data(__name__, name)
    if content:
        return BeautifulSoup(content.decode("utf-8"), "html.parser")
    raise ValueError(f"resource not found {name} in package {__name__}")
