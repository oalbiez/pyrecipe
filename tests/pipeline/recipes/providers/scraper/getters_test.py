# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.pipeline.recipes.providers.scraper.getters import (
    URL,
    Cache,
    CacheRead,
    CacheWrite,
    SqliteCache,
)
from pyrecipe.pipeline.recipes.providers.scraper.scraper import Getter


class TestCacheWrite:
    def test_should_put_the_received_resources_in_the_cache(self) -> None:
        cache = MemoryCache()
        CacheWrite(FakeGetter().resource(URL("first"), "first content"), cache).get(
            URL("first")
        )
        assert cache.get(URL("first")) == "first content"


class TestCacheRead:
    def test_should_get_the_resource_from_the_cache_when_found(self) -> None:
        assert (
            CacheRead(MemoryCache().add(URL("first"), "first content")).get(
                URL("first")
            )
            == "first content"
        )

    def test_should_return_nothing_when_getting_unknown_resource(self) -> None:
        assert CacheRead(MemoryCache()).get(URL("first")) is None


class TestSqliteCache:
    def test_should_return_nothing_when_getting_unknown_resource(self) -> None:
        cache = SqliteCache(":memory:")
        assert cache.get(URL("first")) is None

    def test_should_return_the_resource(self) -> None:
        cache = SqliteCache(":memory:")
        cache.put(URL("first"), "first content")
        assert cache.get(URL("first")) == "first content"


class MemoryCache(Cache):
    def __init__(self):
        self._cache: dict[URL, str] = {}

    def add(self, url: URL, content: str) -> MemoryCache:
        self._cache[url] = content
        return self

    def put(self, url: URL, content: str) -> str:
        self._cache[url] = content
        return content

    def get(self, url: URL) -> str | None:
        return self._cache.get(url, None)


class FakeGetter(Getter):
    def __init__(self):
        self._contents: dict[URL, str] = {}

    def resource(self, url: URL, content: str) -> FakeGetter:
        self._contents[url] = content
        return self

    def get(self, url: URL) -> str | None:
        return self._contents.get(url, None)
