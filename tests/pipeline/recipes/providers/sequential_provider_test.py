# -*- coding: utf-8 -*-
from __future__ import annotations

from pyrecipe.pipeline.recipes.collectors.recipes_list import ListRecipes
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.providers.memory import MemoryRecipeProvider
from pyrecipe.pipeline.recipes.providers.sequential import SequentialRecipeProvider

first = MemoryRecipeProvider(
    """
    recipe: First a
        lang: en

    recipe: First b
        lang: en
    """
)

second = MemoryRecipeProvider(
    """
    recipe: Second a
        lang: en

    recipe: Second b
        lang: en
    """
)


def test_sequential_provider() -> None:
    collector = ListRecipes()
    Pipeline().from_(SequentialRecipeProvider.chain(first, second)).to_(collector).run()
    assert list(collector.recipes) == [
        "First a",
        "First b",
        "Second a",
        "Second b",
    ]
