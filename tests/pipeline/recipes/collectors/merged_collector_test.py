# -*- coding: utf-8 -*-
from pyrecipe.pipeline.recipes.collectors.merger import RecipeMerger
from pyrecipe.pipeline.recipes.collectors.recipes_list import ListRecipes
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.providers.memory import MemoryRecipeProvider

original = MemoryRecipeProvider(
    """
    recipe: First @first
        lang: en

    recipe: Second @second
        lang: en
    """
)

news = MemoryRecipeProvider(
    """
    recipe: Third @third
        lang: en

    recipe: Second @second
        lang: en

    recipe: First @first
        lang: en
    """
)


def test_should_merge_keeping_original_order() -> None:
    result = ListRecipes()
    RecipeMerger(original, result)
    Pipeline().from_(news).to_(
        RecipeMerger(original, result).keep_original_order()
    ).run()
    assert list(result.recipes) == [
        "First",
        "Second",
        "Third",
    ]


def test_should_merge_with_update_order() -> None:
    result = ListRecipes()
    RecipeMerger(original, result)
    Pipeline().from_(news).to_(RecipeMerger(original, result).update_order()).run()
    assert list(result.recipes) == [
        "Third",
        "Second",
        "First",
    ]
