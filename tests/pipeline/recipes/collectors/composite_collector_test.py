# -*- coding: utf-8 -*-
from pyrecipe.pipeline.recipes.collectors.composite import CompositeRecipeCollector
from pyrecipe.pipeline.recipes.collectors.recipes_list import ListRecipes
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.providers.memory import MemoryRecipeProvider

recipes = MemoryRecipeProvider(
    """
    recipe: First
        lang: en

    recipe: Second
        lang: en
    """
)


def test_should_collect_composite_collector() -> None:
    first = ListRecipes()
    second = ListRecipes()
    Pipeline().from_(recipes).to_(CompositeRecipeCollector.chain(first, second)).run()
    assert list(first.recipes) == [
        "First",
        "Second",
    ]
    assert list(second.recipes) == [
        "First",
        "Second",
    ]
