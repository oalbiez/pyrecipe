# -*- coding: utf-8 -*-

from pyrecipe.pipeline.recipes.collectors.recipes_statistics import (
    RecipesStatistics,
    Statistics,
)
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.providers.memory import MemoryRecipeProvider

recipes = MemoryRecipeProvider(
    """
    recipe: First
        lang: en

    recipe: Second
        lang: en
    """
)


def statistics(provider: MemoryRecipeProvider) -> Statistics:
    collector = RecipesStatistics()
    Pipeline().from_(provider).to_(collector).run()
    return collector.statistics


def test_should_compute_recipes_statistics() -> None:
    assert statistics(recipes) == Statistics(recipes=2, characters=11)
