# -*- coding: utf-8 -*-
from io import StringIO
from textwrap import dedent

from pyrecipe.pipeline.recipes.collectors.latex import LatexGenerator
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.providers.memory import MemoryRecipeProvider

recipes = MemoryRecipeProvider(
    """
    recipe: Grape Couscous @dehydrated.Couscous
        source: myself
        lang: en
        servings: 1

        --
        <80 g> of couscous @couscous
        <20 g> dried grapes @fruit.grapes|dried
        <3 g> vegetable broth powder @vegetable-broth
        <3 g> dehydrated leeks
        <7> chickpeas
        <some> salt
        Mix all ingredients together.
        --
        <40 cl> water
        Add water at a minimum temperature of [90 °C] and wait [8--10 min].
    """
)


def generate(provider: MemoryRecipeProvider) -> str:
    result = StringIO()
    Pipeline().from_(provider).to_(LatexGenerator(result)).run()
    return normalize(result.getvalue())


def test_should_generate_latex() -> None:
    assert generate(recipes) == normalize(
        """
            \\documentclass[12pt]{article}
            \\usepackage[utf8]{inputenc}
            \\usepackage[french]{babel}
            \\usepackage[a4paper,textwidth=18cm,textheight=27cm]{geometry}
            \\usepackage[nonumber]{cuisine}
            \\usepackage{cooking-units}
            \\usepackage{fontawesome}
            \\usepackage{tikzsymbols}

            \\pagenumbering{gobble}

            \\newcommand{\\preparing}[2]{\\hspace{0.25cm} {\\faGears} \
                {\\bfseries{\\cunum{#1}{#2}}}}
            \\newcommand{\\cooking}[2]{\\hspace{0.25cm} {\\fryingpan} \
                {\\bfseries{\\cunum{#1}{#2}}}}
            \\newcommand{\\baking}[2]{\\hspace{0.25cm} {\\oven} \
                {\\bfseries{\\cunum{#1}{#2}}}}
            \\newcommand{\\waiting}[2]{\\hspace{0.25cm} {attente} \
                {\\bfseries{\\cunum{#1}{#2}}}}
            \\newcommand{\\servings}[1]{{\\faUser}#1}
            \\newcommand{\\tags}[1]{\\freeform#1}
            \\newcommand{\\tag}[1]{{\\small\\faHashtag}{\\bfseries#1}}
            \\newcommand{\\source}[1]{\\freeform #1}

            \\RecipeWidths{\\textwidth}{3cm}{1cm}{4cm}{.75cm}{.75cm}
            \\renewcommand*{\\recipefont}{\\Large\\sffamily}
            \\renewcommand*{\\recipefreeformfont}{\\itshape}
            \\renewcommand*{\\recipeingredientfont}{\\large\\sffamily}
            \\renewcommand*{\\recipestepnumberfont}{\\LARGE\\bfseries\\sffamily}
            \\renewcommand*{\\recipetitlefont}{\\Huge\\selectfont\\bfseries\\sffamily}

            \\declarecookingunit{cup}

            \\begin{document}

                \\begin{recipe}
                    { Grape Couscous }
                    {\\servings{ 1 }}
                    {  }

                    \\source{ myself }


                    \\Ingredient{ \\cunum{ 80 }{ g } of couscous }
                    \\Ingredient{ \\cunum{ 20 }{ g } dried grapes }
                    \\Ingredient{ \\cunum{ 3 }{ g } vegetable broth powder }
                    \\Ingredient{ \\cunum{ 3 }{ g } dehydrated leeks }
                    \\Ingredient{ \\cuam{ 7 } chickpeas }
                    \\Ingredient{ salt }
                    Mix all ingredients together.

                    \\Ingredient{ \\cunum{ 40 }{ cl } water }
                    Add water at a minimum temperature of \\cunum{ 90 }{ C } \
                        and wait \\cunum { 8--10 } { min }.


                \\end{recipe}
                \\newpage

            \\end{document}
            """
    )


def normalize(value: str) -> str:
    return dedent(value).strip().replace(" ", "")
