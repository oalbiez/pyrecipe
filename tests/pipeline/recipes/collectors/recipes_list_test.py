# -*- coding: utf-8 -*-
from pyrecipe.model.book import Name
from pyrecipe.pipeline.recipes.collectors.recipes_list import ListRecipes
from pyrecipe.pipeline.recipes.pipeline import Pipeline
from pyrecipe.pipeline.recipes.providers.memory import MemoryRecipeProvider

recipes = MemoryRecipeProvider(
    """
    recipe: First
        lang: en

    recipe: Second
        lang: en
    """
)


def list_recipes(provider: MemoryRecipeProvider) -> list[Name]:
    collector = ListRecipes()
    Pipeline().from_(provider).to_(collector).run()
    return list(collector.recipes)


def test_should_list_recipes() -> None:
    assert list_recipes(recipes) == [
        "First",
        "Second",
    ]
