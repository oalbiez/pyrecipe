# -*- coding: utf-8 -*-
from pytest import mark

from pyrecipe.utils.ciqual.calnut_reader import aliments


@mark.slow
def test_can_read_calnut() -> None:
    assert len(aliments("data/ciqual-2020-07-07")) == 2119
