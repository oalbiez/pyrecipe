# -*- coding: utf-8 -*-
from pyrecipe.model.tag import Tag, Tags
from pyrecipe.utils.ciqual.common import Group, to_tags


def test_groups_to_tags_should_keep_only_defined_names() -> None:
    assert Group.to_tags(Group("10", "group name")) == Tags.on(
        [Tag("group:10: group name")]
    )
    assert Group.to_tags(
        Group("10", "group name"), Group("1002", "sub group name")
    ) == Tags.on(
        [
            Tag("group:10: group name"),
            Tag("group:1002: sub group name"),
        ]
    )
    assert Group.to_tags(Group("10", "group name"), Group("0000", None)) == Tags.on(
        [Tag("group:10: group name")]
    )


def test_to_tags() -> None:
    assert to_tags(
        source="calnut",
        code="20020",
        name="Courgette, pulpe et peau, crue",
        groups=[
            Group("02", "fruits, légumes, légumineuses et oléagineux"),
            Group("0201", "légumes"),
            Group("020101", "légumes crus"),
        ],
    ) == Tags.on(
        [
            Tag("calnut: Courgette, pulpe et peau, crue"),
            Tag("calnut: 20020"),
            Tag("group:02: fruits, légumes, légumineuses et oléagineux"),
            Tag("group:0201: légumes"),
            Tag("group:020101: légumes crus"),
        ]
    )
