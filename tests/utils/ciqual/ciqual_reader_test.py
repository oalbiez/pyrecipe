# -*- coding: utf-8 -*-
from pytest import mark

from pyrecipe.utils.ciqual.ciqual_reader import aliments


@mark.slow
def test_can_read_ciqual() -> None:
    assert len(aliments("data/ciqual-2020-07-07")) == 3185
