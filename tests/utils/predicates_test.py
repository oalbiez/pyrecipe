# -*- coding: utf-8 -*-
from __future__ import annotations

from random import shuffle
from typing import Any

from hypothesis import given
from hypothesis.strategies import integers

from pyrecipe.utils.predicates import Predicate, all_true, one_true


def true(_: Any) -> bool:
    return True


def false(_: Any) -> bool:
    return False


def value(predicate: Predicate) -> bool:
    return predicate("")


def test_all_true_should_be_true_when_no_predicate() -> None:
    assert value(all_true()) is True


@given(integers(min_value=1, max_value=1000))
def test_all_true_should_be_true_when_all_predicates_are_true(count: int) -> None:
    predicates = [true] * count
    assert value(all_true(*predicates)) is True


@given(integers(min_value=1, max_value=1000), integers(min_value=1, max_value=1000))
def test_all_true_should_be_false_when_at_least_one_predicate_is_false(
    trues: int, falses: int
) -> None:
    predicates = ([true] * trues) + ([false] * falses)
    shuffle(predicates)
    assert value(all_true(*predicates)) is False


def test_one_true_should_be_false_when_no_predicate() -> None:
    assert value(one_true()) is False


@given(integers(min_value=1, max_value=1000))
def test_one_true_should_be_false_when_all_predicates_are_false(count: int) -> None:
    predicates = [false] * count
    assert value(one_true(*predicates)) is False


@given(integers(min_value=1, max_value=1000), integers(min_value=1, max_value=1000))
def test_one_true_should_be_true_when_at_least_one_predicate_is_true(
    trues: int, falses: int
) -> None:
    predicates = ([true] * trues) + ([false] * falses)
    shuffle(predicates)
    assert value(one_true(*predicates)) is True
