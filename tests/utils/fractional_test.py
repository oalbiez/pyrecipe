from fractions import Fraction

from pyrecipe.utils.fractional import decimal_view, fractional_view


def test_fractional_view() -> None:
    assert fractional_view(Fraction(2)) == "2"
    assert fractional_view(Fraction(1, 2)) == "1/2"
    assert fractional_view(Fraction(1, 2), 3) == "1/2"
    assert fractional_view(Fraction(1, 3), 3) == "1/3"
    assert fractional_view(Fraction(1, 4)) == "1/4"
    assert fractional_view(Fraction(1, 8)) == "1/8"
    assert fractional_view(Fraction(40, 39), 3) == "1.026"


def test_decimal_view() -> None:
    assert decimal_view(Fraction(2)) == "2"
    assert decimal_view(Fraction(1, 2)) == "0.5"
    assert decimal_view(Fraction(1, 2), 3) == "0.5"
    assert decimal_view(Fraction(1, 3), 3) == "0.333"
    assert decimal_view(Fraction(1, 4)) == "0.25"
    assert decimal_view(Fraction(1, 8)) == "0.125"
    assert decimal_view(Fraction(40, 39), 3) == "1.026"
