# -*- coding: utf-8 -*-
from pyrecipe.utils.nlp import Index


def test_index_should_match_when_same() -> None:
    assert Index("courge").all_in(Index("Soupe de courge au chocolat"))


def test_index_should_match_when_plurals() -> None:
    assert Index("courges").all_in(Index("Soupe de courge au chocolat"))


def test_index_should_match_when_phonetics() -> None:
    assert Index("carotes").all_in(Index("Soupe de carottes"))
