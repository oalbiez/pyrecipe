# -*- coding: utf-8 -*-

from pyrecipe.model.tag import Diets, Region
from pyrecipe.utils.fuzzy.tag import create_tag


def test_create_tag_with_region() -> None:
    assert create_tag("Asiatique") == Region.ASIAN.value
    assert create_tag("asian") == Region.ASIAN.value
    assert create_tag("Cuisine française") == Region.FRENCH.value
    assert create_tag("Indienne") == Region.INDIAN.value
    assert create_tag("Italienne") == Region.ITALIAN.value
    assert create_tag("Amérique du Sud") == Region.LATIN.value


def test_create_tag_with_diet() -> None:
    assert create_tag("dairy-free") == Diets.DAIRY_FREE.value
    assert create_tag("gluten-free") == Diets.GLUTEN_FREE.value
    assert create_tag("keto") == Diets.KETO.value
    assert create_tag("paleo") == Diets.PALEO.value
    assert create_tag("vegan") == Diets.VEGAN.value
    assert create_tag("vegetarian") == Diets.VEGETARIAN.value
    assert create_tag("végé") == Diets.VEGETARIAN.value
    assert create_tag("sans gluten") == Diets.GLUTEN_FREE.value
    assert create_tag("sans lait") == Diets.DAIRY_FREE.value
