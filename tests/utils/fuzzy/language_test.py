# -*- coding: utf-8 -*-

from pyrecipe.model.book import Lang
from pyrecipe.utils.fuzzy.language import match, tokenize


def test_tokenize_in_english() -> None:
    assert list(
        tokenize(
            Lang("en"),
            "Add water at a minimum temperature of [90 °C] and wait [8--10 min].",
        )
    ) == ["Add", "water", "minimum", "temperature", "wait"]


def test_tokenize_in_french() -> None:
    assert list(
        tokenize(
            Lang("fr"),
            "Ajouter de l'eau à une température minimale de [90 °C] "
            "et attendre [8--10 min].",
        )
    ) == ["Ajouter", "eau", "température", "minimale", "attendre"]


def test_match_in_french() -> None:
    assert (
        match(
            Lang("fr"),
            "courge butternut",
            "Cuire la courge à la vapeur ou 10 minutes à l'eau.",
        )
        is True
    )
