# -*- coding: utf-8 -*-
from pytest import mark

from pyrecipe.model.pantry import Component, Composition
from pyrecipe.model.quantities.scalar import Scalar
from pyrecipe.utils.nutriscore import Nutriscore, NutriscoreAnalyse


@mark.it("nutriscore should evaluate an A")
def test_nutriscore_should_evaluate_an_a():
    composition = Composition.for100g(
        energy=Component.kcal(10),
        sugars=Component.g(10),
        fa_saturated=Component.g(2),
        sodium=Component.mg(0),
        fibres=Component.g(10),
        protein=Component.g(2),
    )
    assert (
        NutriscoreAnalyse.compute_for(
            composition, vegetable_percent=Scalar.scalar("0.2")
        ).score()
        == Nutriscore.A
    )


@mark.it("nutriscore should evaluate an B")
def test_nutriscore_should_evaluate_an_b():
    composition = Composition.for100g(
        energy=Component.kcal(10),
        sugars=Component.g(10),
        fa_saturated=Component.g(5),
        sodium=Component.mg(0),
        fibres=Component.g(10),
        protein=Component.g(2),
    )
    assert (
        NutriscoreAnalyse.compute_for(
            composition, vegetable_percent=Scalar.scalar("0.2")
        ).score()
        == Nutriscore.B
    )


@mark.it("nutriscore should evaluate an C")
def test_nutriscore_should_evaluate_an_c():
    composition = Composition.for100g(
        energy=Component.kcal(10),
        sugars=Component.g(10),
        fa_saturated=Component.g(9),
        sodium=Component.mg(40) * 0.5,
        fibres=Component.g(10),
        protein=Component.g(2),
    )
    assert (
        NutriscoreAnalyse.compute_for(
            composition, vegetable_percent=Scalar.scalar("0.2")
        ).score()
        == Nutriscore.C
    )


@mark.it("nutriscore should evaluate an D")
def test_nutriscore_should_evaluate_an_d():
    composition = Composition.for100g(
        energy=Component.kcal(10),
        sugars=Component.g(10),
        fa_saturated=Component.g(9),
        sodium=Component.mg(40) * 5,
        fibres=Component.g(1),
        protein=Component.g(2),
    )
    assert (
        NutriscoreAnalyse.compute_for(
            composition, vegetable_percent=Scalar.scalar("0.2")
        ).score()
        == Nutriscore.D
    )


@mark.it("nutriscore should evaluate an E")
def test_nutriscore_should_evaluate_an_e():
    composition = Composition.for100g(
        energy=Component.kcal(10),
        sugars=Component.g(10),
        fa_saturated=Component.g(20),
        sodium=Component.mg(40) * 20,
        fibres=Component.g(1),
        protein=Component.g(2),
    )
    assert (
        NutriscoreAnalyse.compute_for(
            composition, vegetable_percent=Scalar.scalar("0.2")
        ).score()
        == Nutriscore.E
    )
