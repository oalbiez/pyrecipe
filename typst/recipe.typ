#set page(paper: "a4", margin: 1cm)


#let icon(file) = box(height: 1em, baseline: 20%, image(height: 100%, file))

#let person = icon("icons/person.svg")
#let hashtag = icon("icons/hashtag.svg")

#let icon_for_duration = (
  baking: icon("icons/oven.svg"),
  cooking: icon("icons/skillet.svg"),
  preparing: icon("icons/manufacturing.svg"),
  waiting: icon("icons/hourglass.svg"),
)

#let icons = (
  alcohol: image(height: 100%, "icons/food/alcohol.svg"),
  celery: image(height: 100%, "icons/food/celery.svg"),
  corn: image(height: 100%, "icons/food/corn.svg"),
  crustaceans: image(height: 100%, "icons/food/crustaceans.svg"),
  eggs: image(height: 100%, "icons/food/eggs.svg"),
  fish: image(height: 100%, "icons/food/fish.svg"),
  gluten: image(height: 100%, "icons/food/gluten.svg"),
  glutenfree: image(height: 100%, "icons/food/gluten-free.svg"),
  gmo: image(height: 100%, "icons/food/gmo.svg"),
  lupin: image(height: 100%, "icons/food/lupin.svg"),
  meat: image(height: 100%, "icons/food/meat.svg"),
  milk: image(height: 100%, "icons/food/milk.svg"),
  molluscs: image(height: 100%, "icons/food/molluscs.svg"),
  mustard: image(height: 100%, "icons/food/mustard.svg"),
  peanut: image(height: 100%, "icons/food/peanut.svg"),
  pork: image(height: 100%, "icons/food/pork.svg"),
  sesame: image(height: 100%, "icons/food/sesame.svg"),
  soya: image(height: 100%, "icons/food/soya.svg"),
  sugar: image(height: 100%, "icons/food/sugar.svg"),
  sulphurdioxide: image(height: 100%, "icons/food/sulphurdioxide.svg"),
  transfat: image(height: 100%, "icons/food/transfat.svg"),
  treenut: image(height: 100%, "icons/food/treenut.svg"),
  vegan: image(height: 100%, "icons/food/vegan.svg"),
)

#let lighgray = luma(95%)

#let recipe(
  name: "",
  servings: 4,
  durations: (:),
  tags: (),
  icons: (),
  ..steps,
) = {
  set text(font: "Linux Biolinum")
  set list(marker: [#sym.ballot], indent: 0mm, body-indent: 2mm)
  let to_durations = durations
    .pairs()
    .map(it => {
      let (name, value) = it
      [#icon_for_duration.at(name) #value]
    })
    .join(" ")
  let counter = counter("step")
  let content = steps.pos().map(it => it(counter))

  stack(
    dir: ttb,
    spacing: 5mm,
    text(
      24pt,
      weight: "bold",
      grid(
        columns: (100% - 2cm, 2cm),
        inset: 0mm,
        name, align(right + bottom, [#person #servings]),
      ),
    ),
    align(top, line(length: 100%, stroke: (thickness: 1mm, cap: "round"))),
    align(right, text(18pt, to_durations)),
    v(10mm),
    ..content,
    v(1fr),
    align(top, line(length: 100%, stroke: (thickness: 1mm, cap: "round"))),
    text(
      10pt,
      tags
        .map(it => box(fill: lighgray, inset: 2pt, radius: 2pt, it))
        .join(" "),
    ),
    block(height: 12mm, inset: 0mm, stack(dir: ltr, spacing: 1mm, ..icons)),
  )
}

#let step(ingredients: "", instructions: "") = counter => {
  counter.step()
  grid(
    columns: (1cm, 5.5cm, 12cm),
    rows: auto,
    column-gutter: 2.5mm,
    row-gutter: 5mm,
    inset: 0mm,
    stroke: (x, y) => if x == 0 {
      (
        right: (
          paint: gray,
          thickness: 1mm,
          cap: "round",
        ),
      )
    },
    align(
      right,
      box(inset: 0.75em, text(20pt, weight: "bold", context counter.display())),
    ),
    text(12pt, ingredients),
    par(justify: true, text(17pt, instructions)),
  )
}

#let description(body) = _ => {
  block(width: 100%, inset: 2mm, text(17pt, style: "italic", body))
}

#let section(body) = _ => {
  block(
    width: 100%,
    fill: lighgray,
    radius: 1mm,
    inset: 2mm,
    above: 5mm,
    text(17pt, body),
  )
}

#recipe(
  name: [Curry de poireaux et c'est une super long titre de recette],
  servings: 4,
  durations: (
    preparing: "10 min",
    cooking: "15 min",
    baking: "25 min",
    waiting: "24 hours",
  ),
  tags: (
    "category:lunch",
    "diet:gluten-free",
    "diet:vegan",
    "diet:vegetarian",
    "region:indian",
    "tag:été",
    "tag:printemps",
  ),
  icons: (icons.vegan,),
  step(
    ingredients: [
      - 15 ml huile d’olive
      - 1/2 oignon émincé
    ],
    instructions: [
      Faire chauffer l'huile d'olive dans une sauteuse. Ajouter l'oignon, faire cuire 2 min.
    ],
  ),
  step(
    ingredients: [
      - 4 poireaux coupés en rondelles
      - 200 g pommes de terre pelées, coupées en morceaux
    ],
    instructions: [
      Ajouter les poireaux et les pommes de terre, mouiller à hauteur, saler, poivrer, porter à ébullition, couvrir, cuire 15 min ou jusqu'à ce que les pommes de terre soient cuites.
    ],
  ),
  step(
    ingredients: [
      - 350 g riz basmati
    ],
    instructions: [
      Pendant ce temps, cuire le riz selon les indications du paquet ou 10 min.
    ],
  ),
  step(
    ingredients: [
      - 20 cl crème de coco
      - 1 tbs curry
    ],
    instructions: [
      Ajouter la crème de coco et le curry dans la sauteuse. Mélanger et cuire 5 min.
    ],
  ),
)
