[![pipeline status](https://gitlab.com/oalbiez/pyrecipe/badges/main/pipeline.svg)](https://gitlab.com/oalbiez/pyrecipe/-/commits/main)  [![coverage report](https://gitlab.com/oalbiez/pyrecipe/badges/main/coverage.svg)](https://gitlab.com/oalbiez/pyrecipe/-/commits/main)


# pyrecipe

hrecipe is a tool suite for managing cooking recipes.


## Development

For running tests:

    poetry install
    poetry run make check test
